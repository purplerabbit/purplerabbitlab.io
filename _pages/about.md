--- 
title: "About"
permalink: /About
type: pages
values:
layout: single
author_profile: true
classes: wide
--- 

I have closed this blog due to doxing issues and will remain up as an historic artifact.

I have changed the password such that even I can not update this blog anymore.

History can be found down below:

<https://gitlab.com/purplerabbit/purplerabbit.gitlab.io>

Hi, I am The Purple Rabbit, an IT Professional and Cyber Security enthusiast.

I started my journey in cybersecurity back when I started a degree in Computer Forensics and Cyber Security back in 2018 which focussed on Computer Forensics using tools such as the paid tool Encase and the free tool Autopsy which I have completed since 2021 and attained a 2:1.

In 2020 I decided to take the world renowned Cyber Security certification The Offensive Security Certified Professional (OSCP) certification which I passed in August 2020. 

I have worked for a UK MSP (Managed Service Provider) working as a junior security analyst and then an IT Support Engineer helping to troubleshoot and fix common IT issues for a number of clients between 2021 and Septmeber 2023

Since September 2023, I am currently studying data structures and algorithms full time to become a Software engineer.

As of Februrary I decided that I don't wish to pursue Software Engineering and am currently seeking employment as a service desk analyst as this is where my expertise are.

To contact me please email me below:

purplerabbit[the number 9][AT SIGN]protonmail[DOT]com

## Analytics

As of 30th November 2023 this blog uses fairanalytics linked [here](https://www.fairanalytics.de) and I will update every so often the analytics data for this small blog.

Updated: 29/01/23

