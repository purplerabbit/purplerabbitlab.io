---
title: "Ethic and Moral issues in Cyber Security"
date: 2022-03-05
classes: wide
categories:
  - infosec 
tag:
  - Cyber Security 
  - Blue Team
header:
  teaser: /assets/ethical.webp
--- 

![ethical]({{ site.url }}{{ site.baseurl }}/assets/ethical.webp)


I will talk about my ethical and moral concerns over ethical hacking or pentesting as some people describe it as in this short article. Now ethical hacking or pentesting as its described is the practice whereby you attempt to find some kind of vulnerability in a system (can be linux, Windows etc) and then attempt to exploit said vulnerability by means of an exploit of some kind, which then gives you access into the system. 

Now Offsec security which is the company that sells the offensive security certified professional which is a course that I completed back in September 2020, you can read my article on it on this blog [Here]({% post_url 2020/2020-12-24-OSCP-Review%}) say that the best approach to cyber security is an offensive one, so that we can secure systems. But I believe they are wrong because in pushing this narrative onto people it thereby encourages more people to take up red teaming and thereby means that we are more likely to have people who are experienced in red teaming than we do blue teaming. Due to this we now have a situation in cyber security whereby there are a lot more red teamers than there are blue teams. Which I think is a shame because red teaming is dangerous in the wrong hands – whereas blue teamers are purely defensive in nature. I believe that because of this narrative that is pushed onto up-and-coming cyber security experts we now have a situation in cyber space whereby computer systems are more likely to be hacked into than defended against.

Bruce Schneier a renowned cyber security expert has said in one of his books that I read a few years ago said that essentially in history in medieval times there has always been a tendency for attackers to have the upper hand and sometimes there is a tendency for the defenders to have the upper hand. In cyber space right now, there is a tendency for attackers to have the upper hand because there simply isn’t enough defenders that we have and coders developing things like defensive tools i.e antivirus software. This issue has gone on for so long because I feel that we should have more people pursuing Blueteam and the industry is filled with pentesting certs such as the OSCP. But I really enjoyed the BTL1 and thought it was a great cert, feel free to give my review on it a read [Here]({% post_url 2022/2022-03-02-BTL1-Review %})

Now more on the ethics and moral side of why I think that pentesting isn’t a good idea for securing computers, because you don’t teach someone the skills of how to make a poison for example and then learn how to defend against a poison. You teach someone an understanding of a poison (and likely human biology) and then teach them how to defend against said poison. This example can be applied to computers, you don’t teach someone how to hack a computer to teach them how to defend it – you teach them what attackers use to hack a computer and then defend against that. But that’s not to say that a blue team defender can’t learn the skills of hacking to be a good blue team defender. Its just I disagree with the notion that in order to be a skilled defender in computers you need to be a skilled attacker.

I feel it’s a little sad how cyber space is right now because we have a situation where there are not many resources for blue team defenders to practice there skills at defending machines during a real time attack, but with red teaming we have a situation where its actually gamified with things like Hackthebox and it doesn’t seem to be clear if there is any actual benefit for Blue Team defenders in taking up things like Hackthebox because most of Hackthebox is intentionally vulnerable machines which doesn’t teach anyone useful skills in how to actually defend against an attack. Granted it teaches the skills that attackers use, which can help Blue Team defenders – but my point is that it doesn’t teach people the fundamental skills in order to be good Cyber Security experts. 

In conclusion, I think that the approach taken by Offsec and others is wrong and the cyber security space needs a complete revamp in terms of its approach to dealing with cyber security because as I said you don’t teach someone how to make poison and then secure yourself against poison – you teach them what kinds of poison that people make poison create and then teach them how to defend against said poison. With that said I think I will be stopping doing anything related to red teaming and focusing a lot more on things like SOC analysis and malware analysis and just topics that are relevant to blue teamers. I think I will also be taking the BTL2 very soon – or at least I’m given it some contemplation because I feel I am drawn to malware analysis more than anything else.

Also please do check out Security Blue Team if you are really interested in becoming a Blue Teamer they have some really good resources for Blue Teaming and their certs are really good. [https://securityblue.team/](https://securityblue.team/)
