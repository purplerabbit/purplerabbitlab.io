---
title: "Archive Emails Locally Using Thunderbird"
date: 2023-11-22
classes: wide
header:
  teaser: "/assets/posts/2023/tb.jpg"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/tb.jpg" alt="">

In this quick guide I am going to teach you how to archive your emails from whichever email provider you use into a folder of .eml files (which is the standard format for emails).

The first thing you need to do is download and install Thunderbird <https://www.thunderbird.net> which if you don’t know is an email client whereby you can manage emails using the Thunderbird client instead of needing to use the online client. 

Just follow through the installation and accept the TOS. It should then ask you to add your email account. Input your name, email, and password (note that if you have MFA enabled you may need to get an app password for Outlook or disable MFA momentarily)

Once you have added your email account, navigate to the folder that you wish to archive locally and then do CTRL + A to select all emails in that folder, right click and then click “Save As” and then choose a folder to save all these emails into .eml format, depending on the number of emails it will take a few moments to save.

Once its saved you can then navigate to the folder you saved these files in and confirm that you have the emails in .eml file. You can then decide to delete the emails from online as you have a local copy of these emails.

You can also find this guide for further reference on another way to archive emails in thunderbird client <https://conetix.com.au/support/archiving-emails-in-thunderbird/>
