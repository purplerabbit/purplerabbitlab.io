---
title: "How To Get YouTube Ad Free on LG SmartTV"
date: 2023-11-22
classes: wide
header:
  teaser: "/assets/posts/2023/lg.jpg"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/lg.jpg" alt="">

In this article I am going to explain how you can get YouTube ad Free on an LG Smart TV running webOS.

The first thing you need to do is create a developer account on the lg website, you need to visit this site <https://webos.developer.lge.com/login> and using an email address create a developer account.

After you have made the developer account, the next thing you need to do is find and navigate to the LG content store on your TV (I struggled to find this, but you should be able to find it when navigating the apps on the TV). On the app store find the search function and search for “developer mode” and then install the developer mode app. (LG’s instructions on enabling the developer mode can be found in this link <https://webos.developer.lge.com/login> )

Next, run the developer mode app on your tv and then enter the email id and password that you used to create the LG developer account previously (note I had to do a password reset for some reason because the password wasn’t working). Once singed in click the “Dev Mode Status” to “ON” and then restart the TV to enable the dev mode.

After your TV has rebooted, the next thing to do is to go back to the developer mode app and enable key server. You should then be given an ip address and a password which you will need to connect to the TV from your PC. 

On your PC install this tool <https://github.com/webosbrew/dev-manager-desktop/releases> and get the installer for your operating system. 

Once you install this tool on your PC you should then have the option to input the ip address and password, input what you got from the developer app on your tv.

Once you enter these details you should then see  “Apps” in the left side of the software. But before you install YouTube ad free from this software you need to go back to your smart tv and uninstall the official YouTube app.

Once you uninstall the YouTube app, navigate back to the dev manager software on your PC and then install YouTube Adfree from the Apps > Available (note that it is a different logo from the official YouTube logo) 

Go back to your TV and confirm that youtube adfree is installed and check it works correctly. You are also able to get a number of other apps if you do so choose.

For reference:
This is a guide on rooting your TV if you want to do that instead <https://gist.github.com/throwaway96/e811b0f7cc2a705a5a476a8dfa45e09f> 	

Update 13/12/2023

For some reason the wifi kept messing up and so I disabled developer mode for a while and recently enabled developer mode again and did what I did in this article, but this time I added adguard's dns to the wifi settings and I also disabled key server and the wifi issue has stopped now.
