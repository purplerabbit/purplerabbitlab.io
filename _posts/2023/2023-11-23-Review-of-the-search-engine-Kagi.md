---
title: "Review of The Search Engine Kagi"
date: 2023-11-23
classes: wide
header:
  teaser: "/assets/posts/2023/kagi.webp"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/kagi.webp" alt="">


The date is currently 23/11/2023 as I write this and I have been using the Kagi search engine since September 30th 2023, meaning that I have been using the Kagi search engine for almost 2 months now and this is going to be my review of this search engine.

If you don’t know Kagi is a search engine like Google or DuckDuckGo, but Kagi has a number of features that makes it different from these search engines. The first main difference is that Kagi is a paid search engine there are three main plans offered by Kagi. The first is $5/Per Month and you get 300 searches per month, the second plan is the one that I have gone with and its costs $10/Per Month and you get unlimited searches and get unlimited access to FastGPT and universal summarizer and then the last plan is $25/Per Month and for this you get “Exclusive and early access to Kagi features and services” as well as this plan is “Best way to support Kagi on its mission to humanize the web”.

The Kagi search engine also is completely ad-free and so doesn’t have the same business model as more popular search engines such as Google or DuckDuckGo. Kagi also says that it is a private search engine and says that it doesn’t collect any logs and don’t tie logs to any account. Kagi also says that “We only collect the bare necessities to run the service. Please see our privacy policy for more information.” Other features of Kagi include the ability to block sites from appearing in search results and also the ability to rank certain websites higher than others, as well as a number of search filters such as filter by date, time, location. The Unique selling point of Kagi is that “Instead of trying to create a search product for billions of people, we want to develop a refined search experience for sophisticated customers who value high-quality results, privacy, and speed.”

Before I share my thoughts on Kagi I want to share how much I have actually used Kagi in these two months. In September I did 74 searches (and this was mostly me playing around with Kagi, doing a number of searches), In October I did 490 searches and then so far in November I have done 487 searches and so I have done more searches than the first plan which offers 300 searches for $5/Per Month. I have been using Kagi as my main search engine on my daily driver as well as my mobile phone.

So, I wanted to share some good things I like about Kagi; the first thing I like is that you are able to block websites from showing in search results (I’m looking at you Wikihow) and I really like the fact you can prioritize results from certain websites i.e. rank documentation sites and stackoverflow higher than normal sites. 

As for the results themselves, I found them to be generally fine and noticed that there are a lot more independent blogs showing in search results, which I really liked as compared to Google ranking garbage sites that are only top sites because of their SEO i.e., top 10 articles from random sites. Whenever I felt that the search results were lacking in Kagi and tried searching the same thing in Google, generally Google would have the same if not very similar results. I also notice that on Kagi there isn’t results from YouTube as there normally is whenever you search on Google, which I generally liked.

I also notice than on Google, Google would a lot of the time have reddit posts as the top search results and I do think Kagi does similar things with ranking reddit posts highly and I don’t really mind this because reddit generally has people in comments sharing their advice and most of the time it is helpful.  Another good thing I like about Kagi is that the results are generally very fast as compared to other search engines, and I quite like the ability to do filters with time and I found this working quite well generally.

So now for the downsides of using Kagi and to be honest there are not that many. The first downside I found is that you have to create an account with an email address and pay them money to use the search engine and there is no way to really verify their claim that Kagi doesn’t store any logs on search queries, so you generally just have to trust their word that they don’t store any logs. The other downside with Kagi is it may block search requests coming from the tor network and I actually asked about this and was told that this is something that Kagi have no control over.

In conclusion, Kagi is a very fast and reliable search engine that doesn’t serve any ads to its customers and offers a range of features which to be honest Google and DuckDuckGo should have implemented years ago such as the ability to rank sites and block sites from appearing in search results. I also did like the filters that Kagi offers with search results and although I haven’t really utilized some of its other features such as using LLM’s to summarize websites I still really liked Kagi and for the price of only $10 a month which is roughly £8.50 UK currency I thought it was a very reasonable price for what you are getting. I also believe that there needs to be improvement or radical changes with search engines, because generally the quality of search results on Google and DuckDuckGo have only gotten worse and worse over the years, especially with Google so heavily having ads on their search results and pushing so heavily for their other platforms such as YouTube.

Update 1: 31/12/2023

I decided to try DuckDuckGo for two weeks on 14/12/2023 and using it for two weeks I really just miss Kagi and as such have decided to switch back to Kagi because Kagi's search results are simply better than DuckDuckGo's.
