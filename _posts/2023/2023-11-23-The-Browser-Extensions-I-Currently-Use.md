---
title: "The Browser Extensions I Currently Use"
date: 2023-11-23
classes: wide
header: 
  teaser: "/assets/posts/2023/firefox.png"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/firefox.png" alt="">

In this article I’m going to share the browser extensions I am currently using. (note the links are for firefox, you may or may not be able to find the same extensions for Chrome)

# Usability Online

uBlock Origin <https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/> - This is perhaps the best ad-blocker available on the web to this day. This extension is very useful for blocking ads on most websites and can even block ads on YouTube (even with YouTube putting efforts to stop ad blockers)

Dark Reader <https://addons.mozilla.org/en-US/firefox/addon/darkreader/> - This extension just really helps to reduce the glare of white websites because it will automatically turn the web to dark mode.

Temporary Containers <https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/> - This extension really helped me when working for a UK MSP where I need to sign into so many 365 Tenants. This extension will create a temporary container (with 0 cookies) and so you can open a temporary container and sign into a different account on the same platform which is really useful when messing around with signing into multiple accounts. Defo would recommend this extension.

# YouTube related:

Clickbait Remover for Youtube <https://addons.mozilla.org/en-US/firefox/addon/clickbait-remover-for-youtube/> - This browser extension will make the thumbnails of YouTube videos change to either the start of the video, middle of the video or the end of the video (personally I use the middle of the video). This extension has helped with preventing clickbaiting. Although I will say that this extension does impact on the recommended algorithm in unusual ways i.e. the algorithm will not be as effective or good.

SponsorBlock - Skip Sponsorships on YouTube <https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/> - This extension is really good for automatically skipping sponsored segments of YouTube videos and I love that I can play a game on one monitor and play YouTube videos on another and not have to alt tab out my game to fast forward the YouTube video to skip the sponsored segments, this extension does that for me automatically. The extension even has a total time that you have saved from the extension and even has a leaderboard for top time saved (although currently this isn’t working for me).

Unhook: Remove YouTube Recommended Videos Comments <https://addons.mozilla.org/en-US/firefox/addon/youtube-recommended-videos/> - This extension has the power to hide the YouTube recommended feed, which is useful when you want to stop being so controlled by the YouTube algorithm. 


