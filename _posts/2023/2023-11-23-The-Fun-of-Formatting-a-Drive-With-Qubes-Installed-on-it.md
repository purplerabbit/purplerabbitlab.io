---
title: "The Fun of Formatting a Drive With Qubes Installed On It"
date: 2023-11-23
classes: wide
--- 

This article is going to be about my journey of reformatting an SSD drive that had Qubes OS installed on it and the troubles I experienced of doing so. 

I installed Qubes OS on an SSD and played around with it and decided I no longer wish to use Qubes OS anymore. I may or may not do a review of Qubes OS in the future.

For those who just want the TL;DR of how I got the SSD with encrypted Qubes installation back to an SSD drive that can be used for Windows:

I booted into a Kali bootable USB and then using gparted (gui tool) deleted partitions and then made a new partition making sure to choose NTFS and not the default EXT4 which is the file system used in Linux. Then added some test files to the new volume in Linux, then booted back into Windows and the drive was showing with the files that I put while on the Live Linux system and the drive was working as expected.
 
Now the long version:

On the Windows 10 Machine that I now use I first removed all the partitions from the SSD using the Disk Management tool and right clicking on the partitions and deleting them (Although I think I may have used the tool “AOMEI Partition Assistant” to delete the EFI partition as the delete option was greyed out in disk management.

I then spent the next hour and a half messing with diskpart and doing so many cleans on the drive and trying about maybe 5 times to format the drive to NTFS but each time I did it would just freeze at 0%.

I then installed AOMEI Partition Assistant and tried to format using this tool but it gave me an error “Information Code: 206 Description:Failed to update $BITMAP file (with bad cluster)” 

So I tried to format to FAT32, maybe that would work… Nope got “Information Code: 107 Description:Failed to write FAT table 2 due to bad sectors.”

So I spent about an hour trying to look online for someone with similar issues and most articles just said to do diskpart and some did say check attributes to see if its read only and I checked this and readonly was not the issue.

So I then decided to check for bad sectors or bad clusters (one of those) and this took about an hour (and tbh I didn’t think this was the issue, but though worth a shot) and it came back completely clean.

I then decided to boot from a Kali bootable usb I have and try using fdisk instead following guidance from this stackexchange thread <https://unix.stackexchange.com/questions/252625/how-can-i-use-fdisk-to-create-a-ntfs-partition-on-dev-sdx>

I did the following:

Opened terminal and did sudo su to get to root  
Then did  
    fdisk -l  
to find the drive  
Then did  
    fdisk /dev/sdx (x being the drive)  
Then typed n and enter   
Then typed p and enter  
Then typed w and enter to write the changes  
Next I then did mkfs.ntfs -f /dev/sdx1   
The drive auto mounted and I added a file to test  
Booted back into Windows 10 and the drive was not showing as NTFS and wasn’t showing in drives 
 
I then booted back into Kali bootable USB (note to boot into a USB you need to go into your bios and change the boot order and choose your bootable Linux USB)
  
I then decided to try using gparted (GUI tool) and deleted the partition that was there and created a new  partition and made sure to choose ntfs and not ext4 which is default.   
Then mounted the drive and put some dummy files on there   
Booted back into windows 10 and the drive was now showing correctly and showing as NTFS and I could put files on the drive just fine.
 
(Not too sure why it wasn’t showing as NTFS the first time with using mfs.ntfs, but gparted did the job)  

I guess this has taught me that Windows tools are useless when it comes to formatting drives and that whenever you have issues with formatting drives don’t waste your time with windows, just resort to trying it through Linux because Linux tools do work. Although with Linux there is a greater chance of messing up your real windows system if you misspell your drive which is very easy to do, so make sure you know what you are doing when it comes to using Linux to format drives because one wrong character and you can easily brick your Windows installation. 

Update 02.12.2023

I tried putting data on this ssd and it was very slow and just wasn't working correctly.

I bought a new SATA cable for the ssd and it is now working fine.
