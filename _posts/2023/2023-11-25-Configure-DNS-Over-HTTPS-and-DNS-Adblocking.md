---
title: "How To Configure DNS Over HTTPS and DNS Ad-Blocking on Windows and Android"
date: 2023-11-25
classes: wide
--- 

This is going to be a quick article about how to enable DNS over HTTPS for Android and Windows for added security and to block ads. The DNS server I will be using is the free one offered by AdGuard. 

The main reason I did this was because I wanted to block ads on my phone which is running Graphene OS and read on this forum thread <https://discuss.grapheneos.org/d/3347-vanadium-ad-blocker> that to do this I would need to use a DNS server that blocked ads, hence why I decided to use AdGuard's DNS server which blocks ads and is completely free and also offers DNS over HTTPS.

This is because on the default browser on Graphene OS called "Vandium" it doesn't by default block ads and you are not able to get extensions for this browser (although you can still get firefox and install Ublock Origin). This browser does come with some security features, hence why I wanted to configure DNS Ad-blocking on my phone and this is the prefered method when using Graphene OS.

However, like most things IT related I don't just end with there... I need to get it to work also on my PC running Windows and also need to confirm that its all working correclty. 

All in all this took me about 2 hours of my Saturday afternoon including writing this post and I hope it helps anyone out there who wants to configure DNS over HTTPS or wants to use DNS Ad-Blocking.

Note that doing this will block the ads served on most news websites (although haven't really checked for that many news sites) and I can confirm that it doesn remove ads on sky news which is the main news source I go to nowadays.

# Why setup DNS over HTTPS?

From Wikipedia page on DNS over HTTPS:

*“DNS over HTTPS (DoH) is a protocol for performing remote Domain Name System (DNS) resolution via the HTTPS protocol. A goal of the method is to increase user privacy and security by preventing eavesdropping and manipulation of DNS data by man-in-the-middle attacks by using the HTTPS protocol to encrypt the data between the DoH client and the DoH-based DNS resolver.”*

However the main reason I myself am using DNS over HTTPS is mainly for the ability to block ads (although the added security benefit is also nice).

# Difference between DNS over HTTPS and DNS over TLS 

From <https://1.1.1.1/faq>

*“Both DNS over TLS and DNS over HTTPS encrypt plain DNS queries from the phone. DNS over HTTPS uses port 443 and DNS over TLS uses port 853. In some networks, one of these ports might be blocked. If port 443 is blocked you should use DNS over TLS. If port 853 is blocked, you should use DNS over HTTPS. In some cases, DNS over TLS may be faster than DNS over HTTPS or the other way around.”*

# Configuring DNS over HTTPS in Windows and fixing Google Chrome DNS issue

To enable DNS over HTTPS on Windows first go to Control Panel > Network and Sharing Centre > Change Adapter settings > Right click on the appropriate network i.e ethernet if you use ethernet or wifi if you use wifi > Properties > double click “Internet Protocol Version 4 (TCP/IPv4) > and Click “Use the following DNS server addresses” > Input the ip address of the DNS server you want to use, in my case it was the AdGuard DNS servers which are free and so the in “Preferred DNS server” I input “94.140.14.14” and for the alternative DNS server I input “94.140.15.15”. Then click “OK”.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/dns.png" alt="">

Next still in properties click on “Internet Protocol Version 6 (TCP/IPv6)” and same again click “Use the following DNS server addresses” and then input the IPv6 addresses of the DNS server you want to use, in my case again I used the IPv6 addresses for AdGuard DNS which is “2a10:50c0::ad1:ff” and “2a10:50c0::ad2:ff” and then press “OK”.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/ipv6.png" alt="">

Next open up your browser and visit <https://adguard.com/en/test.html> and confirm that you see that DNS over HTTPS is enabled.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/dns 1.png" alt="">

Please note that the DNS checker website <https://1.1.1.1/help> will only show dns over HTTPS if you are using the 1.1.1.1 server, any other servers i.e using AdGuard servers will show you as not having DNS over HTTPS and I had to figure this out the hard way... i.e spending maybe an hour trying to figure out why this site was showing DNS over HTTPS as disabled.

Another thing you need to note is that browsers have their own DNS over HTTPS options. So, make sure that you don’t have these set up already in your browser otherwise it may show on AdGuard test page that you are not using AdGuard DNS over HTTPS. I also sadly had to figure this out myself the hard way.

Now I want to share the issue I had with Google Chrome that may help someone in the future. Note that this is only relevant if you are using the Google Chrome's built in feature to enable DNS over HTTPS and also are using 1.1.1.1 as the DNS server. 

When I tried going to 1.1.1.1/help on Google Chrome it said that DNS over HTTPS was disabled and seemed to be using a different DNS server than to what I just configured. After some searching I found this article on serverfault <https://serverfault.com/questions/1090210/why-will-firefox-resolve-my-domain-but-chrome-will-not> and the last answer said to visit <chrome://net-internals/#dns> on Chrome and then “Click the "Clear host cache" button.” And I did this and then tested by visiting 1.1.1.1/help again and it worked just fine now.

# Configuring DNS over HTTPS for Android

Go to Settings → Network & Internet (or Wi-Fi & Internet) > Select Advanced → Private DNS. > Select Private DNS provider hostname and enter the DNS server you want to use, in my case again it was AdGuard’s which was “dns.adguard-dns.com”. 

Then visit the same page <https://adguard.com/en/test.html> and at the bottom of the page it should tell you that you are using AdGard’s DNS server.

I also want to add that I use ExpressVPN on Windows and Android and when using ExpressVPN it will still use the AdGuard server (not too sure about other VPNs as they may use their own DNS server depending on which one you use).

I also just wanted to add here that the Pi Hole project where you have a raspberry pi acting as a DNS server which blocks ads DOES NOT WORK FOR YOUTUBE, I had to learn this after I had spent around 3 and a half hours updating my old raspberry pi and updating pi hole and configuring it. This is because YouTube ads are served on the same domain as YouTube itself meaning that it doesn't block any ads for YouTube sadly. 

With that said I will also be bringing out an article on how to block YouTube ads very soon.

For further reference please see below sites:

# Reference

<https://helpdeskgeek.com/how-to/what-is-secure-dns-and-how-to-enable-it-in-google-chrome/>  
<https://windowsloop.com/enable-dns-over-https-chrome/>  
<https://www.dnsleaktest.com/>  
<https://www.routersecurity.org/testdns.php>  
<https://www.techradar.com/features/what-is-dns-over-https-and-should-you-be-using-it>   
<https://adguard-dns.io/en/public-dns.html>   
<https://en.wikipedia.org/wiki/DNS_over_HTTPS>  
<https://1.1.1.1/help>  
<https://www.cloudflare.com/en-gb/learning/dns/dns-over-tls/>  
<https://github.com/curl/curl/wiki/DNS-over-HTTPS>    
<https://adguard.com/en/test.html>   
<https://adblock-tester.com/>   
<https://dnsleaktest.com/>     