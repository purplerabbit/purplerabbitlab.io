---
title: "How to Block YouTube Ads"
date: 2023-12-02
classes: wide
header:
  teaser: "/assets/posts/2023/youtube_logo.png"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/youtube_logo.png" alt="">

This article is going to be an updated guide 2023/2024 on how to block YouTube ads on various different platforms. To get YouTube ad-free on smartTV please see my article I did on this [here]({% post_url 2023/2023-11-22-How-To-Get-YouTube-Adfree-on-LG-SmartTV %})

# Private Clients 
## FreeTube for Windows/Linux/Mac OS
You can download the Private Free and Open Source YouTube client called FreeTube linked [here]( https://freetubeapp.io/#download) and this client allows you to watch YouTube completely ad-free

## NewPipe for Android
NewPipe is similar to FreeTube but is for android only and allows you to watch YouTube completely ad-free and also allows you to download YouTube videos or just its audio. Linked [here]( https://newpipe.net/#download) 

## IOS 
Adguard linked [here]( https://adguard.com/kb/adguard-for-ios/solving-problems/block-youtube-ads/) is the way most people that use IOS block YouTube ads

# Browser Extensions for Firefox/Chrome
## Ublock Origin 
Ublock Origin made by Raymond Hill is the industry standard for blocking ads in your browser. You can get the Firefox extension [here]( https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) and the Chrome version [here]( https://chromewebstore.google.com/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) (although Chrome is said to have some issues with blocking ads or using this extension) 

Also note that it’s a very cat and mouse game with Ublock Origin. Sometimes YouTube brings an update to the platform and Ublock Origin stops blocking ads. Make sure you have the latest update of Ublock Origin.

A general good rule of thumb with this extension is to check their subreddit found [here](https://www.reddit.com/r/uBlockOrigin/) for any reported issues with it working with YouTube and they oft have an updated guide on how to get this extension to block YouTube ads.

There is also this very cool website which checks if the current Ublock Origin works against the updated version of YouTube linked [here](https://drhyperion451.github.io/does-uBO-bypass-yt/)

# Browsers
## Brave
Brave browser is known to be good for blocking YouTube ads. Linked [here]( https://brave.com/) 

Resources:

<https://privacysavvy.com/security/social/block-youtube-ads/>  
<https://adguard.com/en/blog/how-to-add-a-shortcut-to-block-youtube-ads.html#instruction>  
<https://cybernews.com/best-ad-blockers/how-to-block-ads-on-youtube/>  
<https://www.cloudwards.net/block-ads-on-youtube/>  
<https://www.websafetytips.com/how-to-block-youtube-ads/>  
<https://www.reddit.com/r/youtube/comments/17bvj9w/safe_solutions_to_avoid_ads_on_youtube_and_avoid/>  
<https://www.reddit.com/r/youtube/comments/17ac0ht/megathread_ad_blockers_are_now_blocked_on_youtube/>  
<https://www.reddit.com/r/uBlockOrigin/comments/173jmog/youtube_antiadblock_and_ads_october_09_2023/>  
<https://www.reddit.com/r/Adblock/comments/17m8anu/how_to_block_yt_ads_like_a_pro/>  

