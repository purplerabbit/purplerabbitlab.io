---
title: "How To Block Cookie Popups"
date: 2023-12-08
classes: wide
--- 

This article is going to go over how to block the annoying Cookie pop ups that you likely get on the websites that you visit.

First I want to mention here that the popular extension [“I don’t care about cookies”]( https://addons.mozilla.org/en-GB/firefox/addon/i-dont-care-about-cookies/) was acquired by [avast]( https://www.i-dont-care-about-cookies.eu/whats-new/acquisition/) and avast was [caught spying on its own users for 7 years]( https://www.safetydetectives.com/blog/avast-scandal-why-we-stopped-recommending-avast-avg/) Generally this extension is no longer recommended anymore as of 2023/2024.

# I still Don’t care about cookies Extension 

Available for download for Firefox [here](https://addons.mozilla.org/en-US/firefox/addon/istilldontcareaboutcookies/) and Chrome [here]( https://chromewebstore.google.com/detail/i-still-dont-care-about-c/edibdbjcniadpccecjdfdjjppcpchdlm) 

This extension is a fork of the extension “I don’t care about cookies” and is trusted a lot more due to it *not* being owned by avast and it being open source, source available on their [github]( https://github.com/OhMyGuus/I-Still-Dont-Care-About-Cookies) 

# Consent-O-Matic Extension 

Available for download for FireFox [here](https://addons.mozilla.org/en-GB/firefox/addon/consent-o-matic/) and for Chrome [here]( https://chromewebstore.google.com/detail/consent-o-matic/mdjildafknihdffpkfmmpnpoiajfjnjd) 

# Ublock Origin lists

Ublock Origin by Raymond Hill Available for download for Firefox [here](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/) and for Chrome [here]( https://chromewebstore.google.com/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) 

## Adding I still don’t care about cookies filter list to Ublock Origin 

You can use the extension “I still don’t care about cookies” filter list and simply import this into Ublock Origin. Goto Ublock Origin settings > Filter List > Import > then in the text field add <https://www.i-dont-care-about-cookies.eu/abp/> and then you should see it appear in filter list/Custom.

## Using Adguard and Easylist filters to block cookies

Ublock Origin settings > Filter lists > Annoyances > Adguard/uBO – Cookies notices Enable and > Easylist Annoyances > Easylist/uBO – Cookie notices Enable. 	

Resources: 

<https://www.vice.com/en/article/qjdkq7/avast-antivirus-sells-user-browsing-data-investigation>  
<https://palant.info/2019/12/03/mozilla-removes-avast-extensions-from-their-add-on-store-what-will-google-do/>   
<https://www.vice.com/en/article/qjdkq7/avast-antivirus-sells-user-browsing-data-investigation>     
<https://github.com/cavi-au/Consent-O-Matic>     
<https://github.com/CodyMcCodington/AutoCookieOptout>  
<https://news.ycombinator.com/item?id=32850799>  




