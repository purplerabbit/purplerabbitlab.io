---
title: "How to Download YouTube Videos"
date: 2023-12-08
classes: wide
header:
  teaser: "/assets/posts/2023/youtube_logo.png"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/youtube_logo.png" alt="">

This article is going to be a quick guide on how to download YouTube videos in mp3 format or mp4 format. The tools used is the Android application NewPipe linked [here]( https://newpipe.net/) and yt-dlp linked [here]( https://github.com/yt-dlp/yt-dlp) 

## Downloading the application 

This tool is the fork of the popular tool YouTube-dl which has now been discontinued. 

First download the latest version of the tool from the release page on github linked [here]( https://github.com/yt-dlp/yt-dlp/releases) (if on windows download the [this]( https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp.exe) version.

You will need to download the separate tool ffmpeg and you can get Windows binaries linked [here]( https://www.gyan.dev/ffmpeg/builds/) Be sure to download the “ffmpeg-git-essentials.7z”.

You will also need 7zip to extract the files linked [here]( https://7-zip.org/) 

## Configure application in environment variables

Make a directory “C:\ytdl” and then place “yt-dlp.exe” in this directory and then place the file “ffmpeg.exe” into the directory (you should find it in the bin directory).

Next you can add “C:\ytdl” to environmental variables so that you don’t need to cd into it every time.

To do this Windows Key + R > run box type “SystemPropertiesAdvanced” and hit run > Environment variables > System variables > scroll to PATH > Edit > New >  type “C:\ytdl” > Click ok to save changes

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/download yt.png" alt="">

Open a new command prompt and you should be able to type “yt-dlp” and it should run the application and just give an error because you didn’t specify option.



## Download MP3 from Youtube videos

`yt-dlp -x --audio-format mp3 --audio-quality 0 -P "<Path to store the files>" https://www.youtube.com/watch?v=W8lB54gbsTA`

You can simply add further YouTube videos to the end with a space between them i.e 

`yt-dlp -x --audio-format mp3 --audio-quality 0 -P "<Path to store the files>" https://www.youtube.com/watch?v=W8lB54gbsTA https://www.youtube.com/watch?v=PZiWWjlG-Zs` 

## Downloading the video and audio 

`yt-dlp -P "<Path to store the files>"  https://www.youtube.com/watch?v=dQw4w9WgXcQ`

## Downloading Playlist MP3 

`yt-dlp -i --extract-audio --audio-format mp3 --audio-quality 0 -P "<Path to store the files>" https://www.youtube.com/playlist?list=PLGTTBqAPJzTqooqXyALeL9JfRq3dJoPYQ`

## Downloading video + audio of an entire channel 

You can simply navigate to the directory you want to store all the videos of someone’s channel and then run

`yt-dlp https://www.youtube.com/@OutperformMP` 

Replace the youtube channel with the one you want to download

## Outputting without the YouTube ID

`yt-dlp -x --audio-format mp3 --audio-quality 0 -o "C:\Users\<user>\Documents\%(title)s.%(ext)s" https://www.youtube.com/watch?v=hK8cjJ8kv8I`

## Stay up-to-date
Simply run  
`yt-dlp -U`


# NewPipe Download YouTube videos on Android

First download NewPipe on your android device from the official website and you may need to turn on the setting “Download applications from unknown sources”.

Once downloaded you can search for a video you wish to download and then on the right side there is an option to Download and then you can choose to either download the video or the audio.

Resources:

<https://www.reddit.com/r/youtubedl/comments/qzqzaz/can_someone_please_post_a_simple_guide_on_making/>    
<https://github.com/yt-dlp>    
<https://helpdeskgeek.com/windows-10/add-windows-path-environment-variable/>     
<https://www.java.com/en/download/help/path.html>    
<https://stackoverflow.com/questions/5327495/list-all-environment-variables-from-the-command-line>     	
<https://wiki.archlinux.org/title/Yt-dlp>      
<https://www.reddit.com/r/youtubedl/comments/qzqzaz/can_someone_please_post_a_simple_guide_on_making/>  
<https://www.reddit.com/r/youtubedl/comments/14c31go/how_to_use_ytdlp_advanced_tutorial_guide_2023/>


