---
title: "How to Export YouTube Data/Google Data"
date: 2023-12-08
classes: wide
header:
  teaser: "/assets/posts/2023/youtube_logo.png"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/youtube_logo.png" alt="">

In this article I am going to share how you can export your YouTube history and YouTube subscriptions from YouTube or more broadly how you can export all your Google Data – literally every bit of data that Google has about your account.

The first thing you need to do is [visit this link](https://takeout.google.com/) and then sign into your Google account you want to export data from.

Next you can look at all the different data that you can export from your Google account as of this article’s data 08/12/2023 there should be 45 data sources for your export. Make sure you have YouTube ticket (it should be at the bottom) if you only want to export YouTube data then simply Deselect All and just click YouTube. Then it will ask the destination, choose whichever you prefer, in my case I emailed it to myself and click “Create Export”. 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/youtube export.png" alt="">

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/youtube export 2.png" alt="">

You will then need to wait and depending on the size of the export it could take a few days, but you should get an email when its complete. 

Once you are emailed that the export is done. Download it and then unzip the archive and then click the folder “YouTube and YouTube Music” and all the data is there and some is in .html format and some is in .csv format – this is simply how Google exports the data. For watch-history you can open the html file and then search though it using CTRL + F to find what you are looking for.

Note that the data will only include the YouTube videos you have watched while signed into that account. If you are not signed in YouTube doesn’t retain the data.

Resources:

<https://takeout.google.com/>  
<https://www.ticktechtold.com/export-youtube-watch-history/>    
<https://webapps.stackexchange.com/questions/137049/how-do-i-export-my-data-from-youtube>   
<https://freepctech.com/how-to/export-youtube-subscriptions/>   