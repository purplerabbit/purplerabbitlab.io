---
title: "Why I left LinkedIn"
date: 2023-12-15
classes: wide
header: 
  teaser: /assets/posts/2023/linkedin.jpg
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/linkedin.jpg" alt="">

This is going to be an article on why I decided to leave the business and employment-focused social media platform LinkedIn which I have had an account on since around 2021 since after I joined my first company. I mainly used LinkedIn to advertise myself to potential employers and had around 90 connections all with people whom I had met personally either through my education in Uni/A-Levels, my employment or friends/family. I never actually posted anything on LinkedIn and mainly used it to connect with people in my work and keep track of what others are doing, but also use it to find new employment.

One of the main reasons I decided to leave LinkedIn is because every time I scrolled through my feed on the main page I just felt like all the content from people I’ve connected to and companies that I follow were purely made for PR purposes and I felt like all interactions and comments I saw on this platform were all just very disingenuous and all interactions were just to make yourself “look good” for future employers. I could compare LinkedIn to the reality distortion mess that is Instagram with the majority of users on Instagram using untold number of filters and effects on their picture to make themselves seem more attractive than they actually are, but this wouldn’t quite hit the mark.

LinkedIn’s reality distortion is focussed on what people think future employers want to see from them and its just a constant game of everyone on the platform trying to meet the expectations of what they think their future employers will want to see and this has lead this platform to become this absolute mess of everyone so concerned about their self-image and how they come across to others that it just makes any meaningful interaction on the platform completely impossible because everyone on the platform are all playing this game of trying to make themselves look so professional to potential future employers with all their profile pictures of themselves wearing a suit and smiling. 

Generally, I have found LinkedIn’s users and content to be so out of touch with reality and so absolutely disingenuous that I just feel so disgusted each time I scroll thorough my feed and need some time after to “de-toxify” from all the content I see on the platform purely designed for “PR purposes”. Also, I feel that the “Reality vs LinkedIn” meme to be very accurate which you can find [here](). I have found that everyone on the platform is so completely scared and afraid to share their actual genuine opinions on any topics because they know that it links with their real-life account tied to their actual in real life name. This just leads to the same kind of content, opinions and narratives in every single post/comment that is just makes the platform very repetitive and I feel like I’ve read the same post/comment 100 times already. 

Furthermore, I found that LinkedIn’s constant virtue signalling from its users to just be completely ridiculous and there was just a complete lack of any kind of original content on the platform. Most of the content was some sort of achievement like a certification obtained and then the all the comments were simply congratulations on the achievement. At times, I felt like the content on the platform was something that ChatGPT could probably come up with because all of it was just so unoriginal, so focussed on self-image and so concerned with how prospect future employers may view the content.

Conclusively, this is of course my experience with the platform LinkedIn, and you may have different experiences with the platform, but for me I have since deleted my LinkedIn account because I just felt like I just don’t want to be part of a platform that is so disingenuous and so focussed on self-image and “PR”. I also felt that any meaningful interaction on the platform is completely impossible because everyone on the platform is so focussed on how they come across and how they are perceived and almost everyone on the platform only speaks in “PR”. Goodbye LinkedIn, I will not miss you.

Update 1 26/12/2023

Privacy issues were not the reason I actually left, but its nice to know that in leaving this has helped reduce my digital footprint online and has helped by privacy online.



