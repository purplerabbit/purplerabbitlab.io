---
title: "A Guide to PGP Encryption on Windows"
date: 2023-12-16
classes: wide
header:
  teaser: "/assets/posts/2023/windows.png"
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/windows.png" alt="">

This is going to be a guide on how to use PGP encryption on Windows, doing so will be similar on MacOS and Linux. PGP is generally used to encrypt and sign emails. The main goals of PGP is to ensure that only the intended person can read the content of the email and also to confirm that it’s really the person you think it is sending the message. 

PGP does this by generating a public key – the key which you give out to everyone and then a private key which you keep hidden from others. The private key is used to decrypt messages that people encrypt with your public key.

## Download PGP gpg4win on Windows

Goto [this]( https://gpg4win.org/download.html) website and download and for the donation set it to 0 and it should download the file.

Once downloaded install the software and use default installation settings.

## Setting up key pair

To setup a key pair launch a command prompt in Windows and run “gpg --full-gen-key” > Select 1 RSA and RSA > set to 4096 keysize for maximum security > then specify how long the key is valid for, in my case I set to 0 does not expire > then enter your name and email associated with the key > Then it will ask you for a password, you can not set a password if you do so choose. > Then hit O for okay and it may ask you to move around your mouse for randomness. 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/gpg.png" alt="">

## Signing something with your key without encryption 

This is done to ensure that it is really you that has written the message because the message is signed with your private key that only you should have.

To do this open “Kleopatra” which should be installed now > click on “Notepad” > Then goto recipients and disable all encryption and make sure you have sign as and your correct key selected > go back to Notepad and type your message and then click “Sign Notepad”.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/gpg 1.png" alt="">

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/gpg 2.png" alt="">

## Importing someone else’s public key so you can encrypt your messages with their public key

Simply Import > Select the .asc file of the public key you want to import > You should then see the key imported in “All Certificates” 

## Signing something with your key and using someone’s public key to encrypt

In Notepad > Recipients > Sign as and choose the correct private key to sign the message as > “Encrypt for others” > Choose the person’s key you want to encrypt the message with > Go back to Notepad and click “Sign / Encrypt Notepad” Then copy the from “-----BEGIN PGP MESSAGE-----” down to “-----END PGP MESSAGE-----” and send the text as it appears in an email or as an attachment.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/gpg 3.png" alt="">

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/gpg 4.png" alt="">

## Decrypting messages that were encrypted with your public key

Copy the entire text of the PGP encrypted message from “-----BEGIN PGP MESSAGE-----” down to “-----END PGP MESSAGE-----” and then paste it into notepad > then do “Decrypt / Verify Notepad” and it should output the text that was encrypted and tell you if the signature was valid.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/gpg 5.png" alt="">

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/gpg 6.png" alt="">

## Exporting your public key to give to people

Click on “Certificates” > right click the key you want to export > click export > click the file and open with notepad and confirm that it says “PGP PUBLIC KEY BLOCK” meaning this is the public key and this is the key that can be shared with people.

## Exporting your private key to keep safe

Click on “Certificates” > right click the key you want to export > click “Backup Secret keys” > Save somewhere > open the file with notepad and confirm that it says “PGP PRIVATE KEY BLOCK” meaning that this is the private key that SHOULD NOT BE SHARED WITH ANYONE.

Resources:

<https://emailselfdefense.fsf.org/en/>   
<https://sectigostore.com/blog/public-key-vs-private-key-how-do-they-work/>  
<https://wiki.archlinux.org/title/GnuPG>    
<https://www.reddit.com/r/GPGpractice/wiki/the_process/kleopatra/#wiki>
