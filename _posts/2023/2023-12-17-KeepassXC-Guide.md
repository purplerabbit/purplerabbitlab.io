---
title: "KeepassXC and KeepassDX Guide"
date: 2023-12-17
classes: wide
header: 
  teaser: /assets/posts/2023/keepass.png
--- 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass.png" alt="">

This article is going to be a quick guide on how to setup a KeepassXC database. 

Please note that by default KeepassXC disallows you to screen record it or capture screenshots. However, you need to run KeePassXC.exe --allow-screencapture in the directory you have it installed to be able to screenshot.

## What is KeepassXC?

KeepassXC is a offline password manager that allows you to store and generate all your passwords for all your accounts in an encrypted database.

Password managers are a solution to storing and generating all your passwords for your online accounts. There are online and offline solutions, KeepassXC is an offline solution, meaning you will need to have your own way of syncing the database across different devices – personally I use Syncthing to do this and may make an article in the future on this application. 

With KeepassXC instead of remembering all different passwords for every single account you own, you just remember one very secure password which then decrypts the database and allows you to access all your passwords. 

# Downloading KeepassXC 

Download KeepassXC from [this]( https://keepassxc.org/download/) link. KeepassXC is available for MacOS, Windows and Linux.

# Setting up the Database 

Open KeepassXC and then click on “Create a New Database” and then fill some details if you like > Encryption settings choose KDBX4 (recommended) and set the decryption time to your liking > Then choose a very unique and secure password which will then be used to decrypt your password database [ITS IMPORTANT TO REMEMBER THIS PASSWORD, OTHERWISE YOU WILL LOSE ACCESS TO ALL THE DATA INSIDE THE DATABASE] 

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 9.png" alt="">

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 10.png" alt="">

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 11.png" alt="">

ITS VERY IMPORTANT TO KEEP THE DATABASE ITSELF BACKED UP, OTHERWISE IF YOU LOSE IT, YOU WILL LOSE ALL OF THE DATA INSIDE IT.

## Adding entries

Click the Plus icon in the top bar

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 1.png" alt="">

Insert the details you want for the entry

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 2.png" alt="">

You also have the option of generating a password as shown in the image below. Click the dice icon in next to the password field.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 3.png" alt="">

Here you can generate a password and give the options of using special characters/numbers in the generation.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 4.png" alt="">

ITS VERY IMPORTANT FOR YOU TO PRESS OK AFTER YOU HAVE ENTERED EVERYTHING, OTHERWISE IT WILL NOT SAVE.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 5.png" alt="">

## Copying Usernames and Passwords

Simply click on the entry you want to copy and click the username field to copy the username entry and the password field for the password entry. Please note that it will delete the clipboard after a set amount of time.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 6.png" alt="">

## Managing the entries using groups

KeepassXC gives you the option to create groups to manage your entries. To do so right click the "Root" group and Click "New Group" and enter the details.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 7.png" alt="">

Below is an example of how you may manage the groups.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 8.png" alt="">

Another important note is that KeepassXC by default will close the database after a set amount of time of inactivty. 

Its also important to note that if you have an entry not saved and in the process of creating an entry the database will not lock itself and will stay open until you take action on the unsaved entry - Very good to know.

## Using KeepassDX on Android

To Download KeepassDX on Android you can get it from either the F-Droid Store or the Google Play Store, if you are using something like Graphene OS you can also use the Aurora store.

There is also a screenshot mode for the android version which can be enabled by going to Settings > App Settings > Screenshot mode.

## Creating a Database

Open the app > Create New Vault > Choose where to save the database > Enter the Master Password

## Opening a Database 

Open the app > Open Existing Database (or if you have already opened a database it will show in the recent databases) > Select the database file > Enter the master password for the database 

## Adding Entries 

Click the Plus icon at the bottom.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 12.png" alt="">

Then enter the details as appropriate.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 13.png" alt="">

You can also generate a random password, see as below.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 14.png" alt="">

## Copying Usernames and Passwords

For copying passwords to clipboard on mobile you need to turn this feature on by going to settings > Form filling > Make sure you have the "Clipboard trust" option set to on.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 17.png" alt="">

To copy the username and password simply click on the entry and click the copy icon on the username field and the password field.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 16.png" alt="">

## Managing the entries using groups

Similar to how on KeepassXC you can create groups and manage your entries in this way.

<img src="{{ site.url }}{{ site.baseurl }}/assets/posts/2023/keepass 15.png" alt="">

Resources:

<https://keepassxc.org/docs/KeePassXC_UserGuide>  
<https://github.com/keepassxreboot/keepassxc>  
<https://en.wikipedia.org/wiki/KeePassXC>  
<https://www.techradar.com/reviews/keepassxc>  
<https://superuser.com/questions/878902/whats-the-difference-between-keepass-keepassx-keepassxc>  
<https://www.howtogeek.com/879987/keepassxc-password-manager-review/>  
<https://www.howtogeek.com/886880/keepassxc-features-you-should-be-using/>  
<https://github.com/keepassxreboot/keepassxc/releases>  
<https://itsfoss.com/keepassxc/>  