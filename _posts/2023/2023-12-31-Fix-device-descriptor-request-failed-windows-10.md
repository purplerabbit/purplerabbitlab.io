---
title: "Fix Device Descriptor Request Failed Windows 10"
date: 2023-12-31
classes: wide
---

Quick article on this issue I came across in Windows 10

TL;DR 

Restart the PC without drive in uninstalled drive in device manager, plug back in and it should fix the issue.

I had a recent scare with an 8tb seagate external drive. I was transferring data across it and for some reason it would transfer to a halt and the transferring status was 0kb, so I tried closing it and it just didn't close. So I then force quit the file explorer application and continued to try transferring the data but in small increments.

I rebooted my computer and the whole desktop was going grey as if it wasn't responding and I then unplugged the external drive and it started to work. Plugged it back in and the external drive wasn't showing. Odd. I then check device manager and it shows that the device has "device descriptor request failed". Ok. Did some searching online and tried using a different port and a different wire. No luck. I then restarted my machine without the external drive attached and then plugged it in after windows had rebooted and the drive was showing in device manager, but not disk manager.

I then decided to uninstall the driver in device manager to see if that would have any affect and the drive popped up in file explorer working just fine. Restarting windows again and the external drive was working compltely fine.

Reources:

<https://helpdeskgeek.com/help-desk/10-ways-to-fix-unknown-usb-device-device-descriptor-request-failed/>   
<https://answers.microsoft.com/en-us/windows/forum/all/win-10-seagate-external-backup-drive-not/1d687cf7-51ce-41ee-8665-0ba469d625bb>   
<https://www.seagate.com/nl/nl/support/kb/usb-external-troubleshooter-003581en/>  