---
title: "Check Windows 11 Compatability"
date: 2024-01-01
classes: wide
---

This is going to be a quick article guide on how to check if your Windows 10 Computer is compatible with Windows 11. The first thing you should know is that Windows 11 requires you to have an 8th generation CPU [supposedly because of the meltdown and spectre vulnerabilities in those processors]. Meaning that I am unable to upgrade to Windows 11 because my CPU is before 6th generation – too bad for me. However I have heard of some people still able to use Windows 11 on computers with 6th generation CPU’s and to do that they put Windows 11 iso on a bootable USB and install as normal although Windows say that this isn’t recommended and that the computer will not receive updates, though people are reporting they still receive updates when doing this.

## Using Microsoft PC Health Check app

Download this app from the store and run it and then it should tell you if your machine is compatible with Windows 11.

## Using WhyNotWin11 an open-source tool

WhyNotWin11 is a free and open source tool that you can download from their Github [here]( https://github.com/rcmaehl/WhyNotWin11) and this tell will tell you specifically the issues that you have that are causing the compatibility issue with Windows 11.

Resources:

<https://github.com/rcmaehl/WhyNotWin11>  
<https://guidetech.pages.dev/posts/how-to-check-if-your-windows-10-pc-can-run-windows-11/#how-to-check-if-your-windows-10-pc-can-run-windows-11>  
<https://www.reddit.com/r/WindowsHelp/comments/o7yjgl/windows_11_cant_support_intel_core_i76700k/>  
