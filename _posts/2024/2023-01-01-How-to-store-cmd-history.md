---
title: "How To Store CMD History in Windows"
date: 2024-01-01
classes: wide
---

This article is going to be a very quick guide on how you can add history to your commands for command prompt on Windows.

To do this, we are going to use a too called [Clink]( https://github.com/mridgers/clink) which is free and open source.

All you need to do is download Clink from the downloads page on their github linked [here]( https://github.com/mridgers/clink/releases/tag/0.4.9) (this link may be outdated in the future, just check the github linked above and then look for the latest download.

Once downloaded install it and then reopen your command prompt and it should give you the option to press up on the arrow and allow you to go through your history. You can also use CTRL + R to search throughout your command history (obviously you will have needed to type some commands - it starts creating history once its installed)

Resources: 

<https://github.com/mridgers/clink/releases/tag/0.4.9>  
<https://github.com/mridgers/clink>
